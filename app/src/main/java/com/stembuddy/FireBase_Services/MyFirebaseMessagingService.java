package com.stembuddy.FireBase_Services;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stembuddy.Database.DB_Chat;
import com.stembuddy.R;
import com.stembuddy.activities.ChatActivity;
import com.stembuddy.activities.Login;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.ModelScan;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Config;
import com.stembuddy.utils.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by Himanshu on 8/12/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private SharedPreference_Main sharedPreference_main;

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());


        if (remoteMessage == null)
            return;
        sharedPreference_main = SharedPreference_Main.getInstance(this);
        if (remoteMessage.getData().size() > 0) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().toString());
                if (new JSONObject(new JSONObject(remoteMessage.getData()).getString("data")).getString("title").equalsIgnoreCase("chatting")) {
                    handleDataMessage(jsonObject.getJSONObject("data"));
                } else {
                    if (remoteMessage.getData() != null) {
                        try {
//            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
                            JSONObject jsonObject1 = new JSONObject(remoteMessage.getData());
                            JSONObject jsonObject2 = new JSONObject(jsonObject1.getString("data"));
                            handleNotification(jsonObject2);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }

        // Check if message contains a notification payload.
    }

    private void handleNotification(JSONObject message) {
        // app is in foreground, broadcast the push message
        Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
        String title = "";
        String notification_message = "";
        String topic_id = "";
        String comments_ids = "";
        String video_id = "";
        try {
            title = message.getString("title");
            String jsonObject = message.getString("message");
            JSONObject jsonObject1 = new JSONObject(jsonObject);
            notification_message = jsonObject1.getString("message");
            if (notification_message.equalsIgnoreCase("Please logout")){
                if (!NotificationUtils.isAppIsInBackground(getApplicationContext())){
                    SharedPreference_Main.getInstance(getApplicationContext()).remove_Preference();
                    startActivity(new Intent(getApplicationContext(), Login.class));
                    Toast.makeText(getApplicationContext(), "Your account is logged out as your session is expired.", Toast.LENGTH_SHORT).show();
                    getActivity().finishAffinity();
                }
                else {
                    SharedPreference_Main.getInstance(getApplicationContext()).remove_Preference();
                }
            }
            try {
                topic_id = jsonObject1.getString("topic_id");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                comments_ids = jsonObject1.getString("from_comment");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                video_id = jsonObject1.getString("video_id");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pushNotification.putExtra("message", title);
//            if (title.equalsIgnoreCase("New comment on topic")) {
//                pushNotification.putExtra("topic_id", topic_id);
//            } else if (title.equalsIgnoreCase("New comment for user")) {
//                pushNotification.putExtra("comments_ids", comments_ids);
//            } else if (title.equalsIgnoreCase("New video share from friend")) {
//                pushNotification.putExtra("video_id", video_id);
//            }
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
        final Intent emptyIntent = new Intent(MyFirebaseMessagingService.this, MainActivity.class);
        emptyIntent.putExtra("notification", "1");
        emptyIntent.putExtra("message", notification_message);
        if (title.equalsIgnoreCase("New comment on topic")) {
            emptyIntent.putExtra("topic_id", topic_id);
        } else if (title.equalsIgnoreCase("New comment for user")) {
            emptyIntent.putExtra("comments_ids", comments_ids);
        } else if (title.equalsIgnoreCase("New video share from friend")) {
            emptyIntent.putExtra("video_id", video_id);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, emptyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon_stem_buddy_app)
                        .setContentTitle(title)
                        .setContentText(notification_message)
                        .setContentIntent(pendingIntent);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        mBuilder.setAutoCancel(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (!notification_message.equalsIgnoreCase("Please logout")) {
            notificationManager.notify(1, mBuilder.build());
        }


//             play notification sound
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();
        // If the app is in background, firebase itself handles the notification
    }

    private void handleDataMessage(JSONObject json) {
        try {
//            {
//            String timestamp = null;
//            String imageUrl = null;
//
//            try {
//                imageUrl = json.getString("avatar");
//                timestamp = json.getString("time");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
            String chat_message = "";
            String senders_id = "";

            String[] msg_and_name_sender_id = handle_chat_message(new JSONObject(json.getString("message")));
            chat_message = msg_and_name_sender_id[0];
            senders_id = msg_and_name_sender_id[2];

            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//                // app is in foreground, broadcast the push message

                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);

                if (AppController.activityChatVisible) {
                    if (AppController.user_id.equalsIgnoreCase(senders_id)) {
                        pushNotification.putExtra("Chat_msg", chat_message);
                        pushNotification.putExtra("senders_id", senders_id);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                    } else {
                        show_notification(json, chat_message, senders_id);
                    }
                } else {
                    show_notification(json, chat_message, senders_id);
                }
            } else {
                show_notification(json, chat_message, senders_id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String[] handle_chat_message(JSONObject jsonObject) {
        DB_Chat db_chat = DB_Chat.getInstance(MyFirebaseMessagingService.this);
        ModelScan modelScan = new ModelScan();
        String msg = "";
        String name = "";
        String senders_id = "";
        JSONObject jsonObject1 = null;
        try {
            jsonObject1 = jsonObject.getJSONObject("details");
            msg = jsonObject.getString("message");
            name = jsonObject1.getString("name");
            senders_id = jsonObject1.getString("id");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
//            modelScan.setMsg_id(jsonObject.getString("chat_id"));
            modelScan.setMessage(jsonObject.getString("message"));
            modelScan.setMsg_senders_name(jsonObject1.getString("name"));
            modelScan.setSenders_Id(jsonObject1.getString("id"));
            modelScan.setRecievers_Id(sharedPreference_main.getUserId());
            modelScan.setMsg_senders_image(jsonObject1.getString("image"));
//            modelScan.setTimestamp(System.currentTimeMillis() / 1000 + "");
            modelScan.setDateTime_ofMsgRecieveed(System.currentTimeMillis() / 1000 + "");
//            jsonObject.getString("time")

            db_chat.insertDB_ChatDetails(modelScan);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String[]{msg, name, senders_id};
    }

    private void showNotificationMessage(Context context, String title, String
            message, String timeStamp, Intent intent, String senderId) {
        notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationMessageWithBigImage(Context context, String title, String
            message, String timeStamp, Intent intent, String imageUrl, String senderId) {
        notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    public void show_notification(JSONObject jsonObject, String chat_message, String senders_id) {
        String name = "", imageUrl = "", timestamp = "";
        JSONObject jsonObject1 = null;
        try {
            jsonObject1 = new JSONObject(jsonObject.getString("message")).getJSONObject("details");
            imageUrl = jsonObject1.getString("image");
            timestamp = jsonObject.getString("timestamp");
            name = jsonObject1.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent resultIntent = new Intent(getApplicationContext(), ChatActivity.class);
        String show_message = "";


        show_message = chat_message;
        String title = name + " has sent you a message";
        resultIntent.putExtra("Chat_msg", chat_message);
        resultIntent.putExtra("senders_id", senders_id);
        resultIntent.putExtra("senders_name", name);

        if (TextUtils.isEmpty(imageUrl)) {
            showNotificationMessage(getApplicationContext(), title, show_message, timestamp, resultIntent, senders_id);
        } else {
            showNotificationMessageWithBigImage(getApplicationContext(), title, show_message, timestamp, resultIntent, imageUrl, senders_id);
        }

    }

    public static Activity getActivity() {
        Class activityThreadClass = null;
        try {
            activityThreadClass = Class.forName("android.app.ActivityThread");
        Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(null);
        Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
        activitiesField.setAccessible(true);

        Map<Object, Object> activities = (Map<Object, Object>) activitiesField.get(activityThread);
        if (activities == null)
            return null;

        for (Object activityRecord : activities.values()) {
            Class activityRecordClass = activityRecord.getClass();
            Field pausedField = activityRecordClass.getDeclaredField("paused");
            pausedField.setAccessible(true);
            if (!pausedField.getBoolean(activityRecord)) {
                Field activityField = activityRecordClass.getDeclaredField("activity");
                activityField.setAccessible(true);
                Activity activity = (Activity) activityField.get(activityRecord);
                return activity;
            }
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
