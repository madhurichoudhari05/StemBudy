package com.stembuddy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllVideoNotus {

    @SerializedName("response")
    @Expose
    private Response_ response;
    @SerializedName("mentor_data")
    @Expose
    private MentorData_ mentorData;

    public Response_ getResponse() {
        return response;
    }

    public void setResponse(Response_ response) {
        this.response = response;
    }

    public MentorData_ getMentorData() {
        return mentorData;
    }

    public void setMentorData(MentorData_ mentorData) {
        this.mentorData = mentorData;
    }

}
