package com.stembuddy.FireBase_Services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Config;


/**
 * Created by Aditya on 27-12-2016.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    SharedPreference_Main sharedPreference_main;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        sharedPreference_main = SharedPreference_Main.getInstance(this);

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
//        send_FireBaseToserver_api(token);

    }

    private void storeRegIdInPref(String token) {
        if (!TextUtils.isEmpty(token)) {
            sharedPreference_main.setFireBaseTokenid(token + "");
        }
    }


//    private void send_FireBaseToserver_api(final String token) {
//        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST,
//                Constant.Url.BASE_URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    if (jsonObject.getInt("flag") == 1) {
////                        showMessage(ChatActivity.this, "Message sent");
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.wtf("TAG", "Error: " + error.getMessage());
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("acid", "13003");
//                params.put("user_id", "" + sharedPreference_main.getUserId());
//                params.put("device_id", sharedPreference_main.getDeviceId());
//                params.put("device_type", "1");
//                params.put("device_token", token);
//                return params;
//            }
//        };
//
//        AppController.getInstance().addToRequestQueue(sr);
//    }

}
