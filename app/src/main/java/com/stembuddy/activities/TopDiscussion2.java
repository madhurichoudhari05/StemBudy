package com.stembuddy.activities;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.adapter.Comment_List_Adapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.NotificationList;
import com.stembuddy.model.Comments_topic_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Constants;
import com.stembuddy.utils.KeyboardUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.richeditor.RichEditor;

public class TopDiscussion2 extends Fragment implements View.OnClickListener {
    private TextView recent, all;
    private View v;
    //    private WebView webView;
    private RichEditor mEditor;
    private EditText comment_box;
    private Button btn_submit;
    private SharedPreference_Main sharedPreference_main;

    private TextView title, description, create_on, post_number, views_number, num_posts;

    private ArrayList<Comments_topic_Model> comments_topic_ArrayList;

    private ListView list_comments;

    private String recent_all = "recent";
    private Comment_List_Adapter comment_list_adapter;
    private ImageView discussion_image_pofile;
    private String posts_value = "";
    private ScrollView scrollView;
    private Button btn_back;
    private boolean firstTime = true;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("JavascriptInterface")
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.top_disscussion_layout2, container, false);
        sharedPreference_main = SharedPreference_Main.getInstance(getActivity());
        list_comments = v.findViewById(R.id.list_comments);
        recent = v.findViewById(R.id.top_recent);
        scrollView = v.findViewById(R.id.scrollView);
        post_number = ((TextView) v.findViewById(R.id.post_number3));
        mEditor = v.findViewById(R.id.richEditor);
        create_on = ((TextView) v.findViewById(R.id.Create_on));
        views_number = ((TextView) v.findViewById(R.id.views_number));
        title = ((TextView) v.findViewById(R.id.title));
        description = ((TextView) v.findViewById(R.id.description));
        num_posts = (TextView) v.findViewById(R.id.num_posts3);
        all = v.findViewById(R.id.top_all);
        btn_back = v.findViewById(R.id.btn_back);
        discussion_image_pofile = v.findViewById(R.id.discussion_image_pofile);

        recent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recent_all = "recent";
                recent.setBackgroundColor(Color.parseColor("#d3af37"));
                all.setBackgroundColor(Color.parseColor("#ffffff"));

                if (((MainActivity) getActivity()).isNetworkEnabled(getActivity())) {
                    hitCommentApi("not", post_number, num_posts);
                } else {
                    ((MainActivity) getActivity()).noInternet();
                }

            }
        });

        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getArguments().getString("from_activity") != null) {
                    if (getArguments().getString("from_activity").equalsIgnoreCase("top_discussion")) {
                        ((MainActivity) getActivity()).loadFragment(new TopDiscussionList(), "Top_discussion", view);
                    } else {
                        NotificationList notificationList = new NotificationList();
                        Bundle bundle = new Bundle();
                        bundle.putString("from_activity", "main_activity");
                        notificationList.setArguments(bundle);
                        ((MainActivity) getActivity()).loadFragment(notificationList, "Notifications", view);
                    }
                }

            }
        });

        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recent_all = "all";
                recent.setBackgroundColor(Color.parseColor("#ffffff"));
                all.setBackgroundColor(Color.parseColor("#d3af37"));
                if (((MainActivity) getActivity()).isNetworkEnabled(getActivity())) {
                    hitCommentApi("not", post_number, num_posts);


                } else {
                    ((MainActivity) getActivity()).noInternet();
                }
            }
        });
        mEditor.setHtml("Enter your comment here");

        v.findViewById(R.id.post_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEditor.getHtml().trim().equalsIgnoreCase("") || mEditor.getHtml().trim().equalsIgnoreCase("Enter your comment here")) {
                    Toast.makeText(getActivity(), "Please Enter Your post first", Toast.LENGTH_SHORT).show();
                } else if (((MainActivity) getActivity()).isNetworkEnabled(getActivity())) {
                    if (((MainActivity) getActivity()).isNetworkEnabled(getActivity())) {
                        hitPost_api();
                    } else {
                        ((MainActivity) getActivity()).noInternet();
                    }
                }
            }
        });

        mEditor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mEditor.getHtml().equalsIgnoreCase("Enter your comment here")) {
                    mEditor.setHtml("");
                }
                return false;
            }
        });

//<<<<<<< HEAD
//        post_number=((TextView) v.findViewById(R.id.post_number));
//        create_on=((TextView) v.findViewById(R.id.Create_on));
//        views_number=((TextView) v.findViewById(R.id.views_number));
//        title =((TextView) v.findViewById(R.id.title));
//        description = ((TextView) v.findViewById(R.id.description));
//        num_posts = (TextView) v.findViewById(R.id.num_posts);
//
//
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.loadUrl("http://ritaraapps.com/Stem_Buddy/Ckeditor.php?user_id=" + sharedPreference_main.getUserId() + "&topic_id=" + getArguments().getString("posts_id"));
//=======

        if (((MainActivity) getActivity()).isNetworkEnabled(getActivity())) {
            hitApiToIncreaseViews("not");
        } else {
            ((MainActivity) getActivity()).noInternet();
        }

        v.findViewById(R.id.action_undo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.undo();
            }
        });

        v.findViewById(R.id.action_redo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.redo();
            }
        });

        v.findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBold();
            }
        });

        v.findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setItalic();
            }
        });

        v.findViewById(R.id.action_subscript).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setSubscript();
            }
        });

        v.findViewById(R.id.action_superscript).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setSuperscript();
            }
        });

        v.findViewById(R.id.action_strikethrough).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setStrikeThrough();
            }
        });

        v.findViewById(R.id.action_underline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setUnderline();
            }
        });

        v.findViewById(R.id.action_indent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setIndent();
            }
        });

        v.findViewById(R.id.action_outdent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setOutdent();
            }
        });

        v.findViewById(R.id.action_align_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignLeft();
            }
        });

        v.findViewById(R.id.action_align_center).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignCenter();
            }
        });

        v.findViewById(R.id.action_align_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignRight();
            }
        });

        v.findViewById(R.id.action_blockquote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBlockquote();
            }
        });

        v.findViewById(R.id.action_insert_bullets).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBullets();
            }
        });

        v.findViewById(R.id.action_insert_numbers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setNumbers();
            }
        });

        v.findViewById(R.id.action_insert_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertImage("http://www.1honeywan.com/dachshund/image/7.21/7.21_3_thumb.JPG",
                        "dachshund");
            }
        });

        v.findViewById(R.id.action_insert_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertLink("https://github.com/wasabeef", "wasabeef");
            }
        });
        v.findViewById(R.id.action_insert_checkbox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertTodo();
            }
        });

        KeyboardUtils.addKeyboardToggleListener(getActivity(), new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                if (isVisible) {
                    Log.d("keyboard", "keyboard visible: " + isVisible);
                    scrollView.post(new Runnable() {

                        @Override
                        public void run() {
                            scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });
                    btn_back.setVisibility(View.GONE);

                } else {
                    if (mEditor.getHtml().trim().equalsIgnoreCase("")) {
                        mEditor.setHtml("Enter your comment here");
                    }
                    btn_back.setVisibility(View.VISIBLE);
                }
            }
        });
        return v;

    }

    private void initView() {
        recent = v.findViewById(R.id.top_recent);
        all = v.findViewById(R.id.top_all);
        // comment_box=findViewById(R.id.top_comm_box);
        //  btn_submit=findViewById(R.id.top_btn_submit);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.top_recent:
                break;
            case R.id.top_all:
                break;
        }
    }

    private void hitCommentApi(final String web, final TextView post_number1, final TextView num_posts1) {
//        if (web.equals("not")) {
//            ((BaseActivity) getActivity()).showLoader();
//        }


        //9781115115
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (web.equals("not")) {
                    ((BaseActivity) getActivity()).dismissLoader();
                }
                try {
                    comments_topic_ArrayList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("response");
                        SharedPreference_Main.getInstance(getActivity()).setPostNumber(String.valueOf(jsonArray.length()));
                        int i5 = 1;
                        for (int i = jsonArray.length() - 1; i >= 0; i--) {
                            i5 = i5 + 1;
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("response");
                            JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("from_comment");
                            Comments_topic_Model topic_model = new Comments_topic_Model();
                            topic_model.setComment(jsonObject1.getString("comment"));
                            topic_model.setName(jsonObject2.getString("name"));
                            topic_model.setImage(jsonObject2.getString("image"));
                            topic_model.setDate(jsonObject1.getString("datetime"));
                            if (jsonObject2.getString("user_type").equalsIgnoreCase("student")) {
                                topic_model.setUser_type("Student");
                            } else {
                                topic_model.setUser_type("Mentor");
                            }
                            if (recent_all.equalsIgnoreCase("recent")) {
                                if (i5 < 11) {
                                    comments_topic_ArrayList.add(topic_model);
                                }
                            } else {
                                comments_topic_ArrayList.add(topic_model);
                            }
                        }
                        try {
                            post_number.setText(comments_topic_ArrayList.size());
                            if (recent_all.equalsIgnoreCase("recent")) {
                                num_posts.setText("10 Posts");
                            } else
                                num_posts.setText(comments_topic_ArrayList.size() + " Posts");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        comment_list_adapter = new Comment_List_Adapter(getActivity(), comments_topic_ArrayList, new TopDiscussion2(), recent_all);
                        list_comments.setAdapter(comment_list_adapter);
                        setListViewHeightBasedOnItems(list_comments);
                    } else {
                        if (web.equals("not")) {
                            Toast.makeText(getActivity(), "No Comments", Toast.LENGTH_SHORT).show();
                        }
                    }
                    if (!firstTime) {
                        scrollView.smoothScrollTo(0, 500);
                    }
                    firstTime = false;
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    setdata();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (web.equals("not")) {
                    ((BaseActivity) getActivity()).dismissLoader();
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "get_all_comment_topic");
                params.put("topic_id", getArguments().getString("posts_id"));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr);
    }








    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 500 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;

        } else {
            return false;
        }

    }

    private void hitApiToIncreaseViews(final String web) {
        if (web.equals("not")) {
            ((BaseActivity) getActivity()).showLoader();
        }

        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                if (web.equals("not")) {
//                    ((BaseActivity) getActivity()).dismissLoader();
//                }
                try {
                    comments_topic_ArrayList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("response");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                            title.setText(jsonObject1.getString("title"));
                            description.setText(Html.fromHtml(jsonObject1.getString("discription")));
                            create_on.setText(jsonObject1.getString("created_date"));
                            views_number.setText(jsonObject1.getString("views"));
                            post_number.setText(jsonObject1.getString("posts"));
                            if (recent_all.equalsIgnoreCase("recent")) {
                                if (Integer.parseInt(jsonObject1.getString("posts")) > 10) {
                                    num_posts.setText("10" + " Posts");
                                } else {
                                    num_posts.setText(jsonObject1.getString("posts") + " Posts");
                                }
                            } else {
                                num_posts.setText(jsonObject1.getString("posts") + " Posts");
                            }
                            posts_value = jsonObject1.getString("posts");
                            Glide.with(getActivity())
                                    .load(jsonObject1.getString("image"))
                                    .transform(new CircleTransform(getActivity()))
                                    .placeholder(R.drawable.profile)
                                    .into(discussion_image_pofile);
                        }
                        hitCommentApi("not", post_number, num_posts);

                    } else {
                        Toast.makeText(getActivity(), "Something Want Wrong", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                if (web.equals("not")) {
//                    ((BaseActivity) getActivity()).dismissLoader();
//                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "get_topic_details");
                params.put("topic_id", getArguments().getString("posts_id"));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void hitPost_api() {
        ((BaseActivity) getActivity()).showLoader();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                ((BaseActivity) getActivity()).dismissLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {


                        hitSend_api();

                        hitCommentApi("not", post_number, num_posts);

                        mEditor.setHtml("");
                        int val = (Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1;
                        post_number.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1));
                        if (recent_all.equalsIgnoreCase("recent")) {
                            if (val > 10) {
                                num_posts.setText("10" + " Posts");
                            } else {
                                num_posts.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1) + " Posts");
                            }
                        } else {
                            num_posts.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1) + " Posts");
                        }
                        Toast.makeText(getActivity(), "Your Post has been sent successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Something Want Wrong", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                ((BaseActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
               /* params.put("rule", "comment_on_topic");
                params.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                params.put("topic_id", getArguments().getString("posts_id"));
                params.put("comment", mEditor.getHtml());*/


                params.put("rule", "comment_on_topic");
                params.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                params.put("topic_id", getArguments().getString("posts_id"));
                params.put("comment", mEditor.getHtml());





                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(retryPolicy);
        requestQueue.add(sr);
    }


    private void hitSend_api() {
        ((BaseActivity) getActivity()).showLoader();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                ((BaseActivity) getActivity()).dismissLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {


                        /*Madhuri*/

                        mEditor.setHtml("");
                        int val = (Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1;
                        post_number.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1));
                        if (recent_all.equalsIgnoreCase("recent")) {
                            if (val > 10) {
                                num_posts.setText("10" + " Posts");
                            } else {
                                num_posts.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1) + " Posts");
                            }
                        } else {
                            num_posts.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber())) + 1) + " Posts");
                        }
                        Toast.makeText(getActivity(), "Your Post has been sent successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Something Want Wrong", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                ((BaseActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "notification_ios");
                params.put("to_id", "community");
                params.put("from_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                params.put("body", "Someone commented on topic.");
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(retryPolicy);
        requestQueue.add(sr);
    }








    private void setdata() {

        if (SharedPreference_Main.getInstance(getActivity()).getPostNumber() != null) {
            int val = (Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber()));
            if (recent_all.equalsIgnoreCase("recent")) {
                if (val > 10) {
                    num_posts.setText("10" + " Posts");
                } else {
                    num_posts.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber()))) + " Posts");
                }
            } else {
                num_posts.setText(String.valueOf((Integer.parseInt(SharedPreference_Main.getInstance(getActivity()).getPostNumber()))) + " Posts");
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        firstTime = true;
    }
}