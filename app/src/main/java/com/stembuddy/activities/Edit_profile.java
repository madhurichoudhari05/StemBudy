package com.stembuddy.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RetryPolicy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.apicall.FileUploadModel;
import com.stembuddy.apicall.UpdateMultipartListener;
import com.stembuddy.apicall.VolleyMultipartRequest;
import com.stembuddy.apicall.onUpdateViewListener;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URI;
import java.util.Calendar;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

/**
 * Created by THAKUR on 12/7/2017.
 */

public class Edit_profile extends Fragment implements View.OnClickListener {
    private ImageView img_upload;
    private EditText mobile, profession, speciality, discription, name;
    private Button btn_submit;
    private Bitmap filephoto;
    private static final int request_camera = 1888;
    private static final int PICK_IMAGE_REQUEST = 1;
    private File photo;
    private String user_email, u_User_Type;
    private EditText uName, uPass, uMobile, uGender, uAddress;
    private TextView uEmail;
    private Uri selected_image;
    private Uri uriFilePath;
    private int columnindex;
    private String file_path = "";

    private SharedPreference_Main sharedPreference_main;

    private View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.edit_profile, container, false);

        sharedPreference_main = SharedPreference_Main.getInstance(getActivity());
        img_upload = (ImageView) v.findViewById(R.id.upload_img);
        mobile = (EditText) v.findViewById(R.id.upload_mobile);
        profession = (EditText) v.findViewById(R.id.upload_professtion);
        speciality = (EditText) v.findViewById(R.id.upload_speciality);
        discription = (EditText) v.findViewById(R.id.upload_desc);
        name = (EditText) v.findViewById(R.id.name);
        img_upload.setOnClickListener(this);
        btn_submit = (Button) v.findViewById(R.id.update_profile);

        mobile.setText(sharedPreference_main.getUserMobile());
        profession.setText(sharedPreference_main.getProfession());
        speciality.setText(sharedPreference_main.getSpeciality());
        discription.setText(sharedPreference_main.getDescription());
        name.setText(sharedPreference_main.getName());

        btn_submit.setOnClickListener(this);


        return v;

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.upload_img) {
            openDialog();

        } else if (view.getId() == R.id.update_profile) {
            String mobile_no = mobile.getText().toString().trim();
            String profess = profession.getText().toString().trim();
            String specility = speciality.getText().toString().trim();
            String desc = discription.getText().toString().trim();
            if (checkValidity(mobile_no, profess, specility, desc)) {
                uploadImageTextApi(mobile_no, profess, specility, desc);

            }
        }

    }


    private void uploadImageTextApi(String mobile_no, String profess, String speciltiy, String desc) {
        Toast.makeText(getActivity(), "Image Uploaded!", Toast.LENGTH_LONG).show();
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.show();
        pd.setMessage("Uploading....");
// Volley MultipartRequest==========================================
        VolleyMultipartRequest request = new VolleyMultipartRequest(Constants.BASE_URL, new UpdateMultipartListener(getActivity(), new onUpdateViewListener() {
            @Override
            public void updateView(Object responseObject, boolean isSuccess, int reqType) {
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(responseObject));
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), "Image successfully uploaded!", Toast.LENGTH_LONG).show();
                        JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                        sharedPreference_main.setuserMobile(jsonObject1.getString("mobile"));
                        sharedPreference_main.setUserProfile(jsonObject1.getString("image"));
                        Toast.makeText(getActivity(), "done", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Image Not uploaded!", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, 1), FileUploadModel.class, "file", photo, getParams(mobile_no, profess, speciltiy, desc));

        int socketTime = 20000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
//        requestQueue.add(request);
        AppController.getInstance().addToRequestQueue(request);

    }

    private HashMap<String, String> getParams(String mobile_no, String profess, String speciltiy, String desc) {
        HashMap<String, String> params = new HashMap<>();
        params.put("rule", "profile");
        params.put("mobile", mobile_no);
        params.put("profession", profess);
        params.put("specility", speciltiy);
        params.put("description", desc);
        params.put("user_id", sharedPreference_main.getUserId());
        return params;
    }

    private boolean checkValidity(final String mobile_no, String profess, String specility, String desc) {
        if (TextUtils.isEmpty(mobile_no)) {
            mobile.setError("Enter mail");
            return false;
        } else if (TextUtils.isEmpty(profess)) {
            profession.setError("Enter mail");
            return false;
        } else if (TextUtils.isEmpty(specility)) {
            profession.setError("Enter mail");
            return false;
        } else {
            return true;
        }

    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.uplaod_image);
        final TextView gallary = dialog.findViewById(R.id.gallary);
        TextView camera = dialog.findViewById(R.id.camera);
        dialog.show();

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
                dialog.dismiss();
            }
        });

        gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChosser();
                dialog.dismiss();
            }
        });

    }

    private void openCamera() {
        PackageManager packageManager = getActivity().getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            File mainDirectory = new File(Environment.getExternalStorageDirectory(), "MyFolder/tmp");
            if (!mainDirectory.exists())
                mainDirectory.mkdirs();
            Calendar calendar = Calendar.getInstance();
            uriFilePath = Uri.fromFile(new File(mainDirectory, "IMG" + calendar.getTimeInMillis() + ".jpg"));
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriFilePath);
            startActivityForResult(intent, request_camera);
        }
    }

    private void showFileChosser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select a File Here !"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                selected_image = data.getData();
                filephoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selected_image);
                imgSet();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(selected_image, filePathColumn, null, null, null);
                columnindex = cursor.getColumnIndex(filePathColumn[0]);
                cursor.moveToFirst();
                file_path = cursor.getString(columnindex);
                photo = new File(new URI("file://" + file_path.replace(" ", "%20")));
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == request_camera) {
            try {

                ExifInterface exif = new ExifInterface(uriFilePath.getPath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);
                Matrix matrix = new Matrix();

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.postRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.postRotate(270);
                        break;
                }
                String uri = uriFilePath.getPath();
                Glide.with(this).load(uri).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_upload) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(Edit_profile.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img_upload.setImageDrawable(circularBitmapDrawable);
                    }
                });
//            Glide.with(this).load(uriFilePath).placeholder(R.mipmap.profile_pic).into(img_profile);
//                Glide.with(this).load(uriFilePath).placeholder(R.mipmap.prfile_bg).into(img_back);

                photo = new File(new URI("file://" + uri.replace(" ", "%20")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void imgSet() {
        Toast.makeText(getActivity(), "camera imsge set", Toast.LENGTH_LONG).show();

        img_upload.setImageBitmap(filephoto);
    }
}
