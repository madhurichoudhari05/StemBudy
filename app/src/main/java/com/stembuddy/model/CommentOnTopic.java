package com.stembuddy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentOnTopic {
    @SerializedName("response")
    @Expose
    private Response__ response;
    @SerializedName("topic_details")
    @Expose
    private TopicDetails topicDetails;
    @SerializedName("mentor_data")
    @Expose
    private MentorData__ mentorData;

    public Response__ getResponse() {
        return response;
    }

    public void setResponse(Response__ response) {
        this.response = response;
    }

    public TopicDetails getTopicDetails() {
        return topicDetails;
    }

    public void setTopicDetails(TopicDetails topicDetails) {
        this.topicDetails = topicDetails;
    }

    public MentorData__ getMentorData() {
        return mentorData;
    }

    public void setMentorData(MentorData__ mentorData) {
        this.mentorData = mentorData;
    }


}
