package com.stembuddy.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class StumBud_moreInfo extends Fragment {
    private View v;
    private Bundle dataFromPreviousFrag;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.stum_buds_more_info, container, false);

        dataFromPreviousFrag = new Bundle();
        dataFromPreviousFrag = getArguments();
        try {
            Glide.with(getActivity()).load(getArguments().getString("photo"))
                    .placeholder(R.drawable.profile).transform(new CircleTransform(getActivity()))
                    .into((ImageView) v.findViewById(R.id.image));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) v.findViewById(R.id.name)).setText(getArguments().getString("name"));

        if (!getArguments().getString("profession").equals("")) {
            ((TextView) v.findViewById(R.id.profession)).setText(getArguments().getString("profession"));
        } else {
            ((TextView) v.findViewById(R.id.profession)).setText("Not Mentioned");
        }

        if (!getArguments().getString("affilation").equals("")) {
            ((TextView) v.findViewById(R.id.affilation)).setText(getArguments().getString("affilation"));
        } else {
            ((TextView) v.findViewById(R.id.affilation)).setText("Not Mentioned");
        }

        if (!getArguments().getString("speciality").equals("")) {
            ((TextView) v.findViewById(R.id.speciality)).setText(getArguments().getString("speciality"));
        } else {
            ((TextView) v.findViewById(R.id.speciality)).setText("Not Mentioned");
        }

        if (!getArguments().getString("description").equals("")) {
            ((TextView) v.findViewById(R.id.desc)).setText(getArguments().getString("description"));
        } else {
            ((TextView) v.findViewById(R.id.desc)).setText("Description: Not Mentioned");
        }

        try {
            if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                v.findViewById(R.id.rate).setVisibility(View.VISIBLE);
                v.findViewById(R.id.view_info).setVisibility(View.GONE);

                if (getArguments().getString("rating").equals("")) {
                    ((RatingBar) v.findViewById(R.id.rate)).setRating(Float.parseFloat("0"));
                } else {
                    ((RatingBar) v.findViewById(R.id.rate)).setRating(Float.parseFloat(getArguments().getString("rating")));
                }
            } else {
                v.findViewById(R.id.rate).setVisibility(View.GONE);
                v.findViewById(R.id.view_info).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReviewActivtiy reviewActivtiy = new ReviewActivtiy();
                reviewActivtiy.setArguments(dataFromPreviousFrag);
                ((MainActivity) getActivity()).loadFragment(reviewActivtiy, "Student_more_info", view);

            }
        });

        return v;
    }
}
