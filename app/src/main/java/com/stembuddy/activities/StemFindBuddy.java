package com.stembuddy.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.MainHome_Fragment;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;
import com.stembuddy.utils.KeyboardUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class StemFindBuddy extends Fragment implements View.OnClickListener {
    private View v;
    private ArrayList<String> arrayList;
    private EditText search_stem;
    private Button btn_back;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.stem_find_buddy, container, false);
        arrayList = new ArrayList<>();
//        hitShowFavApi();
        v.findViewById(R.id.find_buddy_button).setOnClickListener(this);

        search_stem = v.findViewById(R.id.search_stem);
        btn_back = v.findViewById(R.id.btn_back);
        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new MainHome_Fragment(), "Home", view);

            }
        });
        KeyboardUtils.addKeyboardToggleListener(getActivity(), new KeyboardUtils.SoftKeyboardToggleListener() {
            @Override
            public void onToggleSoftKeyboard(boolean isVisible) {
                if (isVisible) {
                    Log.d("keyboard", "keyboard visible: " + isVisible);
                    btn_back.setVisibility(View.GONE);

                } else {
                    btn_back.setVisibility(View.VISIBLE);
                }
            }
        });
//        search_stem.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                try {
//                    search_stem.setFocusableInTouchMode(true);
//                    search_stem.requestFocus();
//                    if (keyEvent.getAction() == keyEvent.KEYCODE_ENTER || keyEvent.getAction() == keyEvent.ACTION_DOWN) {
//                        if (i == EditorInfo.IME_ACTION_DONE || i == keyEvent.ACTION_DOWN) {
//                            if (search_stem.getText().toString().trim().equalsIgnoreCase(""))
//                                Toast.makeText(getActivity(), "Please fill necessary Fields", Toast.LENGTH_SHORT).show();
//                        } else {
//                            ((BaseActivity) getActivity()).hideSoftKeyBoard(textView);
//                            hitShowFavApi();
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return true;
//            }
//        });

        return v;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.find_buddy_button:
                ((BaseActivity) getActivity()).hideSoftKeyBoard(view);
                if (!((EditText) v.findViewById(R.id.search_stem)).getText().toString().equals("")) {
                    StemBuddyFindResult stemBuddyFindResult = new StemBuddyFindResult();
                    Bundle b = new Bundle();
                    b.putString("name_value", ((EditText) v.findViewById(R.id.search_stem)).getText().toString());
                    b.putStringArrayList("block_user_list", arrayList);
                    stemBuddyFindResult.setArguments(b);
                    ((MainActivity) getActivity()).loadFragment(stemBuddyFindResult, "StemBuddyFindResult()", view);
                } else {
                    ((BaseActivity) getActivity()).showToast("Search filed is blank");
                }
                break;
            default:
                break;
        }
    }

    private void hitShowFavApi() {
        String url = Constants.BASE_URL;
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    arrayList = new ArrayList<>();
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("res").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("message");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            JSONObject object = jsonObject.getJSONObject("Mentor");
                            arrayList.add(object.getString("id"));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "all_fav_and_block_by_student");
                } else {
                    params.put("rule", "all_fav_and_block_by_mentor");
                }
                params.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                params.put("status", "0"); //1 for getting unblock favourtes.....0 for getting blocked contacts
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "get_blocked_user");
    }

}
