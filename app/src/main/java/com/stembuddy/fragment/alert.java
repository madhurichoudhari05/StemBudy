package com.stembuddy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.TopDiscussion2;
import com.stembuddy.adapter.NotificationListAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.BlockUserModel;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by THAKUR on 11/27/2017.
 */

public class alert extends Fragment {
    private ListView listView;
    private ArrayList<BlockUserModel> list;
    public static ArrayList<String> ids_req;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alert, container, false);
        listView = (ListView) view.findViewById(R.id.noti_list);
        if (((BaseActivity) getActivity()).isNetworkEnabled(getActivity()))
            first_run();
        else
            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (list.get(i).getFor_what().equalsIgnoreCase("There is new comment on topic")) {
                    TopDiscussion2 topDiscussion2 = new TopDiscussion2();
                    Bundle bundle = new Bundle();
                    bundle.putString("posts_id", list.get(i).getIds());
                    topDiscussion2.setArguments(bundle);
                    ((MainActivity) getActivity()).loadFragment(topDiscussion2, "Top_discussion", view);
                } else {
                    if (list.get(i).getFor_what().equalsIgnoreCase("There is new comment on your profile")) {
                        All_comments_On_Profile all_comments_on_profile = new All_comments_On_Profile();
                        Bundle bundle = new Bundle();
                        bundle.putString("comments_ids", list.get(i).getIds());
                        all_comments_on_profile.setArguments(bundle);
                        ((MainActivity) getActivity()).loadFragment(all_comments_on_profile, "Comment_on_profile", view);
                    }
                }
            }
        });
        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new FeatureHome_Fragment(), "feature_home", view);

            }
        });
        return view;
    }

    private void first_run() {
        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            list = new ArrayList<>();
                            ids_req = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("data");
                                    JSONObject jsonObject2;
                                    if (SharedPreference_Main.getInstance(getActivity()).getUserType().equalsIgnoreCase("student")) {
                                        jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("mentor_data");
                                    } else {
                                        jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("student_data");
                                    }
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setIds(jsonObject1.getString("id"));
                                    ids_req.add(jsonObject2.getString("id"));
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("add_time"));
                                    blockUserModel.setFor_what("There is new request send to you");
                                    list.add(blockUserModel);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hitApi1();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equalsIgnoreCase("student")) {
                    map.put("rule", "get_all_notification_student");
                } else {
                    map.put("rule", "get_all_notification_mentor");
                }
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    private void hitApi1() {
//        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("data");
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("user_data");
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setIds(jsonObject1.getString("topic_id"));
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("date_time"));
                                    blockUserModel.setFor_what("There is new comment on topic");
                                    list.add(blockUserModel);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hitApi2();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "stem_dis_tpc_comment_noti");
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void hitApi2() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("data");
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("from_notifi");
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setIds(jsonObject1.getString("from_id"));
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("date_time"));
                                    blockUserModel.setFor_what("There is new comment on your profile");
                                    list.add(blockUserModel);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hitApi3();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "stem_comment_nofication");
                map.put("to_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void hitApi3() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        ((MainActivity) getActivity()).dismissLoader();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("data");
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("from_notifi");
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("date_time"));
                                    blockUserModel.setFor_what("There is new video share");
                                    blockUserModel.setIds(jsonObject1.getString("video_id"));
                                    list.add(blockUserModel);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            List lst = new ArrayList();
                            for (int i = 0; i < list.size(); i++) {
                                lst.add(list.get(i).getTime());
                            }

                            Collections.sort(lst, new Comparator<String>() {
                                @Override
                                public int compare(String o1, String o2) {
                                    try {
                                        return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));
                                    } catch (ParseException e) {
                                        return 0;
                                    }
                                }
                            });

                            ArrayList<BlockUserModel> userArrayList = new ArrayList<>();
                            for (int i = 0; i < lst.size(); i++) {
                                if (lst.get(0).equals(list.get(i).getTime())) {
                                    userArrayList.add(list.get(i));
                                    lst.remove(0);
                                    i = 0;
                                }
                            }

                            if (userArrayList.size() != 0) {
                                NotificationListAdapter notificationListAdapter = new NotificationListAdapter(list, getActivity());
                                listView.setAdapter(notificationListAdapter);
                            } else {
                                Toast.makeText(getActivity(), "No notification yet", Toast.LENGTH_SHORT).show();

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((MainActivity) getActivity()).dismissLoader();
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "stem_send_video_noti");
                map.put("shareto", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
