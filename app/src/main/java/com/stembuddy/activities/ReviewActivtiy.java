package com.stembuddy.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.adapter.ReviewListAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.Review_List_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class ReviewActivtiy extends Fragment implements View.OnClickListener {

    private View v;
    private SharedPreference_Main sharedPreference_main;
    private ArrayList<Review_List_Model> review_arrylist;
    private ReviewListAdapter reviewListAdapter;
    private ListView review_list;
    private String rating_bar = "";
    private Bundle dataFromPreviousFrag;
    private String TAG="ReviewActivtiy";

    String status="",mobileNumber="";
    private Intent sinch_service;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.review_page_layout, container, false);

        sharedPreference_main = SharedPreference_Main.getInstance(getActivity());
        review_list = v.findViewById(R.id.list_review);
        review_arrylist = new ArrayList<>();

        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        hitShowReviewApi();




        dataFromPreviousFrag = new Bundle();
        dataFromPreviousFrag = getArguments();

        status = getArguments().getString("online_offline");
        TextView textView = (TextView) v.findViewById(R.id.statusTxt);
        if (status.equals("1")) {
            textView.setText("Online");
        } else if (status.equals("0")) {
            textView.setText("Offline");
        }


        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new Stem_Buds(), "Stem_Bud", view);
            }
        });

        try {
            Glide.with(getActivity()).load(getArguments().getString("photo"))
                    .placeholder(R.drawable.profile).transform(new CircleTransform(getActivity()))
                    .into((ImageView) v.findViewById(R.id.image));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) v.findViewById(R.id.name)).setText(getArguments().getString("name"));

        if (!getArguments().getString("profession").equals("")) {
            ((TextView) v.findViewById(R.id.profession)).setText(getArguments().getString("profession"));
        } else {
            ((TextView) v.findViewById(R.id.profession)).setText("Not Mentioned");
        }

        if (!getArguments().getString("affilation").equals("")) {
            ((TextView) v.findViewById(R.id.affilation)).setText(getArguments().getString("affilation"));
        } else {
            ((TextView) v.findViewById(R.id.affilation)).setText("Not Mentioned");
        }

        if (!getArguments().getString("speciality").equals("")) {
            ((TextView) v.findViewById(R.id.speciality)).setText(getArguments().getString("speciality"));
        } else {
            ((TextView) v.findViewById(R.id.speciality)).setText("Not Mentioned");
        }

        try {
            if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                v.findViewById(R.id.rating).setVisibility(View.VISIBLE);
                v.findViewById(R.id.view_info).setVisibility(View.GONE);

                if (getArguments().getString("rating").equals("")) {
                    ((RatingBar) v.findViewById(R.id.rating)).setRating(Float.parseFloat("0"));
                } else {
                    ((RatingBar) v.findViewById(R.id.rating)).setRating(Float.parseFloat(getArguments().getString("rating")));
                }
            } else {
                v.findViewById(R.id.rating).setVisibility(View.GONE);
                v.findViewById(R.id.view_info).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (sharedPreference_main.getUserType().equals("student")) {
            v.findViewById(R.id.linear_innfo).setVisibility(View.VISIBLE);
            v.findViewById(R.id.review_more_info1).setVisibility(View.GONE);
            v.findViewById(R.id.linear_review).setVisibility(View.VISIBLE);
        } else {
            v.findViewById(R.id.linear_innfo).setVisibility(View.GONE);
            v.findViewById(R.id.review_more_info1).setVisibility(View.VISIBLE);
            v.findViewById(R.id.linear_review).setVisibility(View.GONE);
        }



        v.findViewById(R.id.review_more_info).setOnClickListener(this);
        v.findViewById(R.id.review_more_info1).setOnClickListener(this);
        v.findViewById(R.id.review_leave_comment).setOnClickListener(this);
        v.findViewById(R.id.review_rating).setOnClickListener(this);
        v.findViewById(R.id.sms_send).setOnClickListener(this);
        v.findViewById(R.id.mobile_icon).setOnClickListener(this);
        v.findViewById(R.id.video_call_icon).setOnClickListener(this);
        v.findViewById(R.id.email_icon).setOnClickListener(this);
        return v;


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.review_leave_comment:
                openCommentDialog();
                break;
            case R.id.review_more_info:
                showCompleteInfo(view);
                break;
            case R.id.review_more_info1:
                showCompleteInfo(view);
                break;
            case R.id.review_rating:
                openRatingBarDialog();
                break;
            case R.id.mobile_icon:
                if (getArguments().getString("mobile").equals("")) {
                    showAlertDialog("You can\'t call to your Buddy because your Buddy has not mentioned any Mobile Number for Call.");
                } else {
                    Intent intent1 = new Intent(Intent.ACTION_CALL);
                    intent1.setData(Uri.parse("tel:" + getArguments().getString("mobile")));
                    startActivity(intent1);
                   // callIntent.setData(Uri.parse("tel:7210569138"));
                    try {
                        startActivity(intent1);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "No App Found To Perform This Operation", Toast.LENGTH_SHORT).show();
                    }

                }


              /*  Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:7210569138"));
                startActivity(callIntent);*/
                break;
            case R.id.email_icon:
                if (getArguments().getString("email").equals("")) {
                    showAlertDialog("You can\'t send any email to your Buddy because your Buddy has not mentioned any Email Address to send mail");
                } else {
                    Intent intent3 = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + getArguments().getString("email")));
                    startActivity(Intent.createChooser(intent3, "Send Email"));
                }
                break;
            case R.id.sms_send:
                if (getArguments().getString("mobile").equals("")) {
                    showAlertDialog("You can\'t send any message to your Buddy because your Buddy has not mentioned any Mobile Number to send messages");
                } else {
                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
                    intent1.setData(Uri.parse("sms:" + getArguments().getString("mobile")));
                    try {
                        startActivity(intent1);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "No App Found To Perform This Operation", Toast.LENGTH_SHORT).show();
                    }

                }
//                Intent intent = new Intent(getActivity(), ChatActivity.class);
//                intent.putExtra("KEY_USER_ID", getArguments().getString("id"));
//                intent.putExtra("KEY_FULLNAME", getArguments().getString("name"));
//                startActivity(intent);
//                getActivity().finishAffinity();
//                }
                break;
            case R.id.video_call_icon:

                checkGogleduo();

              /*  Log.e("opponent_id",getArguments().getString("id"));

                Intent i1 = new Intent(getActivity(), VideoCallingActivity.class);
                i1.putExtra("name", getArguments().getString("name"));
                i1.putExtra("phone", getArguments().getString("mobile"));
                i1.putExtra("opponent_id", getArguments().getString("id"));
              //  i1.putExtra(SinchService.CALL_ID, "id");
                i1.putExtra("photo", getArguments().getString("photo"));
                i1.putExtra("calling", "video");
                startActivity(i1);*/



//                Call call = ((BaseActivity) getActivity()).getSinchServiceInterface().callUserVideo(getArguments().getString("name"));
//                String callId = call.getCallId();
//
//                Intent callScreen = new Intent(getActivity(), CallScreenActivity.class);
//                callScreen.putExtra(SinchService12.CALL_ID, callId);
//                startActivity(callScreen);
                break;
            default:
                break;
        }
    }

    private void checkGogleduo() {

        boolean isAppInstalled = appInstalledOrNot("com.google.android.apps.tachyon");
       // boolean isAppInstalled = appInstalledOrNot("com.whatsapp");

        if(isAppInstalled) {
            //This intent will help you to launch if the package is already installed
         //   Toast.makeText(getActivity(), "Application is already installed.", Toast.LENGTH_SHORT).show();
            Intent LaunchIntent = getActivity().getPackageManager()
                    .getLaunchIntentForPackage("com.google.android.apps.tachyon");
                   // .getLaunchIntentForPackage("com.whatsapp");
            startActivity(LaunchIntent);

            Log.i(TAG,"Google Duo App is already installed.");

        } else {


            showAlertDialog("GoogleDuo App is not currently installed.");

            // Do whatever we want to do if application not installed
            // For example, Redirect to play store
          //  Toast.makeText(getActivity(), "Application is not  installed.", Toast.LENGTH_SHORT).show();
           // Log.i(TAG,"Application is not currently installed.");
        }
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }


    private void showAlertDialog(String s) {
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        ab.setMessage(s);
        ab.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        ab.show();
    }

    private void showCompleteInfo(View view) {
        StumBud_moreInfo stumBud_moreInfo = new StumBud_moreInfo();
//        Bundle b = new Bundle();
//        b.putString("rating", getArguments().getString("rating"));
//        b.putString("name", getArguments().getString("name"));
//        dsbvr b.putString("photo", getArguments().getString("photo"));
//        b.putString("profession", getArguments().getString("profession"));
//        b.putString("affilation", getArguments().getString("affilation"));
//        b.putString("speciality", getArguments().getString("speciality"));
//        b.putString("description", getArguments().getString("description"));
        stumBud_moreInfo.setArguments(dataFromPreviousFrag);
        ((MainActivity) getActivity()).loadFragment(stumBud_moreInfo, "Student_more_info", view);
    }

    private void openRatingBarDialog() {
        final Dialog dialog1 = new Dialog(getActivity());
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.rate_stem_buds_dialog);
        dialog1.setCancelable(false);
        dialog1.show();

        dialog1.findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });

        dialog1.findViewById(R.id.d_rate_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hitRatingBarAPi(((RatingBar) dialog1.findViewById(R.id.d_rating)).getRating(), dialog1);
            }
        });
    }

    private void openCommentDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.leave_comment_dialog);
        dialog.setCancelable(false);
        dialog.show();

        dialog.findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.d_btn_comment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = ((EditText) dialog.findViewById(R.id.d_comment)).getText().toString();
                if (s.equals("")) {
                    ((BaseActivity) getActivity()).showToast("Please enter your comment first");
                } else {
                    hitCommentApi(s, dialog);
                }
            }
        });
    }

    private void hitCommentApi(final String s, final Dialog dialog) {
        String url = Constants.BASE_URL;
        ((BaseActivity) getActivity()).showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ((BaseActivity) getActivity()).dismissLoader();

                    Log.v("CommentReview",response);
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray arry = jsonO.getJSONArray("response");
                        for (int i = 0; i < arry.length(); i++) {
                            JSONObject object = arry.getJSONObject(i);

                            Review_List_Model model = new Review_List_Model();
                            model.setComment(object.getString("comment"));
                            model.setCommented_date(object.getString("added_time"));
                            model.setPhoto(sharedPreference_main.getUserProfile());
                            model.setName(sharedPreference_main.getName());
                            model.setRating(rating_bar);
                            review_arrylist.add(model);
                            Collections.reverse(review_arrylist);

//                            if(review_arrylist.size() == 1){
                            reviewListAdapter = new ReviewListAdapter(getActivity(), review_arrylist);
                            review_list.setAdapter(reviewListAdapter);
                            setListViewHeightBasedOnItems(review_list);


                           // reviewListAdapter.notifyDataSetChanged();
//                            }else {
//                                reviewListAdapter.notifyDataSetChanged();
//                            }
                        }
                        ((BaseActivity) getActivity()).showToast("You have successfully commented on your Buddy");
                        dialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "comment_insert");
                    params.put("user_type", sharedPreference_main.getUserType());
                    params.put("from_comment", sharedPreference_main.getUserId());
                    params.put("to_comment", getArguments().getString("id"));
                    params.put("comment", s);
                }
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "leave_comment");
    }


    private void hitRatingBarAPi(final float rating, final Dialog dialog1) {
        String url = Constants.BASE_URL;
        ((BaseActivity) getActivity()).showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ((BaseActivity) getActivity()).dismissLoader();
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        ((BaseActivity) getActivity()).showToast("You have successfully rated your Buddy");
                        rating_bar = String.valueOf(rating);
                        dialog1.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "reviews_insert");
                params.put("from_reviews", sharedPreference_main.getUserId());
                params.put("to_reviews", getArguments().getString("id"));
                params.put("reviews", String.valueOf(rating));
                params.put("user_type", sharedPreference_main.getUserType());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "rating_bar");
    }

    private void hitShowReviewApi() {
        String url = Constants.BASE_URL;
        ((BaseActivity) getActivity()).showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ((BaseActivity) getActivity()).dismissLoader();
                    Log.e("hitShowReviewApi",response);
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i <array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            JSONObject jsonObject = object.getJSONObject("to_data");
                            JSONObject jsonOb = object.getJSONObject("from_data");
                            JSONObject responseData = object.getJSONObject("response");

                            Review_List_Model model = new Review_List_Model();
                            model.setComment(responseData.getString("comment"));
                            model.setCommented_date(jsonObject.getString("created_date"));
                            model.setPhoto(jsonOb.getString("image"));
                            model.setName(jsonOb.getString("name"));
                            model.setRating(jsonOb.getString("rating_bar"));

                            review_arrylist.add(model);
                            //Toast.makeText(getActivity(), "ohhk"+review_arrylist.size(), Toast.LENGTH_SHORT).show();
                            Collections.reverse(review_arrylist);

                        }

                        reviewListAdapter = new ReviewListAdapter(getActivity(), review_arrylist);
                        review_list.setAdapter(reviewListAdapter);
                        setListViewHeightBasedOnItems(review_list);
                    } else {
                        v.findViewById(R.id.no_found).setVisibility(View.VISIBLE);
                        review_list.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "get_all_comment");
                params.put("user_type", sharedPreference_main.getUserType());
                params.put("from_comment", sharedPreference_main.getUserId());
                params.put("to_comment", getArguments().getString("id"));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "review");
    }

    private boolean setListViewHeightBasedOnItems(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int numberOfItems = listAdapter.getCount();
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }
            int totalDividersHeight = listView.getDividerHeight() * (numberOfItems - 1);
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;
        } else {
            return false;
        }
    }

    private class PhoneCallListener extends PhoneStateListener {

        private boolean isPhoneCalling = false;

        String LOG_TAG = "LOGGING 123";

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(LOG_TAG, "OFFHOOK");

                isPhoneCalling = true;
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // run when class initial and phone call ended,
                // need detect flag from CALL_STATE_OFFHOOK
                Log.i(LOG_TAG, "IDLE");

                if (isPhoneCalling) {

                    Log.i(LOG_TAG, "restart app");

                    // restart app
                    Intent i = getActivity().getPackageManager()
                            .getLaunchIntentForPackage(
                                    getActivity().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                    isPhoneCalling = false;
                }

            }
        }
    }

}


