package com.stembuddy.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.VidioesActivity;
import com.stembuddy.adapter.BlockUserAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.Notification_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish on 12/15/2017.
 */

public class BlockUserList extends Fragment {
    private ListView block_list;
    private ArrayList<Notification_Model> aList;
    private View v;
    private BlockUserAdapter blockUserAdapter;
    private LinearLayout layout_block;
    private Button profile_back;
    private int count = 0;
    private TextView text_no_result_found;
    private ArrayList<String> block_ids;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.block_user_list, container, false);

        block_list = (ListView) view.findViewById(R.id.block_user_list);
        layout_block = view.findViewById(R.id.layout_block);
        profile_back = view.findViewById(R.id.profile_back);
        text_no_result_found = view.findViewById(R.id.text_no_result_found);
        aList = new ArrayList<>();

//        for(int i=0 ; i<2 ; i++) {
//        hitShowFavApi(String.valueOf(0), 1);
//        }
        //  getData();

        if (getArguments().getString("from_activity").equalsIgnoreCase("Block_user_list")) {
            layout_block.setVisibility(View.VISIBLE);
            profile_back.setVisibility(View.VISIBLE);
            count = 0;
            hitShowFavApi(String.valueOf(0), 1);
        } else if (getArguments().getString("from_activity").equalsIgnoreCase("Block_user_list_after_block")) {
            layout_block.setVisibility(View.VISIBLE);
            profile_back.setVisibility(View.VISIBLE);
            count = 0;
            hitShowFavApi(String.valueOf(0), 2);
        } else {
            layout_block.setVisibility(View.GONE);
            profile_back.setVisibility(View.VISIBLE);
            count = 1;
            hitShowFavApi(String.valueOf(1), 1);
        }
        view.findViewById(R.id.profile_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!getArguments().getString("from_activity").equalsIgnoreCase("share_video")) {
                    ((MainActivity) getActivity()).loadFragment(new FeatureHome_Fragment(), "feature_home", view);



                }
                else {
                    VidioesActivity vidioesActivity1 = new VidioesActivity();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("from", "Videos_Share");
                    vidioesActivity1.setArguments(bundle1);
                    ((MainActivity) getActivity()).loadFragment(vidioesActivity1, "Videos_activity", view);
                }

            }
        });
        block_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (!getArguments().getString("from_activity").equalsIgnoreCase("Block_user_list")) {
                    if (!getArguments().getString("from_activity").equalsIgnoreCase("Block_user_list_after_block"))
                        hitApiForShareVideo(getArguments().getString("video_id"), aList.get(i).getId());
                }
            }
        });
        return view;
    }

    private void hitShowFavApi(final String s, final int num_val) {
        String url = Constants.BASE_URL;

        if (num_val == 1) {
            ((BaseActivity) getActivity()).showLoader();
        }
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    if (s.equalsIgnoreCase("1")) {
//                        ((BaseActivity) getActivity()).dismissLoader();
//                    }
                    boolean b = true;
                    if (s.equalsIgnoreCase("0")) {
                        ((MainActivity) getActivity()).block_Arraylist = new ArrayList<>();
                        block_ids = new ArrayList<>();
                    }
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
//                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                            JSONObject object = jsonObject.getJSONObject("Mentor");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("details");
                            JSONObject object = jsonObject.getJSONObject("response");
                            if (SharedPreference_Main.getInstance(getActivity()).getUserType().equalsIgnoreCase("student")) {
                                if (jsonObject1.getString("mentor_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = true;
                                } else {
                                    b = false;
                                }
                            } else {
                                if (jsonObject1.getString("student_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = true;
                                } else {
                                    b = false;
                                }
                            }
                            if (b) {
                                Notification_Model model = new Notification_Model();
                                model.setAffiliation(object.getString("affiliation"));
                                model.setId(object.getString("id"));

//                            if (s.equalsIgnoreCase("0")) {
                                if (!getArguments().getString("from_activity").equalsIgnoreCase("share_video")) {
                                    block_ids.add(jsonObject1.getString("id"));
                                    ((MainActivity) getActivity()).block_Arraylist.add(object.getString("id"));
                                }
//                            }
                                model.setBlock_user(s);
                                model.setDescription(object.getString("description"));
                                model.setName(object.getString("name"));
                                model.setComment(object.getString("comment"));
                                model.setEmail(object.getString("email"));
                                model.setUser_type(object.getString("user_type"));
                                model.setSpeciality(object.getString("specility"));
                                model.setRate(object.getString("rating_bar"));
                                model.setMobile(object.getString("mobile"));
                                model.setOnline_offline(object.getString("status"));
                                model.setProfession(object.getString("profession"));
                                model.setPhoto(object.getString("image"));
                                model.setUser_name(object.getString("user_name"));
                                aList.add(model);
                            }
                        }

//                        blockUserAdapter = new BlockUserAdapter(getActivity(), aList, getContext(), getArguments().getString("from_activity"));
//                        block_list.setAdapter(blockUserAdapter);


                    } else {
//                        if (s.equalsIgnoreCase("1")) {
//                            if (aList.size() == 0) {
//                                block_list.setVisibility(View.GONE);
//                                text_no_result_found.setVisibility(View.VISIBLE);
//                            } else {
//                                block_list.setVisibility(View.VISIBLE);
//                                text_no_result_found.setVisibility(View.GONE);
//                            }
//                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (count == 0) {
                        count++;
                        hitShowFavApi("1", 2);
                    } else {

//                        if (aList.size()==0) {
                        if (!getArguments().getString("from_activity").equalsIgnoreCase("share_video")) {
                            count = 0;
                            hitApiGetAccept("0");
                        } else {
                            count = 1;
                            hitApiGetAccept("1");
                        }
//                        }
                    }
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
//                if (s.equalsIgnoreCase("1")) {
                ((BaseActivity) getActivity()).dismissLoader();
//                }
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
//                    params.put("rule", "all_fav_and_block_by_student");
//                } else {
//                    params.put("rule", "all_fav_and_block_by_mentor");
//                }
//                params.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
//                    params.put("status", s); //1 for getting unblock favourtes.....0 for getting blocked contacts
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "accepted_by_student");
                    params.put("student_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                } else {
                    params.put("rule", "accepted_by_mentor");
                    params.put("mentor_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                }
                params.put("stauts", s);
                return params;
            }
        };
        AppController.getInstance().

                addToRequestQueue(sr, "show_fav");
    }

    public void notifyAdapter() {
        blockUserAdapter.notifyDataSetChanged();
    }

    private void hitApiForShareVideo(final String video_id, final String people_id) {
        String url = Constants.BASE_URL;
        ((BaseActivity) getActivity()).showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ((BaseActivity) getActivity()).dismissLoader();
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("flag").equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), "Video Share Successfully", Toast.LENGTH_SHORT).show();

                        hitSend_api(people_id);





                    } else {
                        Toast.makeText(getActivity(), "Video Not Share Successfully", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "send_video");
                params.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                params.put("video_id", video_id);
                params.put("sendto_id", people_id);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "show_fav");
    }

    private void hitApiGetAccept(final String s) {
        String url = Constants.BASE_URL;

        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    if (s.equalsIgnoreCase("0")){
//                        ((MainActivity)getActivity()).block_Arraylist=new ArrayList<>();
//                    }
                    boolean b=true;

                    Log.v("BlockList",response);
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("details");
                            JSONObject object = jsonObject.getJSONObject("response");
                            if (SharedPreference_Main.getInstance(getActivity()).getUserType().equalsIgnoreCase("student")) {
                                if (jsonObject1.getString("mentor_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = true;
                                } else {
                                    b = false;
                                }
                            } else {
                                if (jsonObject1.getString("student_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = true;
                                } else {
                                    b = false;
                                }
                            }
                            if (b) {
                                Notification_Model model = new Notification_Model();
                                model.setAffiliation(object.getString("affiliation"));
                                model.setId(object.getString("id"));
//                            if (s.equalsIgnoreCase("0")) {
                                if (!getArguments().getString("from_activity").equalsIgnoreCase("share_video")) {
                                    block_ids.add(jsonObject1.getString("id"));

                                    ((MainActivity) getActivity()).block_Arraylist.add(object.getString("id"));
                                }
//                            }
                                model.setBlock_user(s);
                                model.setDescription(object.getString("description"));
                                model.setName(object.getString("name"));
                                model.setComment(object.getString("comment"));
                                model.setEmail(object.getString("email"));
                                model.setUser_type(object.getString("user_type"));
                                model.setSpeciality(object.getString("specility"));
                                model.setRate(object.getString("rating_bar"));
                                model.setMobile(object.getString("mobile"));
                                model.setOnline_offline(object.getString("status"));
                                model.setProfession(object.getString("profession"));
                                model.setPhoto(object.getString("image"));
                                model.setUser_name(object.getString("user_name"));
                                aList.add(model);
                            }
                        }


                    } else {
                        if (s.equalsIgnoreCase("1")) {
                            if (aList.size() == 0) {
                                block_list.setVisibility(View.GONE);
                                text_no_result_found.setVisibility(View.VISIBLE);
                            } else {
                                block_list.setVisibility(View.VISIBLE);
                                text_no_result_found.setVisibility(View.GONE);
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (count == 0) {
                        count++;
                        hitApiGetAccept("1");
                    } else {
                        if (aList.size() != 0) {
                            blockUserAdapter = new BlockUserAdapter(getActivity(), aList, getContext(), getArguments().getString("from_activity"), block_ids);
                            block_list.setAdapter(blockUserAdapter);
//                            if (s.equalsIgnoreCase("1")) {

//                            }
                        }
                        ((BaseActivity) getActivity()).dismissLoader();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "accepted_by_mentor_all");
                    params.put("student_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                } else {
                    params.put("rule", "accepted_by_student_all");
                    params.put("mentor_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                }
                params.put("stauts", s);
//                params.put("status", "1"); //1 for getting unblock favourtes.....0 for getting blocked contacts
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "show_fav");
    }
    private void hitSend_api(final String people_id) {
        ((BaseActivity) getActivity()).showLoader();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                ((BaseActivity) getActivity()).dismissLoader();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {

                        /*Madhuri*/
                        Toast.makeText(getActivity(), "Video Share Successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Video Not Share Successfully", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                ((BaseActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "notification_ios");
                params.put("to_id", people_id);
                params.put("from_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                params.put("body", "Someone commented on topic.");
                return params;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(retryPolicy);
        requestQueue.add(sr);
    }



}
