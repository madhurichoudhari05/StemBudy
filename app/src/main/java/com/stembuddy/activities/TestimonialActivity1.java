package com.stembuddy.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.adapter.TestimonialAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.Testimonial_Model;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class TestimonialActivity1 extends Fragment {
    private ListView listview;
    private View v;
    private ArrayList<Testimonial_Model> arrayList;
    private TestimonialAdapter adapter;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.testimonial_page_layout1, container, false);


        get_all_testimonials();
        //     getData();

        listview = v.findViewById(R.id.test_page1_list);
//        adapter = new TestimonialAdapter(getActivity(), arrayList, getContext());
//
//        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        return v;
    }

    public void get_all_testimonials() {

        ((BaseActivity) getActivity()).showLoader();
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) getActivity()).dismissLoader();
                            arrayList = new ArrayList<>();
                            JSONObject jsonO = new JSONObject(response);
                            if (jsonO.getString("status").equals("1")) {
                                JSONArray array = jsonO.getJSONArray("response");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    Testimonial_Model model = new Testimonial_Model();
                                    model.setTesti_by(object.getString("testi_by"));
                                    model.setId(object.getString("id"));
                                    model.setName(object.getString("title"));
                                    model.setDiscription(object.getString("discription"));
                                    model.setPhoto(object.getString("image"));

                                    arrayList.add(model);
                                }
                                adapter = new TestimonialAdapter(getActivity(), arrayList);
                                listview.setAdapter(adapter);

                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "get_all_testimonials");
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr, "get_all_testimonials");
    }


}


//    private void getData() {
//        Bitmap icon;
//
//
//        arrayList = new ArrayList<>();
//        for (int i = 0; i < 9; i++) {
//            Testimonial_Model nm = new Testimonial_Model();
//            icon = BitmapFactory.decodeResource(getResources(), R.drawable.profile);
//            nm.setName("John Carter");
//            nm.setProfe("Chemist");
//            nm.setContent("jhfw fwhf asfjkwh asfhwf asflhf asfasjfha lksjfa sklffja jk Ajkwf asjasfnasf asfkjsf saklfnsc sakns cKNs sVNslACnkdsNC dsvlkvna svlkSv svlsv svnavas v avnvlknvm vlvv sdvklev smv esnvvnsVd vdsklvndsv vlsD Vsvnsvnsdvlsnvlsvns dvlsKNVs vsVNlksnklsnvsklv");
//            nm.setPhoto(icon);
//
//
//            arrayList.add(nm);
//        }
