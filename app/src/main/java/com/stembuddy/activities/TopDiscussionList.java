package com.stembuddy.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.adapter.TopDiscAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.MainHome_Fragment;
import com.stembuddy.model.TopDiscModel;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/30/2017.
 */

public class TopDiscussionList extends Fragment {
    private ListView listView;
    private View v;
    private ArrayList<TopDiscModel> arrayList;
    private TopDiscAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.top_disscussion_list_layout, container, false);

        get_all_discussion_api();
     //    getData();
        listView=v.findViewById(R.id.top_disc_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TopDiscussion2 topDiscussion2 =new TopDiscussion2();
                Bundle bundle = new Bundle();
                bundle.putString("views", arrayList.get(i).getViews());
                bundle.putString("created_date", arrayList.get(i).getCreated_date());
                bundle.putString("title", arrayList.get(i).getTitle());
                bundle.putString("discription", arrayList.get(i).getDescription());
                bundle.putString("posts_id", arrayList.get(i).getId());
                bundle.putString("from_activity", "top_discussion");
                topDiscussion2.setArguments(bundle);
                ((MainActivity)getActivity()).loadFragment(topDiscussion2, "Top_Discussion2" , view);
            }
        });

        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).loadFragment(new MainHome_Fragment(), "Home", view);
            }
        });

        return v;

    }

    public void  get_all_discussion_api(){
        ((BaseActivity) getActivity()).showLoader();
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) getActivity()).dismissLoader();
                            arrayList = new ArrayList<>();
                            JSONObject jsonO = new JSONObject(response);
                            if (jsonO.getString("status").equals("1")) {
                                JSONArray array = jsonO.getJSONArray("response");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    TopDiscModel model = new TopDiscModel();
                                    model.setId(object.getString("id"));
                                    model.setDescription(object.getString("discription"));
                                    model.setTitle(object.getString("title"));
                                    model.setPost(object.getString("posts"));
                                    model.setViews(object.getString("views"));
                                    model.setProfile(object.getString("image"));
                                    model.setCreated_date(object.getString("created_date"));
                                    arrayList.add(model);

                                }
                                adapter = new TopDiscAdapter(getActivity(), arrayList, getContext());
                                listView.setAdapter(adapter);

                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "get_all_discussion_topic");

                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr, "get_all_discussion_topic");
    }
}
