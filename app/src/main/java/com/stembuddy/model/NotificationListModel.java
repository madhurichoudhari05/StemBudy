package com.stembuddy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationListModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("video_noti")
    @Expose
    private List<VideoNotus> videoNoti = null;
    @SerializedName("all_video_noti")
    @Expose
    private List<AllVideoNotus> allVideoNoti = null;
    @SerializedName("comment_on_user")
    @Expose
    private List<CommentOnUser> commentOnUser = null;
    @SerializedName("comment_on_topic")
    @Expose
    private List<CommentOnTopic> commentOnTopic = null;
    @SerializedName("comment_on_mentor")
    @Expose
    private List<CommentOnMentor> commentOnMentor = null;
    @SerializedName("comment_on_student")
    @Expose
    private List<CommentOnStudent> commentOnStudent = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<VideoNotus> getVideoNoti() {
        return videoNoti;
    }

    public void setVideoNoti(List<VideoNotus> videoNoti) {
        this.videoNoti = videoNoti;
    }

    public List<AllVideoNotus> getAllVideoNoti() {
        return allVideoNoti;
    }

    public void setAllVideoNoti(List<AllVideoNotus> allVideoNoti) {
        this.allVideoNoti = allVideoNoti;
    }

    public List<CommentOnUser> getCommentOnUser() {
        return commentOnUser;
    }

    public void setCommentOnUser(List<CommentOnUser> commentOnUser) {
        this.commentOnUser = commentOnUser;
    }

    public List<CommentOnTopic> getCommentOnTopic() {
        return commentOnTopic;
    }

    public void setCommentOnTopic(List<CommentOnTopic> commentOnTopic) {
        this.commentOnTopic = commentOnTopic;
    }

    public List<CommentOnMentor> getCommentOnMentor() {
        return commentOnMentor;
    }

    public void setCommentOnMentor(List<CommentOnMentor> commentOnMentor) {
        this.commentOnMentor = commentOnMentor;
    }

    public List<CommentOnStudent> getCommentOnStudent() {
        return commentOnStudent;
    }

    public void setCommentOnStudent(List<CommentOnStudent> commentOnStudent) {
        this.commentOnStudent = commentOnStudent;
    }

}
