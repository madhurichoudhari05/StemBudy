package com.stembuddy.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.stembuddy.model.ModelScan;

import java.util.ArrayList;

public class DB_Chat extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 3;
    private static final String CHAT_DB = "CHAT_DB";
    private String TABLE_CHAT = "chat_table";
    private static final String SENDERS_ID = "senders_id";
    private static final String RECEIVER_ID = "receiver_id";
    private static final String NAME = "full_name";
    private static final String PROFILE_IMAGE = "profile_image";
    private static final String MESSAGE = "message";
    private static final String TIMESTAMP = "timestamp";
//    private static final String MESSAGE_ID = "message_ID";


    static DB_Chat DB_Chat;
    Context context;
    private ArrayList<ModelScan> arrayAddIncidentModels;

    public DB_Chat(Context context) {
        super(context, CHAT_DB, null, DATABASE_VERSION);
        this.context = context;
    }

    public static synchronized DB_Chat getInstance(Context context) {
        if (DB_Chat == null) {
            DB_Chat = new DB_Chat(context);
        }
        return DB_Chat;
    }

    public long insertDB_ChatDetails(ModelScan modelScan) {
        long isinserted = 0;
        SQLiteDatabase database = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();
            values.put(RECEIVER_ID, modelScan.getRecievers_Id());
            values.put(NAME, modelScan.getMsg_senders_name());
            values.put(PROFILE_IMAGE, modelScan.getMsg_senders_image());
            values.put(MESSAGE, modelScan.getMessage());
            values.put(SENDERS_ID, modelScan.getSenders_Id());
            values.put(TIMESTAMP, System.currentTimeMillis() / 1000 + "");
//            modelScan.getDateTime_ofMsgRecieveed()
//            values.put(MESSAGE_ID, modelScan.getMsg_id());
//            ModelScan.setTimestamp(System.currentTimeMillis() / 1000 + "");

            isinserted = database.insert(TABLE_CHAT, null, values);
            Log.e("DB_Upcming_Rides_status", "" + isinserted);
        } catch (Exception e) {
            Log.e("exception", e + "");
        } finally {
            database.close();
        }

        return isinserted;
    }

    public ArrayList<ModelScan> get_DB_Chat_Details() {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from " + TABLE_CHAT + " ORDER BY " + TIMESTAMP + " DESC", null);
        arrayAddIncidentModels = new ArrayList<>();
        try {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                ModelScan incidentModel = new ModelScan();
                incidentModel.setRecievers_Id(cursor.getString(cursor.getColumnIndex(RECEIVER_ID)));
                incidentModel.setDateTime_ofMsgRecieveed(cursor.getString(cursor.getColumnIndex(TIMESTAMP)));
                incidentModel.setMsg_senders_name(cursor.getString(cursor.getColumnIndex(NAME)));
                incidentModel.setMsg_senders_image(cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE)));
                incidentModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
                incidentModel.setSenders_Id(cursor.getString(cursor.getColumnIndex(SENDERS_ID)));
//                incidentModel.setMsg_id(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
                arrayAddIncidentModels.add(incidentModel);
                cursor.moveToNext();
            }
        } catch (Exception e) {
        } finally {
            cursor.close();
        }
        return arrayAddIncidentModels;
    }

    public ArrayList<ModelScan> get_Chats_By_SenderAndReceiverID(String receipientID) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from " + TABLE_CHAT + " where " + RECEIVER_ID + "='" + receipientID + "' OR " + SENDERS_ID + "='" + receipientID + "'" + " ORDER BY " + TIMESTAMP, null);
        arrayAddIncidentModels = new ArrayList<>();
        try {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                ModelScan incidentModel = new ModelScan();
                incidentModel.setRecievers_Id(cursor.getString(cursor.getColumnIndex(RECEIVER_ID)));
                incidentModel.setDateTime_ofMsgRecieveed(cursor.getString(cursor.getColumnIndex(TIMESTAMP)));
                incidentModel.setMsg_senders_name(cursor.getString(cursor.getColumnIndex(NAME)));
                incidentModel.setMsg_senders_image(cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE)));
                incidentModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
                incidentModel.setSenders_Id(cursor.getString(cursor.getColumnIndex(SENDERS_ID)));
//                incidentModel.setMsg_id(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
                arrayAddIncidentModels.add(incidentModel);
                cursor.moveToNext();
            }
        } catch (Exception e) {
        } finally {
            cursor.close();
        }
        return arrayAddIncidentModels;
    }

    public ArrayList<ModelScan> get_ALL_DISTINCT_IDS(String sender_reciever_id) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from " + TABLE_CHAT + " where " + RECEIVER_ID + "='" + sender_reciever_id + "' OR " + SENDERS_ID + "='" + sender_reciever_id + "'", null);
        arrayAddIncidentModels = new ArrayList<>();
        try {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                ModelScan incidentModel = new ModelScan();
                incidentModel.setRecievers_Id(cursor.getString(cursor.getColumnIndex(RECEIVER_ID)));
                incidentModel.setDateTime_ofMsgRecieveed(cursor.getString(cursor.getColumnIndex(TIMESTAMP)));
                incidentModel.setMsg_senders_name(cursor.getString(cursor.getColumnIndex(NAME)));
                incidentModel.setMsg_senders_image(cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE)));
                incidentModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
                incidentModel.setSenders_Id(cursor.getString(cursor.getColumnIndex(SENDERS_ID)));
//                incidentModel.setMsg_id(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
                arrayAddIncidentModels.add(incidentModel);
//                }
                cursor.moveToNext();
            }
        } catch (Exception e) {
        } finally {
            cursor.close();
        }
        return arrayAddIncidentModels;
    }


    public ModelScan getLastrowChat() {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from " + TABLE_CHAT + " ORDER BY " + TIMESTAMP + " DESC LIMIT 1", null);
        ModelScan incidentModel = new ModelScan();
        try {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                incidentModel.setRecievers_Id(cursor.getString(cursor.getColumnIndex(RECEIVER_ID)));
                incidentModel.setDateTime_ofMsgRecieveed(cursor.getString(cursor.getColumnIndex(TIMESTAMP)));
                incidentModel.setMsg_senders_name(cursor.getString(cursor.getColumnIndex(NAME)));
                incidentModel.setMsg_senders_image(cursor.getString(cursor.getColumnIndex(PROFILE_IMAGE)));
                incidentModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
                incidentModel.setSenders_Id(cursor.getString(cursor.getColumnIndex(SENDERS_ID)));
//                incidentModel.setMsg_id(cursor.getString(cursor.getColumnIndex(MESSAGE_ID)));
                cursor.moveToNext();
            }
        } catch (Exception e) {
        } finally {
            cursor.close();
        }
        return incidentModel;
    }

    public void delete(Context context) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("delete from " + TABLE_CHAT);
        database.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PROD_TABLE = "CREATE TABLE " + TABLE_CHAT + "("
                + RECEIVER_ID + " TEXT,"
                + TIMESTAMP + " TEXT,"
                + MESSAGE + " TEXT,"
                + SENDERS_ID + " TEXT,"
                + NAME + " TEXT,"
//                + MESSAGE_ID + " TEXT,"
                + PROFILE_IMAGE + " TEXT)";
        db.execSQL(CREATE_PROD_TABLE);
        Log.e(TABLE_CHAT, "Table Creadted " + TABLE_CHAT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHAT);
        Log.e(TABLE_CHAT, "DROP");
        onCreate(db);
    }
}