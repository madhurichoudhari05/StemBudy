package com.stembuddy.adapter;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.TestomonialActivity;
import com.stembuddy.model.Testimonial_Model;
import com.stembuddy.utils.CircleTransform;

import java.util.ArrayList;



public class TestimonialAdapter extends BaseAdapter {

    private ArrayList<Testimonial_Model> datalist;
    private LayoutInflater layoutInflater;
    Context context;

    public TestimonialAdapter(Context context, ArrayList<Testimonial_Model> data ) {
        this.datalist = data;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        if(datalist.size()<=0)
            return 1;
        return datalist.size();
    }

    @Override
    public Object getItem(int i) {
        return datalist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

//    public static class ViewHolder{
//        private ImageView img;
//        private TextView name;
//        private TextView profession;
//        private TextView content;
//        private Button readMore;
//    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
//        ViewHolder holder;
//        inflater=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//        View vi=view;
//        if (vi==null) {
//            holder=new ViewHolder();
//            vi=inflater.inflate(R.layout.testimonial_page1_list_row,null);
//            holder.name=vi.findViewById(R.id.test_name);
//            holder.profession=vi.findViewById(R.id.test_profession);
//            holder.img=vi.findViewById(R.id.test_profile);
//            holder.content=vi.findViewById(R.id.test_content);
//
//
//
//        }else
//        {
//            holder=(ViewHolder)view.getTag();
//        }

        View v = layoutInflater.inflate(R.layout.testimonial_page1_list_row, viewGroup, false);
        try {
            Glide.with(context).load(datalist.get(position).getPhoto())
                    .placeholder(R.mipmap.testimonal_profile).transform(new CircleTransform(context))
                    .into((ImageView)v.findViewById(R.id.test_profile));
        }catch(Exception e){
            e.printStackTrace();
        }

        ((TextView)v.findViewById(R.id.test_name)).setText(datalist.get(position).getName());

        if(datalist.get(position).getTesti_by().equals("")){
            ((TextView) v.findViewById(R.id.test_profession)).setText("Not Mentioned");
        }else {
            ((TextView) v.findViewById(R.id.test_profession)).setText(datalist.get(position).getTesti_by());
        }

        if(datalist.get(position).getDiscription().equals("")){
            ((TextView) v.findViewById(R.id.test_content)).setText("Description: Not Mentioned");
        }else {
            ((TextView) v.findViewById(R.id.test_content)).setText(Html.fromHtml(datalist.get(position).getDiscription()));
        }

        v.findViewById(R.id.test_read_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TestomonialActivity testomonialActivity = new TestomonialActivity();
                Bundle b = new Bundle();
                b.putString("name" , datalist.get(position).getName());
                b.putString("photo" , datalist.get(position).getPhoto());
                b.putString("profession" , datalist.get(position).getTesti_by());
                b.putString("desc" , datalist.get(position).getDiscription());
                testomonialActivity.setArguments(b);

                ((MainActivity) context).loadFragment(testomonialActivity, "testimonial_activity", view);
            }
        });

        return v;
    }
}
