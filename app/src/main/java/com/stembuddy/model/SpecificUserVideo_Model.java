package com.stembuddy.model;

/**
 * Created by Himanshu on 1/22/2018.
 */

public class SpecificUserVideo_Model {
    private String video_id, video_name, video_url, video_thumb_image;
    private String serder_id, sender_name;

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_thumb_image() {
        return video_thumb_image;
    }

    public void setVideo_thumb_image(String video_thumb_image) {
        this.video_thumb_image = video_thumb_image;
    }

    public String getSerder_id() {
        return serder_id;
    }

    public void setSerder_id(String serder_id) {
        this.serder_id = serder_id;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }
}
