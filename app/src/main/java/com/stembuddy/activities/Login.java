package com.stembuddy.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CommonUtils;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.stembuddy.apicall.ConnectivityUtils.isNetworkEnabled;

/**
 * Created by THAKUR on 11/22/2017.
 */

public class Login extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener, View.OnKeyListener, TextWatcher{
    private EditText email;
    private EditText pwd;
    private TextView forgot_pwd, forgot_user;
    private ImageView drawe_profile_pic;

    private EditText mPinFirstDigitEditText;
    private EditText mPinSecondDigitEditText;
    private EditText mPinThirdDigitEditText;
    private EditText mPinForthDigitEditText;
    private EditText mPinFifthDigitEditText;
    private EditText mPinSixthDigitEditText;
    private EditText mPinHiddenEditText;
    private SharedPreference_Main sharedPreference_main;
    String s1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        sharedPreference_main = SharedPreference_Main.getInstance(this);

        initView();
        setPINListeners();





        if (sharedPreference_main.getFireBaseTokenid().equalsIgnoreCase("")) {
            sharedPreference_main.setFireBaseTokenid(FirebaseInstanceId.getInstance().getToken());
            //Log.d("deviceMobileToken",FirebaseInstanceId.getInstance().getToken());

            }
        else {

            //Log.d("deviceMobileTokenLElse",sharedPreference_main.getFireBaseTokenid());


        }


        findViewById(R.id.layout_register).setOnClickListener(this);
        findViewById(R.id.login_submit).setOnClickListener(this);
        forgot_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(Login.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.forgot_pwd_layout);
                final EditText et_email = (EditText) dialog.findViewById(R.id.d_forgot_pwd_email);
                Button bt_submit = (Button) dialog.findViewById(R.id.d_forgot_pwd_button);
                dialog.show();
                bt_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String email = et_email.getText().toString().trim();
                        if (!TextUtils.isEmpty(email)) {
                            hitForgotPasswordApi(email);
                        } else {
                            Toast.makeText(Login.this, "Please enter the email", Toast.LENGTH_LONG).show();
                        }
                        dialog.dismiss();
                    }

                    private void hitForgotPasswordApi(final String email) {
                        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                        Toast.makeText(Login.this, "Password has been  successfully sent to your Email id", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(Login.this, "Please Enter registered email", Toast.LENGTH_LONG).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                        ) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("rule", "forgot_password");
                                params.put("email", email);

                                return params;
                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    }


                });
            }
        });


        forgot_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(Login.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.forgot_user_layout);
                final EditText et_email = (EditText) dialog.findViewById(R.id.d_forgot_pwd_email);
                Button bt_submit = (Button) dialog.findViewById(R.id.d_forgot_pwd_button);
                dialog.show();
                bt_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String email = et_email.getText().toString().trim();
                        if (!TextUtils.isEmpty(email)) {
                            hitForgotUsernameApi(email);
                        } else {
                            Toast.makeText(Login.this, "Please enter the email", Toast.LENGTH_LONG).show();
                        }
                        dialog.dismiss();
                    }

                    private void hitForgotUsernameApi(final String email) {
                        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                        Toast.makeText(Login.this, "User name has been  successfully sent to your Email id", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(Login.this, "Please Enter registered email", Toast.LENGTH_LONG).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                        ) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("rule", "forgot_password");
                                params.put("email", email);


                                return params;
                            }
                        };
                        AppController.getInstance().addToRequestQueue(sr);
                    }


                });
            }
        });


//        pwd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                pwd.setFocusableInTouchMode(true);
//                pwd.requestFocus();
//                if (i == EditorInfo.IME_ACTION_DONE || i == keyEvent.ACTION_DOWN) {
//                    if (email.getText().toString().trim().equalsIgnoreCase("") || pwd.getText().toString().trim().equalsIgnoreCase("")) {
//                        showToast("Please fill necessary Fields");
//                    } else {
//                        hideSoftKeyBoard(textView);
//                        onLogin();
//
//                    }
//                }
//                return true;
//            }
//        });


    }

    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);

        mPinFirstDigitEditText.setOnFocusChangeListener(this);
        mPinSecondDigitEditText.setOnFocusChangeListener(this);
        mPinThirdDigitEditText.setOnFocusChangeListener(this);
        mPinForthDigitEditText.setOnFocusChangeListener(this);
        mPinFifthDigitEditText.setOnFocusChangeListener(this);
        mPinSixthDigitEditText.setOnFocusChangeListener(this);

        mPinFirstDigitEditText.setOnKeyListener(this);
        mPinSecondDigitEditText.setOnKeyListener(this);
        mPinThirdDigitEditText.setOnKeyListener(this);
        mPinForthDigitEditText.setOnKeyListener(this);
        mPinFifthDigitEditText.setOnKeyListener(this);
        mPinSixthDigitEditText.setOnKeyListener(this);
        mPinHiddenEditText.setOnKeyListener(this);
    }

    private void initView() {
        email = findViewById(R.id.login_email);
//        pwd=findViewById(R.id.login_pwd);
        forgot_pwd = findViewById(R.id.forgot_pass);
        forgot_user = findViewById(R.id.forgot_user);

        mPinFirstDigitEditText =  findViewById(R.id.pin1);
        mPinSecondDigitEditText =  findViewById(R.id.pin2);
        mPinThirdDigitEditText =  findViewById(R.id.pin3);
        mPinForthDigitEditText =  findViewById(R.id.pin4);
        mPinFifthDigitEditText =  findViewById(R.id.pin5);
        mPinSixthDigitEditText =  findViewById(R.id.pin6);
        mPinHiddenEditText = (EditText) findViewById(R.id.pin_hidden_edittext);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_register:
                startActivity(new Intent(Login.this, Registration_stem.class));
                break;
            case R.id.login_submit:
                if (email.getText().toString().trim().equalsIgnoreCase("")
                        || getPassord().trim().equalsIgnoreCase("")) {
//                    Toast.makeText(this, "Please fill necessary Fields", Toast.LENGTH_SHORT).show();
                } else
                  CommonUtils.hideKeyPad(this);
                onLogin();
                break;
        }
    }

    private void onLogin() {
        String pass = getPassord().trim();
        // Toast.makeText(this,""+pass,Toast.LENGTH_LONG).show();

        String uemail = email.getText().toString().trim();
//        if (checkValidity(uemail, pass)) {
        if (uemail.equals("") && pass.equals("")) {
            Toast.makeText(this, "Please fill all details", Toast.LENGTH_SHORT).show();
        } else {
            if (isNetworkEnabled(Login.this))
                hitLoginApi(uemail, pass);
            else
                Toast.makeText(Login.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
        }
//        }
    }

    private void hitLoginApi(final String uemail, final String upass) {
        final ProgressDialog dialog = new ProgressDialog(Login.this);
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            dialog.dismiss();

                            Log.v("loginResponse",response);
                            JSONObject jsonObject = new JSONObject(response);
                            //Toast.makeText(Login.this, jsonObject.toString(), Toast.LENGTH_SHORT).show();
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                JSONObject regJsonObject = jsonArray.getJSONObject(0);
                                SharedPreference_Main preferenceMain = SharedPreference_Main.getInstance(Login.this);
                                preferenceMain.setUserId(regJsonObject.getString("id"));
                                preferenceMain.setUserType(regJsonObject.getString("user_type"));
                                preferenceMain.setName(regJsonObject.getString("name"));
                                preferenceMain.setEmail(regJsonObject.getString("email"));
                                preferenceMain.setUserPwd(regJsonObject.getString("pass"));
                                preferenceMain.setUserName(regJsonObject.getString("user_name"));
                                preferenceMain.setUserProfile(regJsonObject.getString("image"));
                                preferenceMain.setDescription(regJsonObject.getString("description"));
                                preferenceMain.setProfession(regJsonObject.getString("profession"));
                                preferenceMain.setSpeciality(regJsonObject.getString("specility"));
                                preferenceMain.setSession_login(true);


                                Intent i = new Intent(Login.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            } else {
                                Toast.makeText(Login.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Login.this, "Invalid Email id or password", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(Login.this, "Something went Wrong", Toast.LENGTH_SHORT).show();
                Log.e("VE ", volleyError.getMessage());
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "login");
                params.put("email", uemail);
                params.put("user_name", uemail);
                params.put("pass", upass);
                if (sharedPreference_main.getFireBaseTokenid().equalsIgnoreCase("")) {
                    sharedPreference_main.setFireBaseTokenid(FirebaseInstanceId.getInstance().getToken());
                    params.put("mobile_token",FirebaseInstanceId.getInstance().getToken());
                    Log.d("deviceMobileTokenSIF",FirebaseInstanceId.getInstance().getToken());
                }
                else {
                    params.put("mobile_token",sharedPreference_main.getFireBaseTokenid());
                    Log.d("deviceMobileTokenSELSE",sharedPreference_main.getFireBaseTokenid());


                }
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(sr);
    }

    private boolean checkValidity(final String uemail, String upass) {
        if (TextUtils.isEmpty(uemail)) {
            email.setError("Please Enter mail");
            return false;
        } else if (TextUtils.isEmpty(upass)) {

            pwd.setError("Please Enter Password");
            return false;
        } else {
            return true;
        }

    }

    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.pin1:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin2:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin3:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin4:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin5:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;
            case R.id.pin6:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.pin_hidden_edittext:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (mPinHiddenEditText.getText().length() == 6)
                            mPinSixthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 5)
                            mPinFifthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 4)
                            mPinForthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 3)
                            mPinThirdDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 2)
                            mPinSecondDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 1)
                            mPinFirstDigitEditText.setText("");

                        if (mPinHiddenEditText.length() > 0)
                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));

                        return true;
                    }

                    break;

                default:
                    return false;
            }
        }

        return false;
    }

    /**
     * Sets default PIN background.
     *
     * @param editText edit text to change
     */
    private void setDefaultPinBackground(EditText editText) {
    }

    private void setFocusedPinBackground(EditText editText) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        setDefaultPinBackground(mPinFirstDigitEditText);
        setDefaultPinBackground(mPinSecondDigitEditText);
        setDefaultPinBackground(mPinThirdDigitEditText);
        setDefaultPinBackground(mPinForthDigitEditText);
        setDefaultPinBackground(mPinFifthDigitEditText);

        if (s.length() == 0) {
            setFocusedPinBackground(mPinFirstDigitEditText);
            mPinFirstDigitEditText.setText("");
        } else if (s.length() == 1) {
            setFocusedPinBackground(mPinSecondDigitEditText);
            mPinFirstDigitEditText.setText(s.charAt(0) + "");
            mPinSecondDigitEditText.setText("");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 2) {
            setFocusedPinBackground(mPinThirdDigitEditText);
            mPinSecondDigitEditText.setText(s.charAt(1) + "");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 3) {
            setFocusedPinBackground(mPinForthDigitEditText);
            mPinThirdDigitEditText.setText(s.charAt(2) + "");
            mPinForthDigitEditText.setText("");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");

        } else if (s.length() == 4) {
            setFocusedPinBackground(mPinFifthDigitEditText);
            mPinForthDigitEditText.setText(s.charAt(3) + "");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 5) {
            setDefaultPinBackground(mPinSixthDigitEditText);
            mPinFifthDigitEditText.setText(s.charAt(4) + "");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 6) {
            setDefaultPinBackground(mPinSixthDigitEditText);
            mPinSixthDigitEditText.setText(s.charAt(5) + "");
            hideSoftKeyboard(mPinFifthDigitEditText);
        }
    }


// useless==================================================================================================

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }





    public class MainLayout extends LinearLayout {

        public MainLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.activity_main, this);

        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            final int proposedHeight = MeasureSpec.getSize(heightMeasureSpec);
            final int actualHeight = getHeight();

            Log.d("TAG", "proposed: " + proposedHeight + ", actual: " + actualHeight);

            if (actualHeight >= proposedHeight) {
                // Keyboard is shown
                if (mPinHiddenEditText.length() == 0)
                    setFocusedPinBackground(mPinFirstDigitEditText);
                else
                    setDefaultPinBackground(mPinFirstDigitEditText);
            }

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public String getPassord() {

        String s = "";

        String s1=mPinFirstDigitEditText.getText().toString().trim();
        String s2=mPinSecondDigitEditText.getText().toString().trim();
        String s3=mPinThirdDigitEditText.getText().toString().trim();
        String s4=mPinForthDigitEditText.getText().toString().trim();
        String s5=mPinFifthDigitEditText.getText().toString().trim();
        String s6=mPinSixthDigitEditText.getText().toString().trim();

        s=s1+s2+s3+s4+s5+s6;

        /*s1 += mPinSecondDigitEditText.getText().toString().trim();
        s1 += mPinThirdDigitEditText.getText().toString().trim();
        s1 += mPinForthDigitEditText.getText().toString().trim();
        s1 += mPinFifthDigitEditText.getText().toString().trim();
        s1 += mPinSixthDigitEditText.getText().toString().trim();*/

        return s;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
