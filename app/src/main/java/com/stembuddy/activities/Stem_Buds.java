package com.stembuddy.activities;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.adapter.StemBudsAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.MainHome_Fragment;
import com.stembuddy.fragment.NotificationList;
import com.stembuddy.model.Notification_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CommonUtils;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class Stem_Buds extends Fragment implements View.OnClickListener {
    private RadioGroup online;
    private View v;
    String userLoginStatus = "";

    private GridView gridView;
    private ArrayList<Notification_Model> arrayList;
    private StemBudsAdapter adapter;
    ScheduledExecutorService scheduledExecutorService;
    private boolean show_loader=true;
    private Dialog dialog;
    private Context context;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.stem_buds_grid_layout, container, false);
           context=getActivity();
        dialog=new Dialog(context);
        arrayList = new ArrayList<>();
        gridView = v.findViewById(R.id.stem_buds_gridview);

        byte x=10;

        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                hitShowFavApi("con");
                show_loader=false;
//                try {
//                    Thread.sleep(1000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        }, 0, 10, TimeUnit.SECONDS);
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                hitShowFavApi("con");
//                try {
//                    Thread.sleep(1000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();

        v.findViewById(R.id.donor_title).setOnClickListener(this);
        v.findViewById(R.id.eventTitle).setOnClickListener(this);
        v.findViewById(R.id.notification_icon).setOnClickListener(this);
        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new MainHome_Fragment(), "Home", view);
            }
        });
        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.donor_title:
                scheduledExecutorService.shutdown();
                scheduledExecutorService = Executors.newScheduledThreadPool(1);
                scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        hitShowFavApi("con");
                        show_loader=false;
//                try {
//                    Thread.sleep(1000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                    }
                }, 0, 10, TimeUnit.SECONDS);
                break;
            case R.id.eventTitle:

                scheduledExecutorService.shutdown();
                scheduledExecutorService = Executors.newScheduledThreadPool(1);
                scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        hitShowFavApi("on");
                        show_loader=false;
//                try {
//                    Thread.sleep(1000);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                    }
                }, 0, 10, TimeUnit.SECONDS);
                break;
            case R.id.notification_icon:
                NotificationList notificationList = new NotificationList();
                Bundle bundle = new Bundle();
                bundle.putString("from_activity", "buds_list");
                notificationList.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(notificationList, "Notification_list", view);
                break;
            default:
                break;
        }
    }

    private void hitShowFavApi(final String value) {
        String url = Constants.BASE_URL;
        if (show_loader) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((BaseActivity) getActivity()).showLoader();
                }
            });
        }
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e("StemBudResponse",response);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) getActivity()).dismissLoader();
                        }
                    });
                    arrayList = new ArrayList<>();
//                    ArrayList<String> strings = new ArrayList<>();
                    boolean b = true;
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            JSONObject object = jsonObject.getJSONObject("mentor");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                            if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                                if (!jsonObject1.getString("mentor_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = false;
                                } else b = true;
                            } else {
                                if (!jsonObject1.getString("student_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = false;
                                }
                                else b = true;
                            }
                            if (b) {
                                Notification_Model model = new Notification_Model();
                                model.setId(object.getString("id"));
                                model.setAffiliation(object.getString("affiliation"));
                                model.setBlock_user(object.getString("block"));
                                model.setDescription(object.getString("description"));
                                model.setName(object.getString("name"));
                                model.setComment(object.getString("comment"));
                                model.setEmail(object.getString("email"));
                                model.setUser_type(object.getString("user_type"));
                                model.setSpeciality(object.getString("specility"));
                                model.setRate(object.getString("rating_bar"));
                                model.setMobile(object.getString("mobile"));
                                model.setOnline_offline(object.getString("status"));
                                model.setProfession(object.getString("profession"));
                                model.setPhoto(object.getString("image"));
                                model.setUser_name(object.getString("user_name"));

                                if (value.equals("on")) {
                                    if (object.getString("status").equals("1")) {
                                        arrayList.add(model);
                                    }
                                } else {
                                    arrayList.add(model);
                                }
                            }
                        }

                        if(value.equalsIgnoreCase("con")) {

                            v.findViewById(R.id.donor_title).setBackgroundColor(getResources().getColor(R.color.online_blue));

                            v.findViewById(R.id.eventTitle).setBackgroundColor(getResources().getColor(R.color.white));
                        }
                        else {

                            v.findViewById(R.id.donor_title).setBackgroundColor(getResources().getColor(R.color.white));
                            v.findViewById(R.id.eventTitle).setBackgroundColor(getResources().getColor(R.color.online_blue));
                        }


//                        adapter = new StemBudsAdapter(getActivity(), arrayList);
//                        gridView.setAdapter(adapter);

                    } else {
//                        gridView.setVisibility(View.GONE);
//                        v.findViewById(R.id.text_no_result_found).setVisibility(View.VISIBLE);
//                        if (value.equals("on")) {
//                            ((TextView) v.findViewById(R.id.text_no_result_found)).setText("None of your Buddy is online");
//                        } else {
//                            ((TextView) v.findViewById(R.id.text_no_result_found)).setText("You have not added any Buddy yet");
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    hitApiGetAccept(value);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
//                ((BaseActivity) getActivity()).dismissLoader();
                gridView.setVisibility(View.GONE);
                v.findViewById(R.id.text_no_result_found).setVisibility(View.VISIBLE);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((BaseActivity) getActivity()).showToast("Network error... Please try again");
                        ((BaseActivity) getActivity()).dismissLoader();
                        ((TextView) v.findViewById(R.id.text_no_result_found)).setText("Network error... Please try again");
                    }
                });

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "all_fav_and_block_by_student");
                } else {
                    params.put("rule", "all_fav_and_block_by_mentor");
                }
                params.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                params.put("status", "0"); //1 for getting unblock favourtes.....0 for getting blocked contacts
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "show_fav");
    }

    private void hitApiGetAccept(final String value) {
        String url = Constants.BASE_URL;

        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e("hitApiGetAccepted",response);
//                    ((BaseActivity) getActivity()).dismissLoader();
//                    arrayList = new ArrayList<>();
                    JSONObject jsonO = new JSONObject(response);
                    boolean b = true;
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            JSONObject object = jsonObject.getJSONObject("response");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("details");
                            if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                                if (!jsonObject1.getString("mentor_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = false;
                                } else b = true;
                            } else {
                                if (!jsonObject1.getString("student_id").equalsIgnoreCase(object.getString("id"))) {
                                    b = false;
                                } else b = true;
                            }
                            if (b) {
                                Notification_Model model = new Notification_Model();
                                model.setAffiliation(object.getString("affiliation"));
                                model.setId(object.getString("id"));
                                model.setBlock_user(object.getString("block"));
                                model.setDescription(object.getString("description"));
                                model.setName(object.getString("name"));
                                model.setComment(object.getString("comment"));
                                model.setEmail(object.getString("email"));
                                model.setUser_type(object.getString("user_type"));
                                model.setSpeciality(object.getString("specility"));
                                model.setRate(object.getString("rating_bar"));
                                model.setMobile(object.getString("mobile"));
                                model.setOnline_offline(object.getString("status"));
                                model.setProfession(object.getString("profession"));
                                model.setPhoto(object.getString("image"));
                                model.setUser_name(object.getString("user_name"));

                                if (value.equals("on")) {
                                    if (object.getString("status").equals("1")) {
                                        arrayList.add(model);
                                    }
                                } else {
                                    arrayList.add(model);
                                }
                            }
                        }
                    } else {
                        if (arrayList.size() == 0) {
                            gridView.setVisibility(View.GONE);
                            v.findViewById(R.id.text_no_result_found).setVisibility(View.VISIBLE);
                            if (value.equals("on")) {
                                ((TextView) v.findViewById(R.id.text_no_result_found)).setText("None of your Buddy is online");
                            } else {
                                ((TextView) v.findViewById(R.id.text_no_result_found)).setText("You have not added any Buddy yet");
                            }
                        } else {
                            gridView.setVisibility(View.VISIBLE);
                            v.findViewById(R.id.text_no_result_found).setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (arrayList.size() != 0) {

                       getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int a=0;
                                if (adapter == null) {
                                    adapter = new StemBudsAdapter(getActivity(), arrayList);
                                }
                                else {
                                    a = adapter.getPosition();
                                    adapter = new StemBudsAdapter(getActivity(), arrayList);
                                }
                                gridView.setAdapter(adapter);
                                gridView.smoothScrollToPosition(a);
                            }
                        });

                    }
//                    ((BaseActivity) getActivity()).dismissLoader();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
//                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
                gridView.setVisibility(View.GONE);
                v.findViewById(R.id.text_no_result_found).setVisibility(View.VISIBLE);
                ((TextView) v.findViewById(R.id.text_no_result_found)).setText("Network error... Please try again");
//                ((BaseActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "accepted_by_student");
                    params.put("student_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                } else {
                    params.put("rule", "accepted_by_mentor");
                    params.put("mentor_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                }
                params.put("stauts", "1");
//                params.put("status", "1"); //1 for getting unblock favourtes.....0 for getting blocked contacts
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "show_fav");
    }

    @Override
    public void onPause() {
        super.onPause();
        scheduledExecutorService.shutdown();

    }
}
