package com.stembuddy.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ashish on 12/22/2017.
 */

public class Change_password extends Fragment {

    private View v;
    ProgressDialog progressDialog;
    private EditText old_pass, confrim_pass, new_pass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.change_password, container, false);
        progressDialog = new ProgressDialog(getActivity());
        old_pass = (EditText) v.findViewById(R.id.old_password);
        confrim_pass = (EditText) v.findViewById(R.id.con_password);
        new_pass = (EditText) v.findViewById(R.id.new_password);

        v.findViewById(R.id.password_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vv) {

                if (!((EditText) v.findViewById(R.id.old_password)).getText().toString().equals("")
                        && !((EditText) v.findViewById(R.id.old_password)).getText().toString().equals("null")
                        && !((EditText) v.findViewById(R.id.new_password)).getText().toString().equals("")
                        && !((EditText) v.findViewById(R.id.new_password)).getText().toString().equals("null")) {


                    if (((EditText) v.findViewById(R.id.new_password)).getText().toString()
                            .equals(((EditText) v.findViewById(R.id.con_password)).getText().toString())) {
                        if (old_pass.getText().toString().length() < 6 || old_pass.getText().toString().length() > 6) {
                            old_pass.setError("Please Enter 6 digit password only");
                        } else if (confrim_pass.getText().toString().length() < 6 || confrim_pass.getText().toString().length() > 6) {
                            confrim_pass.setError("Please Enter 6 digit password only");
                        } else if (new_pass.getText().toString().length() < 6 || new_pass.getText().toString().length() > 6) {
                            new_pass.setError("Please Enter 6 digit password only");
                        } else {
                            if (((BaseActivity) getActivity()).isNetworkEnabled(getActivity()))
                                hit_change_password();
                            else
                                Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Password mismatch!!...", Toast.LENGTH_SHORT).show();
                        ((EditText) v.findViewById(R.id.new_password)).setText("");
                        ((EditText) v.findViewById(R.id.con_password)).setText("");
                    }
                } else {
                    Toast.makeText(getActivity(), "Please fill all details", Toast.LENGTH_SHORT).show();
                }
            }
        });

        confrim_pass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                confrim_pass.setFocusableInTouchMode(true);
                confrim_pass.requestFocus();
//                if(keyEvent.getAction() == keyEvent.KEYCODE_ENTER || keyEvent.getAction() == keyEvent.ACTION_DOWN){
                if (i == EditorInfo.IME_ACTION_DONE || i == keyEvent.ACTION_DOWN) {
                    if (!((EditText) v.findViewById(R.id.old_password)).getText().toString().equals("")
                            && !((EditText) v.findViewById(R.id.old_password)).getText().toString().equals("null")
                            && !((EditText) v.findViewById(R.id.new_password)).getText().toString().equals("")
                            && !((EditText) v.findViewById(R.id.new_password)).getText().toString().equals("null")) {


                        if (((EditText) v.findViewById(R.id.new_password)).getText().toString()
                                .equals(((EditText) v.findViewById(R.id.con_password)).getText().toString())) {
                            if (old_pass.getText().toString().length() < 6 || old_pass.getText().toString().length() > 6) {
                                Toast.makeText(getActivity(), "Please Enter 6 digit number only", Toast.LENGTH_SHORT).show();
                            } else if (confrim_pass.getText().toString().length() < 6 || confrim_pass.getText().toString().length() > 6) {
                                Toast.makeText(getActivity(), "Please Enter 6 digit number only", Toast.LENGTH_SHORT).show();
                            } else if (new_pass.getText().toString().length() < 6 || new_pass.getText().toString().length() > 6) {
                                Toast.makeText(getActivity(), "Please Enter 6 digit number only", Toast.LENGTH_SHORT).show();
                            } else {
                                if (((BaseActivity) getActivity()).isNetworkEnabled(getActivity()))
                                    hit_change_password();
                                else
                                    Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Password mismatch!!...", Toast.LENGTH_SHORT).show();
                            ((EditText) v.findViewById(R.id.new_password)).setText("");
                            ((EditText) v.findViewById(R.id.con_password)).setText("");
                        }
                    } else {
                        Toast.makeText(getActivity(), "Please fill all details", Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            }
        });


        return v;
    }

    private void hit_change_password() {

        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            progressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                Toast.makeText(getActivity(), "You have changed your password successfully", Toast.LENGTH_SHORT).show();
                                ((MainActivity) getActivity()).loadFragment(new MainHome_Fragment(), "Home", v);
                            } else {
                                Toast.makeText(getActivity(), "Incorrect Old Password!..", Toast.LENGTH_SHORT).show();

                                ((EditText) v.findViewById(R.id.old_password)).setText("");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                Toast.makeText(getActivity(), "Someting went wrong. Please check your internet connection", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "change_password");
                params.put("user_id", SharedPreference_Main.getInstance(getContext()).getUserId());
                params.put("old_pass", ((EditText) v.findViewById(R.id.old_password)).getText().toString());
                params.put("new_pass", ((EditText) v.findViewById(R.id.new_password)).getText().toString());
                params.put("con_pass", ((EditText) v.findViewById(R.id.con_password)).getText().toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(request, "change_pass");
    }


}
