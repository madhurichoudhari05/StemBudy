package com.stembuddy.model;

/**
 * Created by THAKUR on 11/29/2017.
 */

public class Testimonial_Model {
    private String name;
    private String profe;
    private String content;
    private String testi_by;
    private String setId;
    private String discription;
    private String photo;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTesti_by() {
        return testi_by;
    }

    public void setTesti_by(String testi_by) {
        this.testi_by = testi_by;
    }

    public String getSetId() {
        return setId;
    }

    public void setSetId(String setId) {
        this.setId = setId;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfe() {
        return profe;
    }

    public void setProfe(String profe) {
        this.profe = profe;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String  photo) {
        this.photo = photo;
    }
}
