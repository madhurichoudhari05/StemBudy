package com.stembuddy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.VidioesActivity;
import com.stembuddy.adapter.VideosFragment_Activity_Adapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.VideosFragment_Activity_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Himanshu on 1/19/2018.
 */

public class VideosFragment_Activity extends Fragment {
    private View view;
    private ArrayList<VideosFragment_Activity_Model> owned_list;
    private ArrayList<VideosFragment_Activity_Model> community_list;
    private ArrayList<VideosFragment_Activity_Model> specificUserList;
    private ListView videos_list, videos_list_specific_user, videos_list_sb_community;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_saved_videos, container, false);
        videos_list = view.findViewById(R.id.videos_list);
        videos_list_specific_user = view.findViewById(R.id.videos_list_specific_user);
        videos_list_sb_community = view.findViewById(R.id.videos_list_sb_community);
        view.findViewById(R.id.saved_videos_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VidioesActivity activity = new VidioesActivity();
                Bundle bundle = new Bundle();
                bundle.putString("from", "Videos_activity");
                activity.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(activity, "Videos_activity", view);
            }
        });
        view.findViewById(R.id.saved_specific_videos_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VidioesActivity activity = new VidioesActivity();
                Bundle bundle = new Bundle();
                bundle.putString("from", "Videos_activity_user");
                activity.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(activity, "Videos_activity", view);
            }
        });
        view.findViewById(R.id.saved_community_videos_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VidioesActivity activity = new VidioesActivity();
                Bundle bundle = new Bundle();
                bundle.putString("from", "Videos_activity_sb_community");
                activity.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(activity, "Videos_activity", view);
            }
        });
        view.findViewById(R.id.videos_btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new View_Videos_Fragment(), "Videos_activity", view);
            }
        });
        getDataSaved_Videos(view);
        return view;
    }

    private void getDataSaved_Videos(final View view) {
        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            owned_list = new ArrayList<>();
//                            community_list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length()/*(jsonArray.length() < 2 ? jsonArray.length() : 2)*/; i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    VideosFragment_Activity_Model videos_model = new VideosFragment_Activity_Model();
                                    videos_model.setVideo_Id(jsonObject1.getString("id"));
                                    //videos_model.setVideo_Url("http://ritaraapps.com/Stem_Buddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_Url("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_thumb_Image("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("thumb_img"));
                                    videos_model.setVideo_Name(jsonObject1.getString("video_title"));
                                    videos_model.setVideo_To("own");
                                    if (jsonObject1.getString("video_type").equalsIgnoreCase("own")) {
                                        owned_list.add(videos_model);
                                    }
                                }
                                if (owned_list.size() > 0) {
                                    VideosFragment_Activity_Adapter adapter = new VideosFragment_Activity_Adapter(getActivity(), owned_list);
                                    videos_list.setAdapter(adapter);
                                } else {
                                    view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                                    view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                                }
                            } else {
                                view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                                view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                            view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                            TextView textView = view.findViewById(R.id.error_message_saved_tv);
                            textView.setText("Opps! Someting Went Wrong");
                            e.printStackTrace();

                        } finally {
                            getDataCommunityList();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community"))
                view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                TextView textView = view.findViewById(R.id.error_message_saved_tv);
                textView.setText("Opps! Someting Went Wrong. Please Check Your Internet Connection");
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "all_video_user");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getDataCommunityList() {
        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            owned_list = new ArrayList<>();
                            community_list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = (jsonArray.length() < 2 ? jsonArray.length() : 2); i > 0; i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    VideosFragment_Activity_Model videos_model = new VideosFragment_Activity_Model();
                                    videos_model.setVideo_Id(jsonObject1.getString("id"));
                                    videos_model.setVideo_Url("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_thumb_Image("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("thumb_img"));
                                    videos_model.setVideo_Name(jsonObject1.getString("video_title"));
                                    videos_model.setVideo_To("community");
                                    if (!jsonObject1.getString("video_type").equalsIgnoreCase("own")) {
                                        community_list.add(videos_model);
                                    }
                                }
                            } else {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            //getDataCommunityList1();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community"))
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "all_video_user");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getDataCommunityList1() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                for (int i = 0; i < (jsonArray.length() < 2 ? jsonArray.length() : 2); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    JSONObject jsonObject1 = jsonObject2.getJSONObject("video_details");
                                    VideosFragment_Activity_Model videos_model = new VideosFragment_Activity_Model();
                                    videos_model.setVideo_Id(jsonObject1.getString("id"));
                                    videos_model.setVideo_Url("http://ritaraapps.com/Stem_Buddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_thumb_Image("http://ritaraapps.com/Stem_Buddy/uploads/" + jsonObject1.getString("thumb_img"));
                                    videos_model.setVideo_Name(jsonObject1.getString("video_title"));
                                    videos_model.setVideo_To("community");
                                    community_list.add(videos_model);
                                }

                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            ((MainActivity) getActivity()).dismissLoader();

                        } finally {
                            if (community_list.size() != 0) {
                                VideosFragment_Activity_Adapter adapter = new VideosFragment_Activity_Adapter(getActivity(), community_list);
                                videos_list_sb_community.setAdapter(adapter);
                            } else {
                                view.findViewById(R.id.videos_list_sb_community_layout).setVisibility(View.GONE);
                                view.findViewById(R.id.error_message_sb_tv).setVisibility(View.VISIBLE);
//                                TextView textView=view.findViewById(R.id.error_message_sb_tv);
//                                textView.setText("Opps! Someting Went Wrong");
                            }
                            getDataSpecificUser();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community"))
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "get_all_user_shared_video");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getDataSpecificUser() {
        ((MainActivity) getActivity()).dismissLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            specificUserList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                for (int i = 0; i < (jsonArray.length() < 2 ? jsonArray.length() : 2); i++) {
                                    try {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        JSONObject data = jsonObject1.getJSONObject("data");
                                        JSONObject video_data = jsonObject1.getJSONObject("video_data");
                                        JSONObject user_data = jsonObject1.getJSONObject("user_data");
                                        VideosFragment_Activity_Model videos_model = new VideosFragment_Activity_Model();
                                        videos_model.setVideo_Id(video_data.getString("id"));
                                        videos_model.setVideo_Url("http://ritaraapps.com/Stem_Buddy/uploads/" + video_data.getString("video"));
                                        videos_model.setVideo_Name(video_data.getString("video_title"));
                                        videos_model.setVideo_thumb_Image("http://ritaraapps.com/Stem_Buddy/uploads/" + video_data.getString("thumb_img"));
                                        videos_model.setVideo_To("specific_user");
                                        specificUserList.add(videos_model);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(getActivity(), "Opps! Something went wrong, Please Try Again Later", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                VideosFragment_Activity_Adapter adapter = new VideosFragment_Activity_Adapter(getActivity(), specificUserList);
                                videos_list_specific_user.setAdapter(adapter);
                            } else {
                                view.findViewById(R.id.videos_list_specific_user_layout).setVisibility(View.GONE);
                                view.findViewById(R.id.error_message_specific_tv).setVisibility(View.VISIBLE);
//                                TextView textView=view.findViewById(R.id.error_message_sb_tv);
//                                textView.setText("Opps! Someting Went Wrong");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "get_on_video_for_user_shared");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
