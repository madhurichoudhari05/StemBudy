package com.stembuddy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.model.Review_List_Model;
import com.stembuddy.utils.CircleTransform;

import java.util.ArrayList;

/**
 * Created by mahak on 12/28/2017.
 */

public class ReviewListAdapter extends BaseAdapter {
    private ArrayList<Review_List_Model> datalist;
    private LayoutInflater layoutInflater;
    Context context;

    public ReviewListAdapter(Context activity, ArrayList<Review_List_Model> data) {
        this.context = activity;
        this.datalist = data;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View v = layoutInflater.inflate(R.layout.review_list, viewGroup, false);
        try {
            Glide.with(context).load(datalist.get(position).getPhoto()).transform(new CircleTransform(context))
                    .placeholder(R.mipmap.review_profile).into((ImageView) v.findViewById(R.id.photo_comment));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) v.findViewById(R.id.name_comment)).setText(datalist.get(position).getName());
        String[] arry = datalist.get(position).getCommented_date().split(" ");
        ((TextView) v.findViewById(R.id.date_comment)).setText(arry[0]);
        ((TextView) v.findViewById(R.id.comment_txt)).setText(datalist.get(position).getComment());

//        if(datalist.get(position).getRating().equals("")){
//            v.findViewById(R.id.rating_comment).setVisibility(View.GONE);
//        }else{
//            v.findViewById(R.id.rating_comment).setVisibility(View.VISIBLE);
//            ((RatingBar) v.findViewById(R.id.rating_comment)).setRating(Float.parseFloat(datalist.get(position).getRating()));
//        }

        return v;
    }
}

