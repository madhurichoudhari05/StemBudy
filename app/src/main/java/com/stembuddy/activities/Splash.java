package com.stembuddy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;

import com.stembuddy.R;
import com.stembuddy.shard_preference.SharedPreference_Main;

/**
 * Created by THAKUR on 11/22/2017.
 */

public class Splash extends BaseActivity{
    int SPLASH_DISPLAY_LENGTH = 3000;
    private SharedPreference_Main sharedPreference_main;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.spalsh_layout);

        sharedPreference_main = SharedPreference_Main.getInstance(this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                if (sharedPreference_main.getSession_login()) {
                    Intent mainIntent = new Intent(Splash.this, Second_Splash.class);
                    Splash.this.startActivity(mainIntent);
                    Splash.this.finish();
                } else {
                    Intent mainIntent = new Intent(Splash.this, Second_Splash.class);
                    Splash.this.startActivity(mainIntent);
                    Splash.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }



}
