package com.stembuddy.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.utils.CircleTransform;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class TestomonialActivity extends Fragment {


    private  View v;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.testimonial_page_layout, container, false);

        try {
            Glide.with(getActivity()).load(getArguments().getString("photo"))
                    .placeholder(R.mipmap.testimonal_profile).transform(new CircleTransform(getActivity()))
                    .into((ImageView)v.findViewById(R.id.test_profile));
        }catch(Exception e){
            e.printStackTrace();
        }

        ((TextView) v.findViewById(R.id.test_name)).setText(getArguments().getString("name"));
        ((TextView) v.findViewById(R.id.test_profession)).setText(getArguments().getString("profession"));
        ((TextView) v.findViewById(R.id.desc)).setText(Html.fromHtml(getArguments().getString("desc")));

        v.findViewById(R.id.testmonial_back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).loadFragment(new TestimonialActivity1(), "Testimonial", v);
            }
        });

        return v;
    }


}
