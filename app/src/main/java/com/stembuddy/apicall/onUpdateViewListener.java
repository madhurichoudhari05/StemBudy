package com.stembuddy.apicall;

public interface onUpdateViewListener {
	void updateView(Object responseObject, boolean isSuccess, int reqType);
}
