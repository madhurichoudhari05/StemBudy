package com.stembuddy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.adapter.ReviewListAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.Review_List_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Himanshu on 12/31/2017.
 */

public class All_comments_On_Profile extends Fragment {

    private ListView comment_profile_listView;
    private ArrayList<Review_List_Model> review_arrylist;
    private ReviewListAdapter reviewListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_comments_profile, container, false);
        comment_profile_listView = view.findViewById(R.id.comment_profile_listView);

        if (((MainActivity) getActivity()).isNetworkEnabled(getActivity())) {
            hitApiToGetComments(view);
        }else {
            ((MainActivity) getActivity()).noInternet();
        }

        return view;
    }

    private void hitApiToGetComments(final View v) {
        String url = Constants.BASE_URL;
        ((BaseActivity) getActivity()).showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    ((BaseActivity) getActivity()).dismissLoader();
                    JSONObject jsonO = new JSONObject(response);
                    review_arrylist = new ArrayList<>();
                    if (jsonO.getString("res").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("message");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            JSONObject jsonObject = object.getJSONObject("data");
                            JSONObject jsonOb = object.getJSONObject("from_data");

                            Review_List_Model model = new Review_List_Model();
                            model.setComment(jsonObject.getString("comment"));
                            model.setCommented_date(jsonObject.getString("added_time"));
                            model.setPhoto(jsonOb.getString("image"));
                            model.setName(jsonOb.getString("name"));
                            model.setRating(jsonOb.getString("rating_bar"));

                            review_arrylist.add(model);
                        }
                        reviewListAdapter = new ReviewListAdapter(getActivity(), review_arrylist);
                        comment_profile_listView.setAdapter(reviewListAdapter);
                    } else {
                        v.findViewById(R.id.no_found).setVisibility(View.VISIBLE);
                        comment_profile_listView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "get_all_comment");
                params.put("user_type", "student");
                params.put("from_comment", getArguments().getString("comments_ids"));
                params.put("to_comment", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "review");
    }
}
