package com.stembuddy.fragment;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.TopDiscModel;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Himanshu on 1/4/2018.
 */

public class VideoShareCommunity extends Fragment {
    private TextView video_title;
    private ImageView video_image;
    private Button submit_button;
    private Spinner community_spinner;
    private ArrayList<TopDiscModel> arrayList;
    private String community_id = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_video_share_community, container, false);
        video_title = view.findViewById(R.id.text_view);
        video_image = view.findViewById(R.id.video_upload);
        submit_button = view.findViewById(R.id.submit);
        community_spinner = view.findViewById(R.id.community_list_adapter);

        video_title.setText(getArguments().getString("video_title"));
        try {
            video_image.setImageBitmap(retriveVideoFrameFromVideo(getArguments().getString("video_url")));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }


        community_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    community_id = arrayList.get(i - 1).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!community_id.equalsIgnoreCase("")){
                    hitApiShareVideo();
                }
                else {
                    Toast.makeText(getActivity(), "Please Select Sb Community", Toast.LENGTH_SHORT).show();
                }
            }
        });
        hitApiGetCommunity();
        return view;
    }

    private void hitApiGetCommunity() {
        ((BaseActivity) getActivity()).showLoader();
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) getActivity()).dismissLoader();
                            arrayList = new ArrayList<>();
                            ArrayList<String> strings = new ArrayList<>();
                            strings.add("Select Your Community");
                            JSONObject jsonO = new JSONObject(response);
                            if (jsonO.getString("res").equals("1")) {
                                JSONArray array = jsonO.getJSONArray("message");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    TopDiscModel model = new TopDiscModel();
                                    model.setId(object.getString("id"));
                                    strings.add(object.getString("title"));
                                    model.setDescription(object.getString("discription"));
                                    model.setTitle(object.getString("title"));
                                    model.setPost(object.getString("posts"));
                                    model.setViews(object.getString("views"));
                                    model.setProfile(object.getString("image"));
                                    model.setCreated_date(object.getString("created_date"));
                                    arrayList.add(model);
                                }
                                ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, strings);
                                community_spinner.setAdapter(arrayAdapter);
                            } else {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "get_all_discussion_topic");

                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(sr, "get_all_discussion_topic");
    }

    private void hitApiShareVideo(){
        ((BaseActivity)getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity)getActivity()).dismissLoader();
                            JSONObject jsonObject = new JSONObject(response);

                        }
                        catch (Exception e){e.printStackTrace();
                            ((BaseActivity)getActivity()).dismissLoader();}
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map =new HashMap<>();
                map.put("rule", "share_on_topic_video");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                map.put("topic_id", community_id);
                map.put("video_id", getArguments().getString("video_id"));
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }


}