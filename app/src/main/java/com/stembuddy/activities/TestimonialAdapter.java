package com.stembuddy.activities;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.model.Testimonial_Model;
import com.stembuddy.utils.CircleTransform;

import java.util.ArrayList;



public class TestimonialAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<Testimonial_Model> data;
    private static LayoutInflater inflater=null;
    Context context;

    public TestimonialAdapter(Activity activity, ArrayList data, Context context) {
        this.activity = activity;
        this.data = data;
        this.context = context;
    }


    @Override
    public int getCount() {
        if(data.size()<=0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public static class ViewHolder{
        private ImageView img;
        private TextView name;
        private TextView profession;
        private TextView content;
        private Button readMore;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        inflater=(LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View vi=view;
        if (vi==null) {
            holder=new ViewHolder();
            vi=inflater.inflate(R.layout.testimonial_page1_list_row,null);
            holder.name=vi.findViewById(R.id.test_name);
            holder.profession=vi.findViewById(R.id.test_profession);
            holder.img=vi.findViewById(R.id.test_profile);
            holder.content=vi.findViewById(R.id.test_content);
            vi.setTag(holder);

        }else
        {
            holder=(ViewHolder)view.getTag();
        }
        vi.findViewById(R.id.test_read_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).loadFragment(new TestomonialActivity(), "testimonial_activity", view);
            }
        });

        Glide.with(context)
                .load(data.get(i).getPhoto())
                .transform(new CircleTransform(context))
                .into(holder.img);

        holder.name.setText(data.get(i).getName());
        holder.profession.setText(data.get(i).getTesti_by());
        holder.content.setText(Html.fromHtml(data.get(i).getDiscription()));
        return vi;
    }
}
