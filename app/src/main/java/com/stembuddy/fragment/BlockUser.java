package com.stembuddy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/27/2017.
 */

public class BlockUser extends Fragment {
    private EditText search_block_et;
    private View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.search_block_layout, container, false);
        search_block_et = view.findViewById(R.id.search_block);
        view.findViewById(R.id.search_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Block_user_list_API();
                if (search_block_et.getText().toString().trim().equalsIgnoreCase("")) {
                    search_block_et.setError("Please enter name");
                } else {
                    BlockUserList blockUserList = new BlockUserList();
                    Bundle bundle = new Bundle();
                    blockUserList.setArguments(bundle);
                    bundle.putString("name_value", search_block_et.getText().toString());
                    ((MainActivity) getActivity()).loadFragment(blockUserList, "Block_user_list", view);
                }

            }
        });
        return view;

    }

    private void Block_user_list_API() {
        ((BaseActivity) getActivity()).showLoader();
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) getActivity()).dismissLoader();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                JSONObject regJsonObject = jsonArray.getJSONObject(0);
                                SharedPreference_Main preferenceMain = SharedPreference_Main.getInstance(getActivity());
                                preferenceMain.setUserId(regJsonObject.getString("id"));
                                preferenceMain.setUserType(regJsonObject.getString("type"));
                                preferenceMain.setName(regJsonObject.getString("name"));
                                preferenceMain.setUserProfile(regJsonObject.getString("image"));
                                ((MainActivity) getActivity()).loadFragment(new BlockUserList(), "Block_user_list", view);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "User list not correct", Toast.LENGTH_LONG).show();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went Wrong", Toast.LENGTH_SHORT).show();
                Log.e("VE ", error.getMessage());
                ((BaseActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "user_search_name");
                params.put("name", getArguments().getString("name_value"));
                params.put("type", SharedPreference_Main.getInstance(getActivity()).getUserType());

                return params;

            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(sr);
    }

}




