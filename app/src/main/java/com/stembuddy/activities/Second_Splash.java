package com.stembuddy.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/22/2017.
 */

public class Second_Splash extends AppCompatActivity {
    int SPLASH_DISPLAY_LENGTH = 3000;
    private RelativeLayout circle_anim;
    private ImageView left_side_avatar;
    private ImageView right_side_avatar;
    private SharedPreference_Main sharedPreference_main;


    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sharedPreference_main=SharedPreference_Main.getInstance(this);

        setContentView(R.layout.splashscreen);
        circle_anim = (RelativeLayout) findViewById(R.id.circle_anim);
        right_side_avatar = (ImageView) findViewById(R.id.right_side_avatar);
        left_side_avatar = (ImageView) findViewById(R.id.left_side_avatar);


//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {

        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_up);
        circle_anim.setAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if (sharedPreference_main.getSession_login()){
                    hitLoginApi();
//                    Intent mainIntent = new Intent(Second_Splash.this, MainActivity.class);
//                    Second_Splash.this.startActivity(mainIntent);
//                    Second_Splash.this.finish();
                }
                else {
                    Intent Intent = new Intent(Second_Splash.this, Login.class);
                    Second_Splash.this.startActivity(Intent);
                    Second_Splash.this.finish();

                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        Animation ani = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_right);

        left_side_avatar.setAnimation(ani);

        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (sharedPreference_main.getSession_login()){
                    Intent mainIntent = new Intent(Second_Splash.this, MainActivity.class);
                    Second_Splash.this.startActivity(mainIntent);
                    Second_Splash.this.finish();
                }
                else {
                    Intent Intent = new Intent(Second_Splash.this, Login.class);
                    Second_Splash.this.startActivity(Intent);
                    Second_Splash.this.finish();

                }

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        Animation animations = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_left);
        right_side_avatar.setAnimation(animations);

        animations.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (sharedPreference_main.getSession_login()){
                    Intent mainIntent = new Intent(Second_Splash.this, MainActivity.class);
                    Second_Splash.this.startActivity(mainIntent);
                    Second_Splash.this.finish();
                }
                else {
                    Intent Intent = new Intent(Second_Splash.this, Login.class);
                    Second_Splash.this.startActivity(Intent);
                    Second_Splash.this.finish();

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    private void hitLoginApi() {
        final ProgressDialog dialog = new ProgressDialog(Second_Splash.this);
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
//        dialog.show();
        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            dialog.dismiss();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                JSONObject regJsonObject = jsonArray.getJSONObject(0);
                                SharedPreference_Main preferenceMain = SharedPreference_Main.getInstance(Second_Splash.this);
                                preferenceMain.setUserId(regJsonObject.getString("id"));
                                preferenceMain.setUserType(regJsonObject.getString("user_type"));
                                preferenceMain.setName(regJsonObject.getString("name"));
                                preferenceMain.setEmail(regJsonObject.getString("email"));
                                preferenceMain.setUserPwd(regJsonObject.getString("pass"));
                                preferenceMain.setUserName(regJsonObject.getString("user_name"));
                                preferenceMain.setUserProfile(regJsonObject.getString("image"));
                                preferenceMain.setDescription(regJsonObject.getString("description"));
                                preferenceMain.setProfession(regJsonObject.getString("profession"));
                                preferenceMain.setSpeciality(regJsonObject.getString("specility"));
                                preferenceMain.setSession_login(true);

                                try {
//                                    if (!getSinchServiceInterface().isStarted()) {
//                                        getSinchServiceInterface().startClient(preferenceMain.getName());
//                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Intent i = new Intent(Second_Splash.this, MainActivity.class);
                                startActivity(i);
                                finish();
                            } else {
//                                Toast.makeText(Second_Splash.this, "Invalid Email id or password", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
//                            Toast.makeText(Second_Splash.this, "Invalid Email id or password", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(Second_Splash.this, "Something went Wrong", Toast.LENGTH_SHORT).show();
                //Log.e("VE ", volleyError.getMessage());
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "login");
                params.put("email", sharedPreference_main.getUserName());
                params.put("user_name", sharedPreference_main.getUserName());
                params.put("pass", sharedPreference_main.getUserPwd());
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(sr);
    }
}




//                Intent mainIntent = new Intent(Second_Splash.this, Login.class);
//                Second_Splash.this.startActivity(mainIntent);
//                Second_Splash.this.finish();
//        }
//        }, SPLASH_DISPLAY_LENGTH);
//    }
//}
