package com.stembuddy.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by THAKUR on 11/22/2017.
 */
public class Contact_Us extends Fragment implements LocationListener {

    private View v;
    private SharedPreference_Main sharedPreference_main;
    private TextView contact_address, contact_email, contact_number;


    private MapView mapView;
    private GoogleMap googleMap;
    private String permissions[] = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.contact_us, container, false);

        contact_address = (TextView )v.findViewById(R.id.contact_addrsss);
        contact_email = (TextView )v.findViewById(R.id.contact_email);
        contact_number = (TextView )v.findViewById(R.id.contact_tool_free);
        sharedPreference_main=SharedPreference_Main.getInstance(getActivity());

        contact_number.setText(sharedPreference_main.getContactnumber());
        contact_email.setText(sharedPreference_main.getContactEmail());
        contact_address.setText(sharedPreference_main.getContactAddress());


        contact_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + contact_email.getText().toString()));
                startActivity(Intent.createChooser(intent3, "Send Email"));
            }
        });

        contact_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(Intent.ACTION_DIAL);
                intent2.setData(Uri.parse("tel:" + contact_number.getText().toString()));
                startActivity(intent2);
            }
        });
        mapView = (MapView)v.findViewById(R.id.map);


        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {

        }

        initMap();
        contact_us_Api();
        return v;
    }
    private void initMap() {
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                googleMap.setMyLocationEnabled(true);
                googleMap.getUiSettings().setZoomControlsEnabled(false);
                googleMap.getUiSettings().setCompassEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                googleMap.getUiSettings().setAllGesturesEnabled(true);
                googleMap.setTrafficEnabled(true);

            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
//        double lat = location.getLatitude();
//        double lang = location.getLongitude();
//        setMarker(lat, lang);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void setMarker(double lat, double lang) {
        LatLng latLng = new LatLng(lat, lang);
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("Me");
        googleMap.addMarker(markerOptions);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(12).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

    }

    private boolean check_permission() {
        int result;
        List<String> list = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getActivity(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                list.add(p);
            }
        }
        if (!list.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), list.toArray(new String[list.size()]), 100);
            return false;
        }
        return true;
    }




public void contact_us_Api(){
    ((BaseActivity)getActivity()).showLoader();
    StringRequest sr=new StringRequest(Request.Method.POST, Constants.BASE_URL,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        ((BaseActivity)getActivity()).dismissLoader();
                        JSONObject jsonObject = new JSONObject(response);
                        Log.e("ContactUs",response);
                        if(jsonObject.getString("status").equalsIgnoreCase("1")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("response");
                            JSONObject regJsonObject = jsonArray.getJSONObject(0);
                            SharedPreference_Main preferenceMain = SharedPreference_Main.getInstance(getActivity());
                            preferenceMain.setContactLat(regJsonObject.getString("lat"));
                            preferenceMain.setContactLong(regJsonObject.getString("long"));
                            preferenceMain.setContactAddress(regJsonObject.getString("address"));
                            preferenceMain.setContactEmail(regJsonObject.getString("email_id"));
                            preferenceMain.setContactnumber(regJsonObject.getString("toll_free_number"));
                            contact_number.setText(sharedPreference_main.getContactnumber());
                            contact_email.setText(sharedPreference_main.getContactEmail());
                            contact_address.setText(Html.fromHtml(sharedPreference_main.getContactAddress()));
                            try {
                                LatLng latLng = new LatLng(Double.parseDouble(regJsonObject.getString("lat")), Double.parseDouble(regJsonObject.getString("long")));
                                MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("Me");
                                googleMap.addMarker(markerOptions);
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(12).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                            } catch (Exception e) {

                            }

                        }
                    }
                        catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(),"Invalid Contact Details",Toast.LENGTH_LONG).show();
                            ((BaseActivity)getActivity()).dismissLoader();

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(getActivity(), "Something went Wrong", Toast.LENGTH_SHORT).show();
                        Log.e("VE ", volleyError.getMessage());
                        ((BaseActivity)getActivity()).dismissLoader();

                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params= new HashMap<>();
                        params.put("rule", "get_contact_details");
                        return params;
                    }
                };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                        0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(sr);
            }



}
