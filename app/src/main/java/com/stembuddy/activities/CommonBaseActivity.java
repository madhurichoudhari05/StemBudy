package com.stembuddy.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by arungeorge on 12/04/16.
 */

/*user_id
token
http://www.404coders.com/NetWrko/WebServices/chat/device_registration.php*/

public abstract class CommonBaseActivity extends Fragment {
    protected abstract void onPermissionResult(int requestCode, boolean isPermissionGranted);
    public static final String TAG = CommonBaseActivity.class.getSimpleName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    protected FrameLayout mCoordinatorLayout;
    protected FragmentManager fm;
    private Toolbar mActionbarToolbar;
    String profile_pic="";
    private Snackbar mSnackbar;
    private ProgressDialog mProgressDialog;
    private  Context context;
    private ImageView toolbarImageView;
    private TextView tvNotiCount;
    int totalCount=0;
    public boolean requestPermission(int requestCode, String... permission) {

        boolean isAlreadyGranted = false;

        isAlreadyGranted = checkPermission(permission);

        if (!isAlreadyGranted)
            ActivityCompat.requestPermissions(getActivity(), permission, requestCode);

        return isAlreadyGranted;

    }
    protected boolean checkPermission(String[] permission){

        boolean isPermission = true;

        for(String s: permission)
            isPermission = isPermission && ContextCompat.checkSelfPermission(getActivity(),s) == PackageManager.PERMISSION_GRANTED;

        return isPermission;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            onPermissionResult(requestCode, true);

        } else {

            onPermissionResult(requestCode, true);
//            Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show();

        }

    }




    public FragmentManager getFm() {
        return fm;
    }













}