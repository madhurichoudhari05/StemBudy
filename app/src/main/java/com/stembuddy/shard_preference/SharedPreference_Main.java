package com.stembuddy.shard_preference;

import android.content.Context;
import android.content.SharedPreferences;


public class SharedPreference_Main {
    private Context mContext;
    private SharedPreferences sharedPreference;
    private SharedPreferences.Editor editor;
    private static SharedPreference_Main sharedpreference_main;

    public SharedPreference_Main(Context mContext) {
        this.mContext = mContext;
        sharedPreference = mContext.getSharedPreferences("PREF_READ", Context.MODE_PRIVATE);

        editor = sharedPreference.edit();
    }

    public static SharedPreference_Main getInstance(Context context) {
        if (sharedpreference_main == null) {
            sharedpreference_main = new SharedPreference_Main(context);
        }
        return sharedpreference_main;
    }

    public void remove_Preference() {
        editor.clear();
        editor.apply();
    }

    public void setUserId(String id) {
        editor.putString("id", id).apply();
    }

    public String getUserId() {
        return sharedPreference.getString("id", "");
    }


    public void setUserType(String userType) {
        editor.putString("usertype", userType).apply();
    }

    public String getUserType() {
        return sharedPreference.getString("usertype", "");
    }

    public void setName(String name) {
        editor.putString("name", name).apply();
    }

    public String getName() {
        return sharedPreference.getString("name", "");
    }

    public void setContactLong(String contactLong){
        editor.putString("ContactLong",contactLong).apply();
    }
    public String getContactLong(){
        return sharedPreference.getString("Long","");
    }



    public void setContactLat(String contactLat){
        editor.putString("ContactLat",contactLat).apply();
    }
    public String getContactLat(){
        return sharedPreference.getString("Lat","");
    }


    public void setContactEmail(String contactEmail){
        editor.putString("ContactEmail",contactEmail).apply();
    }
    public String getContactEmail(){
        return sharedPreference.getString("ContactEmail","");
    }

    public void setContactnumber(String contactnumber){
        editor.putString("contactnumber",contactnumber).apply();
    }
    public String getContactnumber(){
        return sharedPreference.getString("contactnumber","");
    }

    public void setContactAddress(String contactAddress){
        editor.putString("contactAddress",contactAddress).apply();
    }
    public String getContactAddress(){
        return sharedPreference.getString("contactAddress","");
    }

    public void setSession_login(boolean session) {
        editor.putBoolean("session", session).apply();
    }

    public boolean getSession_login() {
        return sharedPreference.getBoolean("session", false);
    }

    public void setEmail(String email) {
        editor.putString("email", email).apply();
    }

    public String getEmail() {
        return sharedPreference.getString("email", "");
    }

    public void setUserName(String uname) {
        editor.putString("username", uname).apply();
    }

    public String getUserName() {
        return sharedPreference.getString("username", "");
    }


    public void setUserPwd(String pass) {
        editor.putString("pass", pass).apply();
    }

    public String getUserPwd() {
        return sharedPreference.getString("pass", "");
    }

    public void setProfession(String profession) {
        editor.putString("profession", profession).apply();
    }

    public String getProfession() {
        return sharedPreference.getString("profession", "");
    }

    public void setSpeciality(String speciality) {
        editor.putString("speciality", speciality).apply();
    }

    public String getSpeciality() {
        return sharedPreference.getString("speciality", "");
    }

    public void setDescription(String description) {
        editor.putString("description", description).apply();
    }

    public String getDescription() {
        return sharedPreference.getString("description", "");
    }

    public void setUserProfile(String img) {
        editor.putString("image", img).apply();
    }

    public String getUserProfile() {
        return sharedPreference.getString("image", "");
    }

    public void setuserMobile(String mobile) {
        editor.putString("mobile", mobile).apply();
    }

    public String getUserMobile() {
        return sharedPreference.getString("mobile", "");
    }

    public void setRating(String profession){ editor.putString("rating",profession).apply();}

    public String getRating(){  return  sharedPreference.getString("rating", "");}

    public String getCallInfo() {
        return sharedPreference.getString("CallInfo", "0");
    }

    public void setCallInfo(String value) {
        editor.putString("CallInfo", value).apply();
    }
    public void setFireBaseTokenid(String fireBaseTokenid){ editor.putString("fireBaseTokenid",fireBaseTokenid).apply();}

    public String getFireBaseTokenid(){  return  sharedPreference.getString("fireBaseTokenid", "");}

    public void setPostNumber(String profession){ editor.putString("postNumber",profession).apply();}

    public String getPostNumber(){  return  sharedPreference.getString("postNumber", "");}

    public void seta(String profession){ editor.putString("a",profession).apply();}

    public String geta(){  return  sharedPreference.getString("a", "a");}

}
