package com.stembuddy.fragment;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.Login;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.UploadInformation;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;


public class FeatureHome_Fragment extends Fragment implements View.OnClickListener {

    private ArrayList<String> backStack;
    private SharedPreference_Main sharedPreference_main;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        view.findViewById(R.id.update_user_profile).setOnClickListener(this);
        view.findViewById(R.id.alert).setOnClickListener(this);
        view.findViewById(R.id.delete).setOnClickListener(this);
        view.findViewById(R.id.block_user).setOnClickListener(this);
        sharedPreference_main = SharedPreference_Main.getInstance(getActivity());
        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new MainHome_Fragment(), "Home", view);



            }
        });
        return view;


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.update_user_profile:
                // startActivity(new Intent(getContext(), UploadInformation.class));
                ((MainActivity) getActivity()).loadFragment(new UploadInformation(), "UploadInformation", view);
                break;
            case R.id.alert:
                ((MainActivity) getActivity()).loadFragment(new alert(), "alert", view);
                break;
            case R.id.delete:

                AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
                ProgressDialog progressDialog;

                ab.setMessage("Are you sure want to delete your  own profile .");
                ab.setCancelable(false);
                ab.setPositiveButton("YES ", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (((BaseActivity) getActivity()).isNetworkEnabled(getActivity()))
                            Delete_user_api();
                        else
                            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

                    }
                });
                ab.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                ab.show();

                break;
            case R.id.block_user:
                BlockUserList blockUserList = new BlockUserList();
                Bundle bundle = new Bundle();
                bundle.putString("from_activity", "Block_user_list");
                blockUserList.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(blockUserList, "Block_user_list", view);
                break;
        }
    }

    public void Delete_user_api() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        try {
                            progressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(s);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                Toast.makeText(getActivity(), "User profile deleted successfully", Toast.LENGTH_SHORT).show();
                                SharedPreference_Main preferenceMain = SharedPreference_Main.getInstance(getActivity());
                                preferenceMain.setSession_login(false);
                                preferenceMain.remove_Preference();
                                Intent intent = new Intent(getActivity(), Login.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                getActivity().finishAffinity();
                            } else {
                                Toast.makeText(getActivity(), "User profile  deleted not success,please try again", Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                progressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "delete_user");
                params.put("user_id", sharedPreference_main.getUserId());
                params.put("user_type", sharedPreference_main.getUserType());
                return params;

            }
        };
        AppController.getInstance().addToRequestQueue(request, "delete_user");
    }
}

