package com.stembuddy.model;

/**
 * Created by Divakar Rao on 03-07-2017.
 */

public class ModelScan {
    private String recievers_Id;
    private String date_created;
    private String message;
    private String senders_Id;
    private String msg_senders_name;
    private String msg_senders_image;
    private String msg_id;
    private String dateTime_ofMsgRecieveed;

    private String membership_monthly_price;
    private String membership_yearly_price;
    private String category_id_fromShop_Cart_Details;

    public String getMembership_monthly_price() {
        return membership_monthly_price;
    }

    public void setMembership_monthly_price(String membership_monthly_price) {
        this.membership_monthly_price = membership_monthly_price;
    }

    public String getMembership_yearly_price() {
        return membership_yearly_price;
    }

    public void setMembership_yearly_price(String membership_yearly_price) {
        this.membership_yearly_price = membership_yearly_price;
    }

    public String getCategory_id_fromShop_Cart_Details() {
        return category_id_fromShop_Cart_Details;
    }

    public void setCategory_id_fromShop_Cart_Details(String category_id_fromShop_Cart_Details) {
        this.category_id_fromShop_Cart_Details = category_id_fromShop_Cart_Details;
    }


    public String getDateTime_ofMsgRecieveed() {
        return dateTime_ofMsgRecieveed;
    }

    public void setDateTime_ofMsgRecieveed(String dateTime_ofMsgRecieveed) {
        this.dateTime_ofMsgRecieveed = dateTime_ofMsgRecieveed;
    }

    public String getRecievers_Id() {
        return recievers_Id;
    }

    public void setRecievers_Id(String recievers_Id) {
        this.recievers_Id = recievers_Id;
    }

    public String getSenders_Id() {
        return senders_Id;
    }

    public void setSenders_Id(String senders_Id) {
        this.senders_Id = senders_Id;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }


    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getMsg_senders_name() {
        return msg_senders_name;
    }

    public void setMsg_senders_name(String msg_senders_name) {
        this.msg_senders_name = msg_senders_name;
    }

    public String getMsg_senders_image() {
        return msg_senders_image;
    }

    public void setMsg_senders_image(String msg_senders_image) {
        this.msg_senders_image = msg_senders_image;
    }
}
