package com.stembuddy.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.ReviewActivtiy;
import com.stembuddy.model.Notification_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;

import java.util.ArrayList;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class StemBudsAdapter extends BaseAdapter {
    private ArrayList<Notification_Model> datalist;
    private LayoutInflater layoutInflater;
    Context context;
    private int selectedPosition;

    public StemBudsAdapter(Context activity, ArrayList<Notification_Model> data) {
        this.context = activity;
        this.datalist = data;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public int getPosition(){
        return selectedPosition;
    }
    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        selectedPosition=position;
        View v = layoutInflater.inflate(R.layout.stem_buds_grid, viewGroup, false);
        try {
            Glide.with(context).load(datalist.get(position).getPhoto())
                    .placeholder(R.drawable.profile).transform(new CircleTransform(context))
                    .into((ImageView)v.findViewById(R.id.noti_grid_profile));
        }catch(Exception e){
            e.printStackTrace();
        }

        ((TextView)v.findViewById(R.id.noti_grid_name)).setText(datalist.get(position).getName());

        if(datalist.get(position).getProfession().equals("")){
            ((TextView) v.findViewById(R.id.noti_grid_profession)).setText("Not Mentioned");
        }else {
            ((TextView) v.findViewById(R.id.noti_grid_profession)).setText(datalist.get(position).getProfession());
        }
        try {
            if(SharedPreference_Main.getInstance(context).getUserType().equals("student")) {
                v.findViewById(R.id.noti_grid_ratting).setVisibility(View.VISIBLE);
                if (datalist.get(position).getRate().equals("")) {
                    ((RatingBar) v.findViewById(R.id.noti_grid_ratting)).setRating(Float.parseFloat("0"));
                } else {
                    ((RatingBar) v.findViewById(R.id.noti_grid_ratting)).setRating(Float.parseFloat(datalist.get(position).getRate()));
                }
            }else{
                v.findViewById(R.id.noti_grid_ratting).setVisibility(View.GONE);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        try{
            if(datalist.get(position).getOnline_offline().equals("1")){
                ((ImageView) v.findViewById(R.id.online_ofline)).setImageResource(R.drawable.online_icon);
            }else{
                ((ImageView) v.findViewById(R.id.online_ofline)).setImageResource(R.drawable.offline_icon);
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReviewActivtiy studentPageActivity = new ReviewActivtiy();
                Bundle b= new Bundle();
                b.putString("id" , datalist.get(position).getId());
                b.putString("name" , datalist.get(position).getName());
                b.putString("user_name" , datalist.get(position).getUser_name());
                b.putString("description" , datalist.get(position).getDescription());
                b.putString("affilation" , datalist.get(position).getAffiliation());
                b.putString("speciality" , datalist.get(position).getSpeciality());
                b.putString("photo" , datalist.get(position).getPhoto());
                b.putString("email" , datalist.get(position).getEmail());
                b.putString("mobile" , datalist.get(position).getMobile());
                b.putString("comment" , datalist.get(position).getComment());
                b.putString("user_type" , datalist.get(position).getUser_type());
                b.putString("online_offline" , datalist.get(position).getOnline_offline());
                b.putString("block" , datalist.get(position).getBlock_user());
                b.putString("rating" , datalist.get(position).getRate());
                b.putString("profession" , datalist.get(position).getProfession());
                studentPageActivity.setArguments(b);
                ((MainActivity)context).loadFragment(studentPageActivity, "Review_Activity", view);
            }
        });

        return v;
    }
}
