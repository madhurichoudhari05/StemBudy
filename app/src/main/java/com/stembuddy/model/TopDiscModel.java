package com.stembuddy.model;

/**
 * Created by THAKUR on 11/30/2017.
 */

public class TopDiscModel {
    private String profile;
    private String textSkills;
    private String post;
    private String views;
    private String id;
    private String description;
    private String title;
    private  String created_date;

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getTextSkills() {
        return textSkills;
    }

    public void setTextSkills(String textSkills) {
        this.textSkills = textSkills;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }
}
