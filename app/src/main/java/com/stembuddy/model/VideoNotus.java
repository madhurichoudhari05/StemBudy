package com.stembuddy.model;

import com.android.volley.Response;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoNotus {

    @SerializedName("response")
    @Expose
    private Response_ response;
    @SerializedName("to_data")
    @Expose
    private ToData toData;
    @SerializedName("mentor_data")
    @Expose
    private MentorData mentorData;

    public Response_ getResponse() {
        return response;
    }

    public void setResponse(Response_ response) {
        this.response = response;
    }

    public ToData getToData() {
        return toData;
    }

    public void setToData(ToData toData) {
        this.toData = toData;
    }

    public MentorData getMentorData() {
        return mentorData;
    }

    public void setMentorData(MentorData mentorData) {
        this.mentorData = mentorData;
    }

}
