package com.stembuddy.activities;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/22/2017.
 */

public class Registration_stem extends BaseActivity implements View.OnClickListener, View.OnFocusChangeListener, View.OnKeyListener, TextWatcher {
    private RadioGroup user;
    private EditText name;
    private EditText email;
    private EditText pwd;
    private EditText userName;
    private EditText mPinFirstDigitEditText;
    private EditText mPinSecondDigitEditText;
    private EditText mPinThirdDigitEditText;
    private EditText mPinForthDigitEditText;
    private EditText mPinFifthDigitEditText;
    private EditText mPinSixthDigitEditText;
    private EditText mPinHiddenEditText;
    private SharedPreference_Main sharedPreference_main;
    String userType = "student", s1;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registraion_layout);
        sharedPreference_main = SharedPreference_Main.getInstance(this);
        initView();
        setPINListeners();
        findViewById(R.id.regi_register).setOnClickListener(this);
        findViewById(R.id.regi_sign_in).setOnClickListener(this);
        user.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkId) {
                if (checkId == R.id.radio1) {
                    userType = "student";
//                    Toast.makeText(Registration_stem.this , ""+userType,Toast.LENGTH_SHORT).show();
                } else {
                    userType = "mentor";
//                    Toast.makeText(Registration_stem.this,""+userType,Toast.LENGTH_SHORT).show();

                }
            }
        });


        userName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                userName.setFocusableInTouchMode(true);
                userName.requestFocus();
//                if(keyEvent.getAction() == keyEvent.KEYCODE_ENTER || keyEvent.getAction() == keyEvent.ACTION_DOWN){
                if (i == EditorInfo.IME_ACTION_DONE || i == keyEvent.ACTION_DOWN) {
                    if (email.getText().toString().trim().equalsIgnoreCase("")
                            || userName.getText().toString().trim().equalsIgnoreCase("") ||
                            getPassord().equalsIgnoreCase("")
                            || name.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(getApplicationContext(), "Please fill necessary Fields", Toast.LENGTH_SHORT).show();
                    } else {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                        onRegister();
                    }
                }
                return true;
            }
        });


    }


    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);

        mPinFirstDigitEditText.setOnFocusChangeListener(this);
        mPinSecondDigitEditText.setOnFocusChangeListener(this);
        mPinThirdDigitEditText.setOnFocusChangeListener(this);
        mPinForthDigitEditText.setOnFocusChangeListener(this);
        mPinFifthDigitEditText.setOnFocusChangeListener(this);
        mPinSixthDigitEditText.setOnFocusChangeListener(this);

        mPinFirstDigitEditText.setOnKeyListener(this);
        mPinSecondDigitEditText.setOnKeyListener(this);
        mPinThirdDigitEditText.setOnKeyListener(this);
        mPinForthDigitEditText.setOnKeyListener(this);
        mPinFifthDigitEditText.setOnKeyListener(this);
        mPinSixthDigitEditText.setOnKeyListener(this);
        mPinHiddenEditText.setOnKeyListener(this);
    }

    private void initView() {
        user = (RadioGroup) findViewById(R.id.regis_radio_group);
        name = (EditText) findViewById(R.id.regi_name);
        //  pwd=(EditText)findViewById(R.id.regi_pwd);
        email = (EditText) findViewById(R.id.regi_email);
        userName = findViewById(R.id.regi_user_id);

        mPinFirstDigitEditText = (EditText) findViewById(R.id.pin1);
        mPinSecondDigitEditText = (EditText) findViewById(R.id.pin2);
        mPinThirdDigitEditText = (EditText) findViewById(R.id.pin3);
        mPinForthDigitEditText = (EditText) findViewById(R.id.pin4);
        mPinFifthDigitEditText = (EditText) findViewById(R.id.pin5);
        mPinSixthDigitEditText = (EditText) findViewById(R.id.pin6);
        mPinHiddenEditText = (EditText) findViewById(R.id.pin_hidden_edittext);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.regi_register:


                if (email.getText().toString().trim().equalsIgnoreCase("")
                        || getPassord().equalsIgnoreCase("") ||
                        userName.getText().toString().trim().equalsIgnoreCase("")
                        || name.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(this, "Please fill necessary Fields", Toast.LENGTH_SHORT).show();
                } else {
                    //hideSoftKeyBoard(view);
                    if (isNetworkEnabled(Registration_stem.this)) {
                        onRegister();
                    }
                    else {
                        Toast.makeText(Registration_stem.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;




            case R.id.regi_sign_in:
                startActivity(new Intent(this, Login.class));
                break;
        }
    }

    private void onRegister() {
        String uname = name.getText().toString().trim();
        String upass = getPassord().trim();
        // String upass=pwd.getText().toString().trim();
        String uemail = email.getText().toString().trim();
        String user_name = userName.getText().toString().trim();
        if (checkValidity(uname, upass, uemail, user_name)) {
            hitRegiApi(uname, upass, uemail, user_name);
        } else {
            Toast.makeText(this, "Please Enter All Fields", Toast.LENGTH_LONG).show();
        }
    }

    public boolean checkValidity(String uname, String upwd, String uemail, String user_name) {
        if (TextUtils.isEmpty(uname)) {
            name.setError("Enter Name");
            return false;
        } else if (TextUtils.isEmpty(uemail)) {
            email.setError("Enter mail");
            return false;
        } else if (!uemail.matches(emailPattern)) {
            email.setError("Enter valid mail");
            return false;
        } else if (TextUtils.isEmpty(upwd)) {
            pwd.setError("Enter password");
            return false;
        } else if (TextUtils.isEmpty(user_name)) {
            Toast.makeText(this, "Please Enter Pin", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;

        }
    }

    private void hitRegiApi(final String uname, final String pass, final String uemail, final String user_name) {
        final ProgressDialog dialog = new ProgressDialog(Registration_stem.this);
        dialog.setMessage("Please Wait");
        dialog.setCancelable(false);
        dialog.show();

        StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            dialog.dismiss();

                            Log.e("register",response);
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONObject regJsonObject = jsonObject.getJSONObject("response");
                                SharedPreference_Main preferenceMain = SharedPreference_Main.getInstance(Registration_stem.this);
                                preferenceMain.setUserId(regJsonObject.getString("id"));
                                preferenceMain.setUserType(regJsonObject.getString("user_type"));
                                preferenceMain.setName(regJsonObject.getString("name"));
                                preferenceMain.setUserName(regJsonObject.getString("user_name"));
                                preferenceMain.setEmail(regJsonObject.getString("email"));
                                preferenceMain.setUserPwd(regJsonObject.getString("pass"));
                                preferenceMain.setSession_login(true);
                                Intent i = new Intent(Registration_stem.this, MainActivity.class);
                                startActivity(i);
                                Registration_stem.this.finish();
                            } else {
                                Toast.makeText(Registration_stem.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(Registration_stem.this, "Execption ", Toast.LENGTH_LONG).show();

                            dialog.dismiss();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VE ", volleyError.getMessage());
                Toast.makeText(Registration_stem.this, "Something went Wrong", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "register");
                params.put("user_type", userType);
                params.put("name", uname);
                params.put("email", uemail);
                params.put("pass", pass);
                params.put("user_name", user_name);

                if (sharedPreference_main.getFireBaseTokenid().equalsIgnoreCase("")) {
                    sharedPreference_main.setFireBaseTokenid(FirebaseInstanceId.getInstance().getToken());
                    params.put("mobile_token",FirebaseInstanceId.getInstance().getToken());
                    Log.d("deviceMobileTokenSIF",FirebaseInstanceId.getInstance().getToken());

                }
                else {

                    params.put("mobile_token",sharedPreference_main.getFireBaseTokenid());
                    Log.d("deviceMobileTokenSELSE",sharedPreference_main.getFireBaseTokenid());


                }





                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(sr);

    }


    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.pin1:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin2:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin3:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin4:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin5:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;
            case R.id.pin6:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.pin_hidden_edittext:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (mPinHiddenEditText.getText().length() == 6)
                            mPinSixthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 5)
                            mPinFifthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 4)
                            mPinForthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 3)
                            mPinThirdDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 2)
                            mPinSecondDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 1)
                            mPinFirstDigitEditText.setText("");

                        if (mPinHiddenEditText.length() > 0)
                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));

                        return true;
                    }

                    break;

                default:
                    return false;
            }
        }

        return false;
    }

    /**
     * Sets default PIN background.
     *
     * @param editText edit text to change
     */
    private void setDefaultPinBackground(EditText editText) {
    }

    private void setFocusedPinBackground(EditText editText) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        setDefaultPinBackground(mPinFirstDigitEditText);
        setDefaultPinBackground(mPinSecondDigitEditText);
        setDefaultPinBackground(mPinThirdDigitEditText);
        setDefaultPinBackground(mPinForthDigitEditText);
        setDefaultPinBackground(mPinFifthDigitEditText);

        if (s.length() == 0) {
            setFocusedPinBackground(mPinFirstDigitEditText);
            mPinFirstDigitEditText.setText("");
        } else if (s.length() == 1) {
            setFocusedPinBackground(mPinSecondDigitEditText);
            mPinFirstDigitEditText.setText(s.charAt(0) + "");
            mPinSecondDigitEditText.setText("");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 2) {
            setFocusedPinBackground(mPinThirdDigitEditText);
            mPinSecondDigitEditText.setText(s.charAt(1) + "");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 3) {
            setFocusedPinBackground(mPinForthDigitEditText);
            mPinThirdDigitEditText.setText(s.charAt(2) + "");
            mPinForthDigitEditText.setText("");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");

        } else if (s.length() == 4) {
            setFocusedPinBackground(mPinFifthDigitEditText);
            mPinForthDigitEditText.setText(s.charAt(3) + "");
            mPinFifthDigitEditText.setText("");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 5) {
            setDefaultPinBackground(mPinSixthDigitEditText);
            mPinFifthDigitEditText.setText(s.charAt(4) + "");
            mPinSixthDigitEditText.setText("");
        } else if (s.length() == 6) {
            setDefaultPinBackground(mPinSixthDigitEditText);
            mPinSixthDigitEditText.setText(s.charAt(5) + "");
            hideSoftKeyboard(mPinFifthDigitEditText);
        }
    }


// useless==================================================================================================

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }


    public class MainLayout extends LinearLayout {

        public MainLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            inflater.inflate(R.layout.activity_main, this);

        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            final int proposedHeight = MeasureSpec.getSize(heightMeasureSpec);
            final int actualHeight = getHeight();

            Log.d("TAG", "proposed: " + proposedHeight + ", actual: " + actualHeight);

            if (actualHeight >= proposedHeight) {
                // Keyboard is shown
                if (mPinHiddenEditText.length() == 0)
                    setFocusedPinBackground(mPinFirstDigitEditText);
                else
                    setDefaultPinBackground(mPinFirstDigitEditText);
            }

            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public String getPassord() {
        String s1 = "";


        s1 = mPinFirstDigitEditText.getText().toString().trim();
        s1 += mPinSecondDigitEditText.getText().toString().trim();
        s1 += mPinThirdDigitEditText.getText().toString().trim();
        s1 += mPinForthDigitEditText.getText().toString().trim();
        s1 += mPinFifthDigitEditText.getText().toString().trim();
        s1 += mPinSixthDigitEditText.getText().toString().trim();

        return s1;
    }


}
