package com.stembuddy.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.iid.FirebaseInstanceId;
import com.stembuddy.R;
import com.stembuddy.Services.LogoutService;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.All_comments_On_Profile;
import com.stembuddy.fragment.MainHome_Fragment;
import com.stembuddy.fragment.NotificationList;
import com.stembuddy.model.ImageViewModel;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Config;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private SharedPreference_Main sharedPreference_main;
    private DrawerLayout drawer;
    private TextView txtHeader;
    private TextView drawer_name;
    private TextView redDot;
    private ImageView appbar_profile;
    private ImageView drawer_profile;
    private String file_path = "";
    private Toolbar toolbar;
    BroadcastReceiver broadcastReceiver;
    private ArrayList<String> backStack;
    private Intent sinch_service;
    private String permissions[] = {Manifest.permission.RECORD_AUDIO,
            Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.CAMERA,
            Manifest.permission.RECEIVE_BOOT_COMPLETED};

    private boolean doubleBackToExitPressedOnce = false;
    public static ArrayList<String> block_Arraylist;
    private int count = 0;
    boolean b = true;
    private ImageViewModel mModel;
//    private  Toolbar toolbar1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreference_main = SharedPreference_Main.getInstance(this);
        try {
            final Intent intent = new Intent();
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("oneplus".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.oneplus.security", "com.oneplus.security.chainlaunch.view.ChainLaunchAppListAct‌​ivity"));
            }
            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (list.size() > 0) {
                if (sharedPreference_main.geta().equalsIgnoreCase("a")) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Auto Start On")
                            .setMessage("Please Enable Autostart of app for proper working of apliction")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(intent);
                                    sharedPreference_main.seta("b");
                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    sharedPreference_main.seta("a");
                                }
                            })
                            .show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backStack = new ArrayList<>();
        sharedPreference_main = SharedPreference_Main.getInstance(this);

        mModel= ViewModelProviders.of(this).get(ImageViewModel.class);

        Observer<String> observer=new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                Toast.makeText(MainActivity.this, "obser"+s, Toast.LENGTH_SHORT).show();
            }
        };
        mModel.getmCurrentImage().observe(this,observer);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
//         toolbar1 = (Toolbar)findViewById(R.id.toolbar1);
        checkPermissions();


        /*try {
            sinch_service = new Intent(MainActivity.this, SinchService.class);
            startService(sinch_service);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

//        try {
//            if (!getSinchServiceInterface().isStarted()) {
//                getSinchServiceInterface().startClient(sharedPreference_main.getName());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        Intent intent = new Intent(MainActivity.this, LogoutService.class);
//        intent.putExtra("service_stop", "stop");
//        startService(intent);

        txtHeader = (TextView) findViewById(R.id.header_txt);
        findViewById(R.id.notification_list).setOnClickListener(this);
        setSupportActionBar(toolbar);
//        setSupportActionBar(toolbar1);
        appbar_profile = findViewById(R.id.appbar_profile);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.mipmap.menu_button);
//        toolbar1.setNavigationIcon(R.drawable.menu);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //this.loadFragment(new Service_provider_fragment(),"Service Provider");
        View view = navigationView.getHeaderView(0);
        loadFragment(new MainHome_Fragment(), "Home", getWindow().getDecorView().getRootView());
        drawer_profile = view.findViewById(R.id.drawe_profile_pic);
        drawer_name = view.findViewById(R.id.drawer_txt_name);
        redDot = findViewById(R.id.redDot);
        view.findViewById(R.id.nav_home).setOnClickListener(this);
        view.findViewById(R.id.nav_about_us).setOnClickListener(this);
        view.findViewById(R.id.nav_testimonial).setOnClickListener(this);
        view.findViewById(R.id.nav_change_password).setOnClickListener(this);
        view.findViewById(R.id.nav_contact_us).setOnClickListener(this);
        view.findViewById(R.id.nav_logout).setOnClickListener(this);

        navigationView.setNavigationItemSelectedListener(this);
        setProfile_Name();

        try {
            Glide.with(MainActivity.this).load(sharedPreference_main.getUserProfile()).placeholder(R.drawable.profile).transform(new CircleTransform(MainActivity.this)).into(appbar_profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isNetworkEnabled(MainActivity.this)) {
            if (sharedPreference_main.getFireBaseTokenid().equalsIgnoreCase("")) {
                sharedPreference_main.setFireBaseTokenid(FirebaseInstanceId.getInstance().getToken());
                if (isNetworkEnabled(MainActivity.this)) {
                    hitApiToupdateToken();
                } else {
                    noInternet();
                }
            } else {
                if (isNetworkEnabled(MainActivity.this)) {
                    hitShowFavApi("0", 1);
                }
            }
        }
        try {

                    if (getIntent().getExtras().getString("message").equalsIgnoreCase("New comment on topic")) {
                TopDiscussion2 topDiscussion2 = new TopDiscussion2();
                Bundle bundle = new Bundle();
                bundle.putString("posts_id", getIntent().getExtras().getString("topic_id"));
                topDiscussion2.setArguments(bundle);
                loadFragment(topDiscussion2, "Top_Discussion2", getWindow().getDecorView().getRootView());
            } else if (getIntent().getExtras().getString("message").equalsIgnoreCase("New comment for user")) {
                All_comments_On_Profile topDiscussion2 = new All_comments_On_Profile();
                Bundle bundle = new Bundle();
                bundle.putString("comments_ids", getIntent().getExtras().getString("comments_ids"));
                topDiscussion2.setArguments(bundle);
                loadFragment(topDiscussion2, "Comment_on_profile", getWindow().getDecorView().getRootView());
            } else if (getIntent().getExtras().getString("message").equalsIgnoreCase("Your Request accept")) {
                loadFragment(new Stem_Buds(), "Stem_Bud", view);
            } else if (!getIntent().getExtras().getString("message").equalsIgnoreCase("")) {
                NotificationList notificationList = new NotificationList();
                Bundle bundle = new Bundle();
                bundle.putString("from_activity", "main_activity");
                notificationList.setArguments(bundle);
                loadFragment(notificationList, "Notifications", view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onStop() {
        super.onStop();

        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(broadcastReceiver);
    }

    private void checkPermissions() {
        int result;
        List<String> list = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                list.add(p);
            }
        }
        if (!list.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(list.toArray(new String[list.size()]), 100);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

      //  Toast.makeText(this, "onresume", Toast.LENGTH_SHORT).show();
        setProfile_Name();
        try {
            Glide.with(MainActivity.this).load(sharedPreference_main.getUserProfile()).placeholder(R.drawable.profile).transform(new CircleTransform(MainActivity.this)).into(appbar_profile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                redDot.setVisibility(View.VISIBLE);
            }
        };

        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(broadcastReceiver, new IntentFilter(Config.PUSH_NOTIFICATION));
    }

    @Override
    public void onBackPressed() {
        if (b) {
            b = true;

            int index = getSupportFragmentManager().getBackStackEntryCount();
            if (index == 1) {
                if (doubleBackToExitPressedOnce) {
//                super.onBackPressed();
                    finishAffinity();
                    return;
                } else {
//            loadFragment(new MainHome_Fragment(), "Home", getWindow().getDecorView().getRootView());
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                b = true;
                FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index - 2);
                String tag = backEntry.getName();
                if (tag.equalsIgnoreCase("com.stembuddy.fragment.MainHome_Fragment")) {
                    findViewById(R.id.notification_list).setVisibility(View.VISIBLE);
                    findViewById(R.id.app_bar_layout).setVisibility(View.VISIBLE);
                    findViewById(R.id.stem_buddy_bg_colr).setBackgroundColor(getResources().getColor(R.color.header_color));
                    findViewById(R.id.header_txt).setVisibility(View.VISIBLE);
                    toolbar.setNavigationIcon(R.mipmap.menu_button);
                    toolbar.setBackgroundColor(getResources().getColor(R.color.header_color));
                    b = false;
                }
                super.onBackPressed();
            }
        } else {
            if (doubleBackToExitPressedOnce) {
//                super.onBackPressed();
                finishAffinity();
                return;
            } else {
//            loadFragment(new MainHome_Fragment(), "Home", getWindow().getDecorView().getRootView());
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.notification_list:
                hideSoftKeyBoard(view);
                NotificationList notificationList = new NotificationList();
                Bundle bundle = new Bundle();
                bundle.putString("from_activity", "main_activity");
                notificationList.setArguments(bundle);
                loadFragment(notificationList, "Notifications", view);
//                toolbar1.setVisibility(View.GONE);

                redDot.setVisibility(View.GONE);

                break;
            case R.id.nav_home:
                hideSoftKeyBoard(view);
                loadFragment(new MainHome_Fragment(), "Home", view);
                toolbar.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_about_us:
                hideSoftKeyBoard(view);
                loadFragment(new AboutUs(), "About_us", view);


                break;
            case R.id.nav_testimonial:
                hideSoftKeyBoard(view);
                loadFragment(new TestimonialActivity1(), "Testimonial", view);

                break;
            case R.id.nav_change_password:
                hideSoftKeyBoard(view);
                loadFragment(new Setting_fragment(), "setting_fragment", view);
                break;

            case R.id.nav_contact_us:
                hideSoftKeyBoard(view);
                loadFragment(new Contact_Us(), "Contact_us", view);
                break;
            case R.id.nav_logout:

//                startActivity(new Intent(this,Login.class));
//                logout();
//                finish();
                if (isNetworkEnabled(MainActivity.this))
                    hitLogoutApi("logout");
                else
                    Toast.makeText(MainActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                break;

        }
        drawer.closeDrawer(GravityCompat.START);

    }


    private void hitLogoutApi(final String logout) {
        String url = Constants.BASE_URL;
//        showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    dismissLoader();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                        if (logout.equalsIgnoreCase("logout")) {
                            String message = jsonObject.getString("message");
                            logout(message);
                        } else {
                            Log.e("lkdfhlkhdfkrgyetsyeylh", "khgdfhgdfhgsdhkfghukfgtwee");
                        }
                    } else {
                        Log.e("lkdfhlkhdfklhretyertyer", "khgdfhgdfhgsdhkfghukfgtwee");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("lkdfhlkhdfklhskflghf", "khgdfhgdfhgsdhkfghukfgtwee");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("lkdfhlkhdfklhqwerqdg", "khgdfhgdfhgsdhkfghukfgtwee");
                error.getMessage();
//                dismissLoader();
                showToast("Network error... Please try again");
//                logout();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "logout");
                params.put("user_id", SharedPreference_Main.getInstance(MainActivity.this).getUserId());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "logout");
    }

    public void loadFragment(final Fragment fragment, String title, View view) {
        hideSoftKeyBoard(view);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container1, fragment).addToBackStack(title);
        transaction.commit();

//        backStack.add(title);
        b = true;
        transaction.addToBackStack(fragment.getClass().getName());


//        if (title.equals("setting_fragment") || title.equals("change_password")) {
        if (title.equals("setting_fragment") || title.equals("change_password") || title.equals("alert")) {
            findViewById(R.id.header_txt).setVisibility(View.VISIBLE);
            findViewById(R.id.notification_list).setVisibility(View.VISIBLE);
            findViewById(R.id.app_bar_layout).setVisibility(View.VISIBLE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.header_color));
            ((LinearLayout) findViewById(R.id.stem_buddy_bg_colr)).setBackgroundColor(getResources().getColor(R.color.header_color));
            toolbar.setNavigationIcon(R.mipmap.menu_button);
        } else {
            findViewById(R.id.header_txt).setVisibility(View.GONE);
            findViewById(R.id.notification_list).setVisibility(View.GONE);
            findViewById(R.id.app_bar_layout).setVisibility(View.GONE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.white));
            try {
                ((LinearLayout) findViewById(R.id.stem_buddy_bg_colr)).setBackgroundColor(getResources().getColor(R.color.white));
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            toolbar.setNavigationIcon(R.mipmap.menu_but);
        }
        if (title.equals("About_us") || title.equals("Testimonial") || title.equals("Contact_us") ||
                title.equals("Stem_Bud") || title.equals("Stem_find_buddy") || title.equals("StemBuddyFindResult()")
                || title.equals("Student_page") || title.equals("Top_discussion") || title.equals("Top_Discussion2")
                || title.equals("video_store") || title.equals("Videos_activity") || title.equals("Review_Activity") || title.equals("Videos_activity_user")
                || title.equals("Student_more_info") || title.equals("testimonial_activity") || title.equals("UploadInformation")
                || title.equals("edit_profile") || title.equalsIgnoreCase("Comment_on_profile") || title.equals("Videos_activity_sb_community")
                ) {
            findViewById(R.id.header_txt).setVisibility(View.GONE);
            findViewById(R.id.notification_list).setVisibility(View.GONE);
            findViewById(R.id.app_bar_layout).setVisibility(View.GONE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.white));
            try {
                ((LinearLayout) findViewById(R.id.stem_buddy_bg_colr)).setBackgroundColor(getResources().getColor(R.color.white));
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
            toolbar.setNavigationIcon(R.mipmap.menu_but);
        } else {
            findViewById(R.id.header_txt).setVisibility(View.VISIBLE);
            findViewById(R.id.notification_list).setVisibility(View.VISIBLE);
            findViewById(R.id.app_bar_layout).setVisibility(View.VISIBLE);
            toolbar.setBackgroundColor(getResources().getColor(R.color.header_color));
            ((LinearLayout) findViewById(R.id.stem_buddy_bg_colr)).setBackgroundColor(getResources().getColor(R.color.header_color));
            toolbar.setNavigationIcon(R.mipmap.menu_button);
        }
//        }
    }

    public void logout(String message) {
        SharedPreference_Main preferenceMain = SharedPreference_Main.getInstance(MainActivity.this);
        preferenceMain.setSession_login(false);
        preferenceMain.remove_Preference();
        try {
//            stopService(sinch_service);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        restartApp();
    }

    private void restartApp() {
        Intent intent = new Intent(MainActivity.this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finishAffinity();

    }


    public void setProfile_Name() {

        //Toast.makeText(this, "setPerofile", Toast.LENGTH_SHORT).show();
        txtHeader.setText(sharedPreference_main.getUserName());
        drawer_name.setText(sharedPreference_main.getUserName());
        if (!sharedPreference_main.getUserProfile().equals("")) {

            Glide.with(this).load(sharedPreference_main.getUserProfile()).asBitmap().centerCrop().into(new BitmapImageViewTarget(appbar_profile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(MainActivity.this.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    appbar_profile.setImageDrawable(circularBitmapDrawable);
                }
            });
            Glide.with(this).load(sharedPreference_main.getUserProfile()).asBitmap().centerCrop().into(new BitmapImageViewTarget(drawer_profile) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(MainActivity.this.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    drawer_profile.setImageDrawable(circularBitmapDrawable);
                }
            });

        }

    }

    private void hitApiToupdateToken() {
        showLoader();
        String url = Constants.BASE_URL;
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    dismissLoader();
                    Toast.makeText(MainActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    showToast("Something Went Wrong...");
                } finally {
                    hitShowFavApi("0", 2);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                dismissLoader();
                showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "token_update_profile");
                params.put("user_id", SharedPreference_Main.getInstance(MainActivity.this).getUserId());
                params.put("mobile_token", sharedPreference_main.getFireBaseTokenid());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr);
    }

/*Madhu*/
    private void hitShowFavApi(final String s, final int num_val) {
        String url = Constants.BASE_URL;
        if (num_val == 1) {
            ((BaseActivity) MainActivity.this).showLoader();
        }
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("mentor/student",response);
                try {
//                    if (s.equalsIgnoreCase("1")) {
//                        ((BaseActivity) MainActivity.this).dismissLoader();
//                    }
                    if (s.equalsIgnoreCase("0")) {
                        ((MainActivity) MainActivity.this).block_Arraylist = new ArrayList<>();
                    }
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
//                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                            JSONObject object = jsonObject.getJSONObject("Mentor");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("details");
                            JSONObject object = jsonObject.getJSONObject("response");
                            if (SharedPreference_Main.getInstance(MainActivity.this).getUserType().equalsIgnoreCase("student")) {
                                if (jsonObject1.getString("mentor_id").equalsIgnoreCase(object.getString("id"))) {
                                    ((MainActivity) MainActivity.this).block_Arraylist.add(object.getString("id"));
                                }
                            } else {
                                if (jsonObject1.getString("student_id").equalsIgnoreCase(object.getString("id"))) {
                                    ((MainActivity) MainActivity.this).block_Arraylist.add(object.getString("id"));
                                }
                            }
                        }

//                        blockUserAdapter = new BlockUserAdapter(MainActivity.this, aList, getContext(), getArguments().getString("from_activity"));
//                        block_list.setAdapter(blockUserAdapter);


                    } else {
//                        if (s.equalsIgnoreCase("1")) {
//                            if (aList.size() == 0) {
//                                block_list.setVisibility(View.GONE);
//                                text_no_result_found.setVisibility(View.VISIBLE);
//                            } else {
//                                block_list.setVisibility(View.VISIBLE);
//                                text_no_result_found.setVisibility(View.GONE);
//                            }
//                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (count == 0) {
                        count++;
                        hitShowFavApi("1", 1);
                    } else {
                        count = 0;
//                        if (aList.size()==0) {
                        hitApiGetAccept("0");
//                        }
                    }
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
//                if (s.equalsIgnoreCase("1")) {
                ((BaseActivity) MainActivity.this).dismissLoader();
//                }
                ((BaseActivity) MainActivity.this).showToast("Network error... Please try again");
            }
        })

        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
//                if (SharedPreference_Main.getInstance(MainActivity.this).getUserType().equals("student")) {
//                    params.put("rule", "all_fav_and_block_by_student");
//                } else {
//                    params.put("rule", "all_fav_and_block_by_mentor");
//                }
//                params.put("user_id", SharedPreference_Main.getInstance(MainActivity.this).getUserId());
//                    params.put("status", s); //1 for getting unblock favourtes.....0 for getting blocked contacts
                if (SharedPreference_Main.getInstance(MainActivity.this).getUserType().equals("student")) {
                    params.put("rule", "accepted_by_student");
                    params.put("student_id", SharedPreference_Main.getInstance(MainActivity.this).getUserId());
                } else {
                    params.put("rule", "accepted_by_mentor");
                    params.put("mentor_id", SharedPreference_Main.getInstance(MainActivity.this).getUserId());
                }
                params.put("stauts", s);
                return params;
            }
        };
        AppController.getInstance().

                addToRequestQueue(sr, "show_fav");
    }

    private void hitApiGetAccept(final String s) {
        String url = Constants.BASE_URL;

        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Log.e("accepteByStudent",response);
                try {
                    if (s.equalsIgnoreCase("1")) {
                        ((BaseActivity) MainActivity.this).dismissLoader();
                    }

                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("details");
                            JSONObject object = jsonObject.getJSONObject("response");
                            if (SharedPreference_Main.getInstance(MainActivity.this).getUserType().equalsIgnoreCase("student")) {
                                if (jsonObject1.getString("mentor_id").equalsIgnoreCase(object.getString("id"))) {
                                    ((MainActivity) MainActivity.this).block_Arraylist.add(object.getString("id"));

                                   // Toast.makeText(MainActivity.this, "mentor_id::"+jsonObject1.getString("mentor_id"), Toast.LENGTH_SHORT).show();
                                  //  Toast.makeText(MainActivity.this, "id::"+object.getString("id"), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                if (jsonObject1.getString("student_id").equalsIgnoreCase(object.getString("id"))) {
                                    ((MainActivity) MainActivity.this).block_Arraylist.add(object.getString("id"));

                                  //  Toast.makeText(MainActivity.this, "student_id::"+jsonObject1.getString("student_id"), Toast.LENGTH_SHORT).show();
                                   // Toast.makeText(MainActivity.this, "id::"+object.getString("id"), Toast.LENGTH_SHORT).show();
                                }
                            }
//                            ((MainActivity) MainActivity.this).block_Arraylist.add(object.getString("id"));
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (count == 0) {
                        count++;
                        hitApiGetAccept("1");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) MainActivity.this).dismissLoader();
                ((BaseActivity) MainActivity.this).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(MainActivity.this).getUserType().equals("student")) {
                    params.put("rule", "accepted_by_mentor_all");
                    params.put("student_id", SharedPreference_Main.getInstance(MainActivity.this).getUserId());
                } else {
                    params.put("rule", "accepted_by_student_all");
                    params.put("mentor_id", SharedPreference_Main.getInstance(MainActivity.this).getUserId());
                }
                params.put("stauts", s);
//                params.put("status", "1"); //1 for getting unblock favourtes.....0 for getting blocked contacts
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "show_fav");
    }


    @SuppressLint("LongLogTag")
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("lkdfhlkhdfklhqfgdfgdfwerqdgbfgbcdcewqrfaskflghfgsdhfg", "khgdfhgdfhgsdhsdfgsdfgsdfgkfghukfgtwee");
//        hitLogoutApi("notLogout");
        Intent intent = new Intent(MainActivity.this, LogoutService.class);
        intent.putExtra("service_stop", "Unstop");
        startService(intent);

//        Intent intent = new Intent(MainActivity.this, LogoutReceiver.class);
//        sendBroadcast(intent);
        Log.e("lkdfhlkhdfklhqfgdfgdfwerqdgbfgbcdcewqrfaskflghfgsdhfg", "khgdfhgdfhgsdhkfghukfgtwee");
    }
}
