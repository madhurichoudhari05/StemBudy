package com.stembuddy.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.Database.DB_Chat;
import com.stembuddy.R;
import com.stembuddy.activities.ChatActivity;
import com.stembuddy.model.ModelScan;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;

import java.util.ArrayList;

public class Chat extends Fragment {
    private ListView chatList;
    private ArrayList<ModelScan> arraylist_chatlist;
    private SharedPreference_Main sharedPreference_main;
    private ArrayList<ModelScan> uniqueArray;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat, container, false);
        initViews();
        chatList = (ListView) view.findViewById(R.id.chat_lst);

        InnerAdapter1 innerAdapter1 = new InnerAdapter1(getActivity(), uniqueArray);
        chatList.setAdapter(innerAdapter1);

        chatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                intent.putExtra("from", "ChatFragment");
                String receipientid = "";
                if (uniqueArray.get(position).getSenders_Id().equalsIgnoreCase(sharedPreference_main.getUserId())) {
                    receipientid = uniqueArray.get(position).getRecievers_Id();//here recipients id is other persons id .
                } else {
                    receipientid = uniqueArray.get(position).getSenders_Id();
                }
                intent.putExtra("KEY_USER_ID", receipientid);
                intent.putExtra("KEY_PROFILE_IMAGE", uniqueArray.get(position).getMsg_senders_image());
                intent.putExtra("KEY_FULLNAME", uniqueArray.get(position).getMsg_senders_name());

                startActivity(intent);

            }
        });
        return view;
    }

    private void initViews() {
        sharedPreference_main = new SharedPreference_Main(getActivity());
        DB_Chat db_chat = DB_Chat.getInstance(getActivity());
        arraylist_chatlist = db_chat.get_DB_Chat_Details();
        uniqueArray = new ArrayList<>();

        for (int i = 0; i < arraylist_chatlist.size(); i++) {
            boolean status = true;
            if (uniqueArray.size() == 0) {
                uniqueArray.add(arraylist_chatlist.get(i));
            }
            for (int k = 0; k < uniqueArray.size(); k++) {
                if (getOthersIDForThisModel(uniqueArray.get(k)).equalsIgnoreCase(getOthersIDForThisModel(arraylist_chatlist.get(i)))) {
                    status = false;
                }
            }
            if (status)
                uniqueArray.add(arraylist_chatlist.get(i));
        }
    }

    private String getOthersIDForThisModel(ModelScan modelScan_hopon) {
        if (modelScan_hopon.getSenders_Id().equalsIgnoreCase(sharedPreference_main.getUserId())) {
            return modelScan_hopon.getRecievers_Id();
        } else {
            return modelScan_hopon.getSenders_Id();
        }
    }

    private class InnerAdapter1 extends BaseAdapter {
        FragmentActivity activity;
        ArrayList<ModelScan> arraylist_chatlist;

        public InnerAdapter1(FragmentActivity activity, ArrayList<ModelScan> arraylist_chatlist) {
            this.activity = activity;
            this.arraylist_chatlist = arraylist_chatlist;
        }

        @Override
        public int getCount() {
            return arraylist_chatlist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v1 = LayoutInflater.from(activity).inflate(R.layout.custom_chat_list, parent, false);
            TextView nameTxt = (TextView) v1.findViewById(R.id.name_chat);
            TextView msgTxt = (TextView) v1.findViewById(R.id.msgTxt);
            TextView time_Txt = (TextView) v1.findViewById(R.id.time_chat);
            ImageView image_chat = (ImageView) v1.findViewById(R.id.propic_chat);

            long timestampString = Long.parseLong(arraylist_chatlist.get(position).getDateTime_ofMsgRecieveed()) * 1000;
            String value = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").
                    format(new java.util.Date(timestampString));

            time_Txt.setText(value);

            try {
                Glide.with(activity).load(arraylist_chatlist.get(position).getMsg_senders_image())
                        .transform(new CircleTransform(getActivity()))
                        .into(image_chat);
            } catch (Exception e) {
                e.printStackTrace();
            }

//            image_chat.setImageURI(Uri.parse(arraylist_chatlist.get(position).getImage_chat()));
            nameTxt.setText(arraylist_chatlist.get(position).getMsg_senders_name().substring(0, 1).toUpperCase() +
                    arraylist_chatlist.get(position).getMsg_senders_name().substring(1, arraylist_chatlist.get(position).getMsg_senders_name().length()));
            msgTxt.setText(arraylist_chatlist.get(position).getMessage());

            return v1;
        }
    }
}
