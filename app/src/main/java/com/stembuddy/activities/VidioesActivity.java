package com.stembuddy.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.adapter.SpecificUserVideoAdapter;
import com.stembuddy.adapter.VideosAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.SpecificUserVideo_Model;
import com.stembuddy.model.Videos_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class VidioesActivity extends Fragment implements View.OnClickListener {


    private ListView vid_list;
    private View v;
    private ArrayList<Videos_Model> arrayList;
    private ArrayList<SpecificUserVideo_Model> specificUserVideoArrayList;
    private VideosAdapter adapter;
    private RelativeLayout relativeLayout;
    private TextView error_tv;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.vedioes_layout, container, false);
        if (getArguments().getString("from").equalsIgnoreCase("Videos_activity_user") || getArguments().getString("from").equalsIgnoreCase("Specific_user_from")) {
            getData_perticular_user();
        } else {
            if (getArguments().getString("from").equalsIgnoreCase("mock_interview")) {
                getData_mock_interview();
            } else if (getArguments().getString("from").equalsIgnoreCase("Videos_activity_user_other")) {
                getDataForSpecificUser();
            } else {
                getData();
            }
        }
        vid_list = v.findViewById(R.id.videos_list);
//        adapter=new VideosAdapter(getActivity(),arrayList,getContext());
//        vid_list.setAdapter(adapter);
        relativeLayout = v.findViewById(R.id.relativeLayout);
        error_tv = v.findViewById(R.id.error_message_tv);
        v.findViewById(R.id.videos_btn_back).setOnClickListener(this);

        return v;

    }

    private void getData() {
        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            arrayList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    Videos_Model videos_model = new Videos_Model();
                                    videos_model.setVideo_id(jsonObject1.getString("id"));
                                    videos_model.setVideo("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_image("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("thumb_img"));
                                    videos_model.setVideo_name(jsonObject1.getString("video_title"));
                                    if (getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || getArguments().getString("from").equalsIgnoreCase("Videos_activity") || getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                                        if (getArguments().getString("from").equalsIgnoreCase("Videos_activity") && jsonObject1.getString("video_type").equalsIgnoreCase("own")) {
                                            arrayList.add(videos_model);
                                        } else {
                                            if (getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") && jsonObject1.getString("video_type").equalsIgnoreCase("community")) {
                                                arrayList.add(videos_model);
                                            } else {
                                                if (!SharedPreference_Main.getInstance(getActivity()).getUserId().equalsIgnoreCase(jsonObject1.getString("user_id")) && jsonObject1.getString("video_type").equalsIgnoreCase("community")) {
                                                    arrayList.add(videos_model);
                                                }
                                            }
                                        }
                                    } else {
                                        arrayList.add(videos_model);
                                    }

                                }
                                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || !getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                                    ((MainActivity) getActivity()).dismissLoader();
                                }
                                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || !getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                                    if (arrayList.size() == 0) {
                                        relativeLayout.setVisibility(View.GONE);
                                        error_tv.setVisibility(View.VISIBLE);
                                        error_tv.setText("No Videos Uploaded");

                                    } else {
                                        if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || !getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                                            adapter = new VideosAdapter(getActivity(), arrayList, getArguments().getString("from"));
                                            vid_list.setAdapter(adapter);
                                        }
                                    }
                                }
                            } else {
                                relativeLayout.setVisibility(View.GONE);
                                error_tv.setVisibility(View.VISIBLE);
                                error_tv.setText("No Videos Uploaded");
                                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || !getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other"))
                                    ((MainActivity) getActivity()).dismissLoader();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            relativeLayout.setVisibility(View.GONE);
                            error_tv.setVisibility(View.VISIBLE);
                            error_tv.setText("Something went wrong...");
                            if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || !getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other"))
                                ((MainActivity) getActivity()).dismissLoader();

                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                            if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || !getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other"))
                                ((MainActivity) getActivity()).dismissLoader();
                        } finally {
                            if (getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community") || getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                                hitApiGetAllData();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community"))
                ((MainActivity) getActivity()).dismissLoader();
                relativeLayout.setVisibility(View.GONE);
                error_tv.setVisibility(View.VISIBLE);
                error_tv.setText("Network Error..., Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "all_video_user");
                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                    map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                }
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.videos_btn_back) {

            ((MainActivity) getActivity()).loadFragment(new VideosStore(), "video_store", view);
        }
    }

    private void getData_perticular_user() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            arrayList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        JSONObject data = jsonObject1.getJSONObject("data");
                                        JSONObject video_data = jsonObject1.getJSONObject("video_data");
                                        JSONObject user_data = jsonObject1.getJSONObject("user_data");
                                        Videos_Model videos_model = new Videos_Model();
                                        videos_model.setVideo_id(video_data.getString("id"));
                                        videos_model.setVideo("http://wehyphens.com/stembuddy/uploads/" + video_data.getString("video"));
                                        videos_model.setVideo_name(video_data.getString("video_title"));
                                        videos_model.setVideo_image("http://wehyphens.com/stembuddy/uploads/" + video_data.getString("thumb_img"));
                                        arrayList.add(videos_model);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } catch (Throwable throwable) {
                                        throwable.printStackTrace();
                                    }

                                }
                                progressDialog.dismiss();

                                adapter = new VideosAdapter(getActivity(), arrayList, getArguments().getString("from"));
                                vid_list.setAdapter(adapter);
                            } else {
                                relativeLayout.setVisibility(View.GONE);
                                error_tv.setVisibility(View.VISIBLE);
                                error_tv.setText("No Videos Uploaded");
                                progressDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            relativeLayout.setVisibility(View.GONE);
                            progressDialog.dismiss();
                            error_tv.setVisibility(View.VISIBLE);
                            error_tv.setText("Something went wrong...");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                relativeLayout.setVisibility(View.GONE);
                error_tv.setVisibility(View.VISIBLE);
                error_tv.setText("Network Error..., Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "get_on_video_for_user_shared");
                if (getArguments().getString("from").equalsIgnoreCase("Specific_user_from")) {
                    map.put("user_id", getArguments().getString("from_id"));
                } else {
                    map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                }
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getData_mock_interview() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            arrayList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("statusstatus").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        Videos_Model videos_model = new Videos_Model();
                                        videos_model.setVideo_id(jsonObject1.getString("id"));
                                        videos_model.setVideo("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("video_name"));
                                        videos_model.setVideo_name(jsonObject1.getString("video_title"));
//                                        videos_model.setVideo_image(retriveVideoFrameFromVideo("http://ritaraapps.com/Stem_Buddy/uploads/" + jsonObject1.getString("video_name")));
                                        arrayList.add(videos_model);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        progressDialog.dismiss();

                                    } catch (Throwable throwable) {
                                        throwable.printStackTrace();
                                        progressDialog.dismiss();
                                    }
                                }
                                progressDialog.dismiss();
                                adapter = new VideosAdapter(getActivity(), arrayList, getArguments().getString("from"));
                                vid_list.setAdapter(adapter);
                            } else {
                                relativeLayout.setVisibility(View.GONE);
                                error_tv.setVisibility(View.VISIBLE);
                                error_tv.setText("No Videos Uploaded");
                                progressDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            relativeLayout.setVisibility(View.GONE);
                            error_tv.setVisibility(View.VISIBLE);
                            error_tv.setText("Something went wrong...");
                            progressDialog.dismiss();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                relativeLayout.setVisibility(View.GONE);
                error_tv.setVisibility(View.VISIBLE);
                error_tv.setText("Network Error..., Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "get_all_mock_interview");
//                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    private void hitApiGetAllData() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("respnse");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    JSONObject jsonObject1 = jsonObject2.getJSONObject("video_details");
                                    Videos_Model videos_model = new Videos_Model();
                                    videos_model.setVideo_id(jsonObject1.getString("id"));
                                    videos_model.setVideo("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_image("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("thumb_img"));
                                    videos_model.setVideo_name(jsonObject1.getString("video_title"));
                                    if (getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                                        if (!jsonObject1.getString("user_id").equalsIgnoreCase(SharedPreference_Main.getInstance(getActivity()).getUserId())) {
                                            arrayList.add(videos_model);
                                        }
                                    } else {
                                        arrayList.add(videos_model);
                                    }

                                }
                                ((MainActivity) getActivity()).dismissLoader();
                                if (arrayList.size() == 0) {
                                    relativeLayout.setVisibility(View.GONE);
                                    error_tv.setVisibility(View.VISIBLE);
                                    error_tv.setText("No Videos Uploaded");

                                } else {
//                                    adapter = new VideosAdapter(getActivity(), arrayList, getArguments().getString("from"));
//                                    vid_list.setAdapter(adapter);
                                }
                            } else {
//                                relativeLayout.setVisibility(View.GONE);
//                                error_tv.setVisibility(View.VISIBLE);
//                                error_tv.setText("No Videos Uploaded");
                                ((MainActivity) getActivity()).dismissLoader();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            relativeLayout.setVisibility(View.GONE);
                            error_tv.setVisibility(View.VISIBLE);
                            error_tv.setText("Something went wrong...");
                            ((MainActivity) getActivity()).dismissLoader();

                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                            ((MainActivity) getActivity()).dismissLoader();
                        } finally {
                            if (arrayList.size() == 0) {
                                relativeLayout.setVisibility(View.GONE);
                                error_tv.setVisibility(View.VISIBLE);
                                error_tv.setText("No Videos Uploaded");

                            } else {
                                relativeLayout.setVisibility(View.VISIBLE);
                                error_tv.setVisibility(View.GONE);
                                adapter = new VideosAdapter(getActivity(), arrayList, getArguments().getString("from"));
                                vid_list.setAdapter(adapter);
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community"))
                ((MainActivity) getActivity()).dismissLoader();
                relativeLayout.setVisibility(View.GONE);
                error_tv.setVisibility(View.VISIBLE);
                error_tv.setText("Network Error..., Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community_other")) {
                    map.put("rule", "get_all_user_shared_video");
                    map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                } else {
                    map.put("rule", "get_all_user_shared_video_for_me");

                }
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void getDataForSpecificUser() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            specificUserVideoArrayList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    try {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        JSONObject data = jsonObject1.getJSONObject("data");
                                        JSONObject video_data = jsonObject1.getJSONObject("video_data");
                                        JSONObject user_data = jsonObject1.getJSONObject("user_data");
                                        JSONObject user_data_to = jsonObject1.getJSONObject("user_data_to");
                                        SpecificUserVideo_Model videos_model = new SpecificUserVideo_Model();
                                        videos_model.setVideo_id(video_data.getString("id"));
                                        videos_model.setVideo_url("http://wehyphens.com/stembuddy/uploads/" + video_data.getString("video"));
                                        videos_model.setVideo_name(video_data.getString("video_title"));
                                        videos_model.setVideo_thumb_image("http://wehyphens.com/stembuddy/uploads/" + video_data.getString("thumb_img"));
                                        videos_model.setSerder_id(user_data.getString("id"));
                                        videos_model.setSender_name(user_data.getString("name"));
                                        if (data.getString("to_id").equalsIgnoreCase(SharedPreference_Main.getInstance(getActivity()).getUserId())) {
                                            specificUserVideoArrayList.add(videos_model);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } catch (Throwable throwable) {
                                        throwable.printStackTrace();
                                    }

                                }
                                progressDialog.dismiss();

                                SpecificUserVideoAdapter adapter = new SpecificUserVideoAdapter(getActivity(), sortList());
                                vid_list.setAdapter(adapter);
                            } else {
                                relativeLayout.setVisibility(View.GONE);
                                error_tv.setVisibility(View.VISIBLE);
                                error_tv.setText("No Videos Uploaded");
                                progressDialog.dismiss();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            relativeLayout.setVisibility(View.GONE);
                            progressDialog.dismiss();
                            error_tv.setVisibility(View.VISIBLE);
                            error_tv.setText("Something went wrong...");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                relativeLayout.setVisibility(View.GONE);
                error_tv.setVisibility(View.VISIBLE);
                error_tv.setText("Network Error..., Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "get_on_video_for_user_shared");
//                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private List<SpecificUserVideo_Model> sortList() {
        Collections.sort(specificUserVideoArrayList, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                SpecificUserVideo_Model p1 = (SpecificUserVideo_Model) o1;
                SpecificUserVideo_Model p2 = (SpecificUserVideo_Model) o2;
                return p1.getSender_name().compareTo(p2.getSender_name());
            }
        });
        return specificUserVideoArrayList;
    }
}