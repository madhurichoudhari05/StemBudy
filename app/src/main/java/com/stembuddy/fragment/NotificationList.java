package com.stembuddy.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.Stem_Buds;
import com.stembuddy.adapter.NotificationListAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.AllVideoNotus;
import com.stembuddy.model.BlockUserModel;
import com.stembuddy.model.CommentOnMentor;
import com.stembuddy.model.CommentOnStudent;
import com.stembuddy.model.CommentOnTopic;
import com.stembuddy.model.CommentOnUser;
import com.stembuddy.model.NotificationListModel;
import com.stembuddy.model.VideoNotus;
import com.stembuddy.retrofit.FileUploadInterface;
import com.stembuddy.retrofit.RetrofitHandler;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CommonUtils;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import retrofit2.Callback;

/**
 * Created by THAKUR on 11/27/2017.
 */

public class NotificationList extends Fragment {
    private ListView listView;
    private ArrayList<BlockUserModel> list;
    public static ArrayList<String> ids_req;
    private ScheduledExecutorService scheduledExecutorService;
    private NotificationListAdapter notificationListAdapter;
    private List<CommentOnMentor> commentOnMentorsList;
    private List<CommentOnStudent> commentOnStudentList;
    private List<CommentOnTopic> commentOnTopicList;
    private List<CommentOnUser> commentOnUserList;
    private List<AllVideoNotus> allVideoNotusList;
    private List<VideoNotus> videoNotusList;
    private List<BlockUserModel> allList;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.notifiaction_list_layout, container, false);
        context=getActivity();

        commentOnMentorsList=new ArrayList<>();
        commentOnStudentList=new ArrayList<>();
        commentOnTopicList=new ArrayList<>();
        commentOnUserList=new ArrayList<>();
        allVideoNotusList=new ArrayList<>();
        commentOnMentorsList=new ArrayList<>();
        allList=new ArrayList<>();


        listView = (ListView) view.findViewById(R.id.noti_list);


        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getArguments().getString("from_activity").equalsIgnoreCase("main_activity")) {
                    ((MainActivity) getActivity()).loadFragment(new MainHome_Fragment(), "Home", view);
                } else {
                    ((MainActivity) getActivity()).loadFragment(new Stem_Buds(), "Stem_Bud", view);
                }
            }
        });
        if (((BaseActivity) getActivity()).isNetworkEnabled(getActivity())) {
            scheduledExecutorService = Executors.newScheduledThreadPool(5);
            scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    first_run();
                    //getAllNotification();


                }
            }, 0, 20, TimeUnit.SECONDS);
        } else
            Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

        return view;
    }



    private void getAllNotification() {

        FileUploadInterface service = RetrofitHandler.getInstance().getApi();


        //retrofit2.Call<CountryCodeResponse> call = service.getChatIntegration(user_id,fireBaseKey);
       // retrofit2.Call<NotificationListModel> call = service.getAllNotiList("get_all_notification_mentor_ios1478",SharedPreference_Main.getInstance(getActivity()).getUserId());
        retrofit2.Call<NotificationListModel> call = service.getAllNotiList("get_all_notification_mentor_ios1478","366");
        call.enqueue(new Callback<NotificationListModel>() {
            @Override
            public void onResponse(retrofit2.Call<NotificationListModel> call, retrofit2.Response<NotificationListModel> response) {


                //CommonUtils.dismissProgress();

                NotificationListModel  notificationListModel = response.body();
                //countryList1.clear();
                // countryCodeList.clear();
                if (notificationListModel != null) {
                    if (notificationListModel.getStatus().equalsIgnoreCase("1")) {


                        for (int i = 0; i < notificationListModel.getVideoNoti().size(); i++) {

                            BlockUserModel blockUserModel=new BlockUserModel();
                            blockUserModel.setName(notificationListModel.getVideoNoti().get(i).getToData().getName());
                            blockUserModel.setImage(notificationListModel.getVideoNoti().get(i).getToData().getImage());
                            blockUserModel.setTime(notificationListModel.getVideoNoti().get(i).getToData().getCreatedDate());
                            blockUserModel.setFor_what("someon share video");
                            allList.add(blockUserModel);

                        }


                        for (int i = 0; i < notificationListModel.getAllVideoNoti().size(); i++) {

                            BlockUserModel blockUserModel=new BlockUserModel();
                            blockUserModel.setName(notificationListModel.getVideoNoti().get(i).getToData().getName());
                            blockUserModel.setImage(notificationListModel.getVideoNoti().get(i).getToData().getImage());
                            blockUserModel.setTime(notificationListModel.getVideoNoti().get(i).getToData().getCreatedDate());
                            blockUserModel.setFor_what("someon share video");
                            allList.add(blockUserModel);

                        }


                        for (int i = 0; i < notificationListModel.getCommentOnUser().size(); i++) {

                            BlockUserModel blockUserModel=new BlockUserModel();
                            blockUserModel.setName(notificationListModel.getVideoNoti().get(i).getToData().getName());
                            blockUserModel.setImage(notificationListModel.getVideoNoti().get(i).getToData().getImage());
                            blockUserModel.setTime(notificationListModel.getVideoNoti().get(i).getToData().getCreatedDate());
                            blockUserModel.setFor_what("someon comment on User");
                            allList.add(blockUserModel);

                        }

                        for (int i = 0; i < notificationListModel.getCommentOnMentor().size(); i++) {

                            BlockUserModel blockUserModel=new BlockUserModel();
                            blockUserModel.setName(notificationListModel.getVideoNoti().get(i).getToData().getName());
                            blockUserModel.setImage(notificationListModel.getVideoNoti().get(i).getToData().getImage());
                            blockUserModel.setTime(notificationListModel.getVideoNoti().get(i).getToData().getCreatedDate());
                            blockUserModel.setFor_what("someon comment on Mentor");
                            allList.add(blockUserModel);

                        }


                        for (int i = 0; i < notificationListModel.getCommentOnStudent().size(); i++) {

                            BlockUserModel blockUserModel=new BlockUserModel();
                            blockUserModel.setName(notificationListModel.getVideoNoti().get(i).getToData().getName());
                            blockUserModel.setImage(notificationListModel.getVideoNoti().get(i).getToData().getImage());
                            blockUserModel.setTime(notificationListModel.getVideoNoti().get(i).getToData().getCreatedDate());
                            blockUserModel.setFor_what("someon comment on Student");
                            allList.add(blockUserModel);

                        }

                        for (int i = 0; i < notificationListModel.getCommentOnTopic().size(); i++) {

                            BlockUserModel blockUserModel=new BlockUserModel();
                            blockUserModel.setName(notificationListModel.getVideoNoti().get(i).getToData().getName());
                            blockUserModel.setImage(notificationListModel.getVideoNoti().get(i).getToData().getImage());
                            blockUserModel.setTime(notificationListModel.getVideoNoti().get(i).getToData().getCreatedDate());
                            blockUserModel.setFor_what("someon share Topic");
                            allList.add(blockUserModel);

                        }

                        Toast.makeText(context, "notiMadhisize"+allList.size(), Toast.LENGTH_SHORT).show();

                        Log.e("notiMadhisize","notiMadhisize"+allList.size());
                    }

                    else {

                        Toast.makeText(context, "not", Toast.LENGTH_SHORT).show();
                    }




                } else {
                    Toast.makeText(context, "not", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<NotificationListModel> call, Throwable t) {                    Toast.makeText(context, "not", Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "exception", Toast.LENGTH_SHORT).show();


            }
        });
    }








    private void first_run() {
//        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            list = new ArrayList<>();
                            ids_req = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("response");
                                    JSONObject jsonObject2;
                                    if (SharedPreference_Main.getInstance(getActivity()).getUserType().equalsIgnoreCase("student")) {
                                        jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("mentor_data");
                                    } else {
                                        jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("student_data");
                                    }
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setIds(jsonObject1.getString("id"));
                                    ids_req.add(jsonObject2.getString("id"));
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("add_time"));
                                    blockUserModel.setFor_what("There is new request send to you");
                                    list.add(blockUserModel);
                                }
                            } else {
                                Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hitApi1();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
//                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equalsIgnoreCase("student")) {
                    map.put("rule", "get_all_notification_student");
                } else {
                    map.put("rule", "get_all_notification_mentor");
                }
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);

    }

    private void hitApi1() {
//        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {

                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("response");
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("user_data");
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setIds(jsonObject1.getString("topic_id"));
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("date_time"));
                                    blockUserModel.setFor_what("There is new comment on topic");
                                    list.add(blockUserModel);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hitApi2();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
//                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "stem_dis_tpc_comment_noti");
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void hitApi2() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("data");
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("from_notifi");
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setIds(jsonObject1.getString("from_id"));
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("date_time"));
                                    blockUserModel.setFor_what("There is new comment on your profile");
                                    list.add(blockUserModel);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hitApi3();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
//                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "stem_comment_nofication");
                map.put("to_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void hitApi3() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        ((MainActivity) getActivity()).dismissLoader();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("message");
                                for (int i = jsonArray.length() - 1; i >= 0; i--) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i).getJSONObject("data");
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i).getJSONObject("from_notifi");
                                    BlockUserModel blockUserModel = new BlockUserModel();
                                    blockUserModel.setImage(jsonObject2.getString("image"));
                                    blockUserModel.setName(jsonObject2.getString("name"));
                                    blockUserModel.setTime(jsonObject1.getString("date_time"));
                                    blockUserModel.setFor_what("There is new video share");
                                    blockUserModel.setIds(jsonObject1.getString("video_id"));
                                    list.add(blockUserModel);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            List lst = new ArrayList();
                            for (int i = 0; i < list.size(); i++) {
                                lst.add(list.get(i).getTime());
                            }

                            Collections.sort(lst, new Comparator<String>() {
                                @Override
                                public int compare(String o1, String o2) {
                                    try {
                                        return new SimpleDateFormat("hh:mm a").parse(o1).compareTo(new SimpleDateFormat("hh:mm a").parse(o2));
                                    } catch (ParseException e) {
                                        return 0;
                                    }
                                }
                            });

                            ArrayList<BlockUserModel> userArrayList = new ArrayList<>();
                            for (int i = 0; i < lst.size(); i++) {
                                if (lst.get(0).equals(list.get(i).getTime())) {
                                    userArrayList.add(list.get(i));
                                    lst.remove(0);
                                    i = 0;
                                }
                            }


//                            postAdapter.notifyDataSetChanged();
//                if (recent_listview.getAdapter() == null)
//                    recent_listview.setAdapter(postAdapter);
//                else {
//                    Toast.makeText(context, "List Updated", Toast.LENGTH_SHORT).show();
//                    int p = postAdapter.getcurrent_Position();
//                    home_posts = db_home_posts.get_All_Posts("HOME", DB_Posts.TABLE_POSTS_HOME);
//
//                    postAdapter = new PostAdapter(getActivity(), home_posts, "HOME");
//                    recent_listview.setAdapter(postAdapter);
//                    setListViewHeightBasedOnItems(recent_listview);
//                    recent_listview.smoothScrollToPosition(p);
//                    if (progressDialog != null)
//                        progressDialog.dismiss();

                            if (userArrayList.size() != 0) {
                                notificationListAdapter = new NotificationListAdapter(userArrayList, getActivity());
                                listView.setAdapter(notificationListAdapter);
                            } else {
                                Toast.makeText(getActivity(), "No notification yet", Toast.LENGTH_SHORT).show();

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                ((MainActivity) getActivity()).dismissLoader();
                Toast.makeText(getActivity(), "Something went worng... Please try again later!", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "stem_send_video_noti");
                map.put("shareto", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onPause() {
        super.onPause();
        scheduledExecutorService.shutdown();
    }
}
