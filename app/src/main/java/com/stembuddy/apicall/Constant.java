package com.stembuddy.apicall;

public class Constant {

    public interface Url {
        String BASE_URL = "http://www.contribcity.com/webservice";
        String Upload_Url = "http://www.contribcity.com/assets/uploads/resume/";
        String logo_url = "http://www.contribcity.com/uploads/company_logo_employer/";
    }

    public interface Params {
        int REQUEST_UPLOAD_FILE_TYPE = 1000;
        int REQUEST_POST_UPLOAD_FILE_TYPE = 1001;

        String KEY_RULE = "register";
        String KEY_RULE_LOGIN = "login";
        String KEY_USER_EMAIL = "email";
        String KEY_USERNAME = "name";
        String Key_PROFILE = "file";
        String KEY_ID = "id";
        String MESSAGE_TYPE_TEXT = "text";
        String MESSAGE_TYPE_IMAGE = "image";
        String MESSAGE_TYPE_VIDEO = "video";
        String MESSAGE_TYPE_LOCATION = "location";
        String MESSAGE_TYPE_CONTACT = "contact";
        String MESSAGE_TYPE_FILE = "file";
        String MESSAGE_TYPE_GROUP_STATUS = "status";

        String KEY_PRICE = "price";


        String KEY_PHOTO = "photo";
    }

}
