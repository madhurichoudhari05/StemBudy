package com.stembuddy.model;

/**
 * Created by THAKUR on 11/27/2017.
 */

public class BlockUserModel {
    private String ids;
    private String image;
    private String name;
    private String time;
    private String for_what;

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFor_what() {
        return for_what;
    }

    public void setFor_what(String for_what) {
        this.for_what = for_what;
    }
}
