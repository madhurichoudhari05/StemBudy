package com.stembuddy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response_ {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("shareto")
    @Expose
    private String shareto;
    @SerializedName("video_id")
    @Expose
    private String videoId;
    @SerializedName("date_time")
    @Expose
    private String dateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getShareto() {
        return shareto;
    }

    public void setShareto(String shareto) {
        this.shareto = shareto;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
