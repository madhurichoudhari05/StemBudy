package com.stembuddy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.stembuddy.R;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class StudentPageActivity1 extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_page1_layout);
        findViewById(R.id.stu1_add_btn).setOnClickListener(this);
        findViewById(R.id.stu1_back_btn).setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.stu1_add_btn:
                startActivity(new Intent(this, StemFindBuddy.class));

                break;
            case R.id.stu1_back_btn:

                break;
        }

    }
}

