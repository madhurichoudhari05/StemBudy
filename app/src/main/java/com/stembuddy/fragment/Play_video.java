package com.stembuddy.fragment;


import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.MediaController;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.VidioesActivity;
import com.stembuddy.apicall.AppController;
import com.stembuddy.utils.Constants;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mahak on 12/19/2017.
 */

public class Play_video extends Fragment {

    private View v;
    private WebView webView;
    private MediaController mediaController;
    private MediaPlayer mMediaPlayer;

    ProgressDialog progressDialog;
    private String VIDEO_PATH;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.play_video, container, false);

        webView = (WebView) v.findViewById(R.id.video_play);

        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webView.getSettings().setDisplayZoomControls(true);
        webView.setWebChromeClient(new WebChromeClient());
        String vedioPath = getArguments().getString("video");
        Log.v("VedioUrl", vedioPath);
        // videoView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(vedioPath);

        // videoView.loadUrl("https://www.youtube.com");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading video...");
        // progressDialog.show();

        v.findViewById(R.id.videos_btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getArguments().getString("from").equalsIgnoreCase("Save_videos")) {
                    VideosFragment_Activity vidioesActivity = new VideosFragment_Activity();
                    Bundle bundle = new Bundle();
                    bundle.putString("from", "Videos_activity");
                    vidioesActivity.setArguments(bundle);
                    ((MainActivity) getActivity()).loadFragment(vidioesActivity, "Videos_activity", view);
                } else {
                    VidioesActivity vidioesActivity2 = new VidioesActivity();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("from", getArguments().getString("from"));
                    vidioesActivity2.setArguments(bundle2);
                    ((MainActivity) getActivity()).loadFragment(vidioesActivity2, "Videos_activity", view);
                }
            }
        });


        //hitIncreaseViews();
        try {
            // mediaController = new MediaController(getActivity());
            // mediaController.setAnchorView(videoView);

            VIDEO_PATH = getArguments().getString("video");
            //  playVedio(vedioPath);

        } catch (Exception e) {
            ((BaseActivity) getActivity()).showToast(e.getMessage());
        }

        /*// videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                try {
                    // mp.setLooping(true);
                     videoView.start();
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/

        return v;
    }

    private void playVedio(String vedioPath) {


        String path = "android.resource://" + getActivity().getPackageName() + "/" + vedioPath;
        MediaController mc = new MediaController(getActivity());
      /*  videoView.setMediaController(mc);
        videoView.setVideoPath(vedioPath);
        videoView.start();*/


        // Uri video = Uri.parse(vedioPath);
        // videoView.setVideoURI(video);
        // videoView.setMediaController(mediaController);

        /*videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {

                //videoView.start();
             //   videoView.start();
            }
        });*/


// videoView.setVideoPath(getArguments().getString("video"));


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        releaseMediaPlayer();
    }

    @Override
    public void onPause() {
        super.onPause();

        releaseMediaPlayer();
    }

    private void releaseMediaPlayer() {

        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void hitIncreaseViews() {
//        ((MainActivity)getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        ((MainActivity)getActivity()).dismissLoader();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "get_mock_interview");
                map.put("mock_id", getArguments().getString("video_id"));
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }


}
