package com.stembuddy.model;

/**
 * Created by mahak on 12/28/2017.
 */

public class Review_List_Model {

    String Photo;

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCommented_date() {
        return commented_date;
    }

    public void setCommented_date(String commented_date) {
        this.commented_date = commented_date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    String Name;
    String commented_date;
    String comment;
    String rating;
}
