package com.stembuddy.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.model.TopDiscModel;
import com.stembuddy.utils.CircleTransform;

import java.util.ArrayList;

/**
 * Created by THAKUR on 11/30/2017.
 */

public class TopDiscAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<TopDiscModel> data;
    private static LayoutInflater inflater = null;
    Context context;

    public TopDiscAdapter(Activity activity, ArrayList<TopDiscModel> data, Context context) {
        this.activity = activity;
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }


    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        public ImageView img;
        public TextView description;
        public TextView post;
        public TextView views;
        public TextView title;


    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View vi = view;
        if (vi == null) {
            /********** Inflate tabitem.xml file for each row ( Defined below ) ************/
            vi = inflater.inflate(R.layout.top_disccussion_list_row, null);

            /******** View Holder Object to contain tabitem.xml file elements ************/
            holder = new ViewHolder();
            holder.description = (TextView) vi.findViewById(R.id.description);
            holder.post = (TextView) vi.findViewById(R.id.top_disc_post);
            holder.views = (TextView) vi.findViewById(R.id.top_disc_view);
            holder.img = (ImageView) vi.findViewById(R.id.top_disc_profile);
            holder.title = (TextView) vi.findViewById(R.id.title);


            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        Glide.with(context)
                .load(data.get(position).getProfile())
                .transform(new CircleTransform(context))
                .into(holder.img);

        holder.title.setText(data.get(position).getTitle());
        holder.post.setText(data.get(position).getPost());
        holder.views.setText(data.get(position).getViews());
        holder.description.setText(Html.fromHtml(data.get(position).getDescription()));
        return vi;

    }
}
