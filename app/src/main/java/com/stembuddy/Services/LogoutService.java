package com.stembuddy.Services;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Himanshu on 1/31/2018.
 */

public class LogoutService extends Service {

    @SuppressLint({"LongLogTag", "WrongConstant"})
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("lkdfhlkhdfkrdffeef", "khgdfhgdfhgsdhkfghukfgtwee");
        hitLogoutApi();
        return START_STICKY;
    }

    @SuppressLint("LongLogTag")
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e("lkdfhlkhdfkrdffeefer", "khgdfhgdfhgsdhkfghukfgtwee");
        if (!intent.getExtras().getString("service_stop").equalsIgnoreCase("stop")) {
            hitLogoutApi();
        }
        return null;
    }

    private void hitLogoutApi() {
        String url = Constants.BASE_URL;
//        showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
//                    dismissLoader();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("1")) {
                    } else {
                        Log.e("lkdfhlk", "khgdfhgdfhgsdhkfghukfgtwee");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("lkdfhg", "khgdfhgdfhgsdhkfghukfgtwee");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("lkdfhlkhd", "khgdfhgdfhgsdhkfghukfgtwee");
                error.getMessage();
//                dismissLoader();
//                logout();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "logout");
                params.put("user_id", SharedPreference_Main.getInstance(LogoutService.this).getUserId());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "logout");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);

        Log.d("gkjgjhghfhkfd", "TASK REMOVED");
        Intent intent = new Intent(LogoutService.this, LogoutService.class);
        intent.putExtra("service_stop", "Unstop");
        PendingIntent service = PendingIntent.getService(
                getApplicationContext(),
                1001,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 1000, service);
    }
}
