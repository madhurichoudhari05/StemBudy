package com.stembuddy.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.adapter.SavedVideosCommonAdapter;
import com.stembuddy.adapter.VideosFragment_Activity_Adapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.VideosFragment_Activity_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SavedVediosFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SavedVediosFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SavedVediosFragment extends Fragment {

    public SavedVediosFragment() {
        // Required empty public constructor
    }

    RecyclerView videos_list_recycler, specific_user_recycler, sb_community_recycler;
    private ArrayList<VideosFragment_Activity_Model> owned_list;
    private ArrayList<VideosFragment_Activity_Model> community_list;
    private ArrayList<VideosFragment_Activity_Model> specificUserList;

    private View view;

    // TODO: Rename and change types and number of parameters
    public static SavedVediosFragment newInstance(String param1, String param2) {
        SavedVediosFragment fragment = new SavedVediosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_saved_vedios, container, false);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        videos_list_recycler = getView().findViewById(R.id.videos_list_recycler);
        specific_user_recycler = getView().findViewById(R.id.specific_user_recycler);
        sb_community_recycler = getView().findViewById(R.id.sb_community_recycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        videos_list_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        specific_user_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        sb_community_recycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        videos_list_recycler.setNestedScrollingEnabled(false);
        specific_user_recycler.setNestedScrollingEnabled(false);
        sb_community_recycler.setNestedScrollingEnabled(false);

        videos_list_recycler.setHasFixedSize(true);
        specific_user_recycler.setHasFixedSize(true);
        sb_community_recycler.setHasFixedSize(true);

        owned_list = new ArrayList<>();
        community_list = new ArrayList<>();
        specificUserList = new ArrayList<>();


        getDataSaved_Videos(view);


    }

    private void getDataSaved_Videos(final View view) {

        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            owned_list = new ArrayList<>();
                            ((MainActivity) getActivity()).dismissLoader();

//                            community_list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = 0; i < jsonArray.length()/*(jsonArray.length() < 2 ? jsonArray.length() : 2)*/; i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    VideosFragment_Activity_Model videos_model = new VideosFragment_Activity_Model();
                                    videos_model.setVideo_Id(jsonObject1.getString("id"));
                                    //videos_model.setVideo_Url("http://ritaraapps.com/Stem_Buddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_Url("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_thumb_Image("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("thumb_img"));
                                    videos_model.setVideo_Name(jsonObject1.getString("video_title"));
                                    videos_model.setVideo_To("own");
                                    if (jsonObject1.getString("video_type").equalsIgnoreCase("own")) {
                                        owned_list.add(videos_model);
                                    }
                                }
                                if (owned_list.size() > 0) {
                                    SavedVideosCommonAdapter adapter = new SavedVideosCommonAdapter(getActivity(), owned_list);
                                    videos_list_recycler.setAdapter(adapter);
                                } else {
                                    SavedVediosFragment.this.view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                                    SavedVediosFragment.this.view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                                }
                            } else {
                                SavedVediosFragment.this.view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                                SavedVediosFragment.this.view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            SavedVediosFragment.this.view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                            SavedVediosFragment.this.view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                            TextView textView = SavedVediosFragment.this.view.findViewById(R.id.error_message_saved_tv);
                            textView.setText("Opps! Someting Went Wrong");
                            e.printStackTrace();

                        } finally {
                            getDataCommunityList();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community"))
                SavedVediosFragment.this.view.findViewById(R.id.videos_list_layout).setVisibility(View.GONE);
                SavedVediosFragment.this.view.findViewById(R.id.error_message_saved_tv).setVisibility(View.VISIBLE);
                TextView textView = SavedVediosFragment.this.view.findViewById(R.id.error_message_saved_tv);
                textView.setText("Opps! Someting Went Wrong. Please Check Your Internet Connection");
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "all_video_user");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


    private void getDataCommunityList() {
        ((MainActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((MainActivity) getActivity()).dismissLoader();

//                            owned_list = new ArrayList<>();
                            community_list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("response");
                                for (int i = 0/*(jsonArray.length() < 2 ? jsonArray.length() : 2)*/; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    VideosFragment_Activity_Model videos_model = new VideosFragment_Activity_Model();
                                    videos_model.setVideo_Id(jsonObject1.getString("id"));
                                    videos_model.setVideo_Url("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("video"));
                                    videos_model.setVideo_thumb_Image("http://wehyphens.com/stembuddy/uploads/" + jsonObject1.getString("thumb_img"));
                                    videos_model.setVideo_Name(jsonObject1.getString("video_title"));
                                    videos_model.setVideo_To("community");
                                    if (!jsonObject1.getString("video_type").equalsIgnoreCase("own")) {
                                        community_list.add(videos_model);
                                    }
                                }

                                SavedVideosCommonAdapter adapter = new SavedVideosCommonAdapter(getActivity(), community_list);
                                sb_community_recycler.setAdapter(adapter);
                            } else {

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            //getDataCommunityList1();


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
//                if (!getArguments().getString("from").equalsIgnoreCase("Videos_activity_sb_community"))
                ((MainActivity) getActivity()).dismissLoader();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "all_video_user");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
