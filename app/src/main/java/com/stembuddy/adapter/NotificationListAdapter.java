package com.stembuddy.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.TopDiscussion2;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.All_comments_On_Profile;
import com.stembuddy.model.BlockUserModel;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.stembuddy.fragment.NotificationList.ids_req;

/**
 * Created by THAKUR on 11/29/2017.
 */

public class NotificationListAdapter extends BaseAdapter {
    private ArrayList<BlockUserModel> data;
    private static LayoutInflater inflater = null;
    Context context;

    public NotificationListAdapter(ArrayList<BlockUserModel> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


//    public static class ViewHolder{
//
//        public ImageView img;
//        public TextView name;
//        public TextView comment_share, accept_req, reject_req;
//        public TextView duration;
//
//
//    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
//        ViewHolder holder;
        View vi = null;
//        if (vi == null) {
        /********** Inflate tabitem.xml file for each row ( Defined below ) ************/
//            holder = new ViewHolder();
//        if (data.get(position).getFor_what().equalsIgnoreCase("There is new request send to you")) {
//            vi = LayoutInflater.from(context).inflate(R.layout.layout_notification_list, viewGroup, false);
//
//        } else {
            vi = LayoutInflater.from(context).inflate(R.layout.notification_list_row, viewGroup, false);
//        }

        /******** View Holder Object to contain tabitem.xml file elements ************/
        TextView name = (TextView) vi.findViewById(R.id.noti_list_name);
        TextView duration = (TextView) vi.findViewById(R.id.noti_list_duration);
        ImageView img = (ImageView) vi.findViewById(R.id.noti_list_profile);
        TextView comment_share = vi.findViewById(R.id.noti_list_comment_share);
//        TextView accept_req = vi.findViewById(R.id.accept_req);
//        TextView reject_req = vi.findViewById(R.id.reject_req);
        comment_share.setSelected(true);
        /************  Set holder with LayoutInflater ************/
//            vi.setTag(holder);
//        } else
//        {
//            holder=(ViewHolder)view.getTag();
//        }
        name.setText(data.get(position).getName());
        duration.setText(data.get(position).getTime());
        comment_share.setText(data.get(position).getFor_what());
        Glide.with(context)
                .load(data.get(position).getImage())
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.profile)
                .into(img);
//        accept_req.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hitApiAcceptReject("1", position);
//            }
//        });
//
//        reject_req.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hitApiAcceptReject("3", position);
//            }
//        });
        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (data.get(position).getFor_what().equalsIgnoreCase("There is new comment on topic")) {
                    TopDiscussion2 topDiscussion2 = new TopDiscussion2();
                    Bundle bundle = new Bundle();
                    bundle.putString("posts_id", data.get(position).getIds());
                    bundle.putString("from_activity", "notification");
                    topDiscussion2.setArguments(bundle);
                    ((MainActivity) context).loadFragment(topDiscussion2, "Top_discussion", view);
                } else {
                    if (data.get(position).getFor_what().equalsIgnoreCase("There is new comment on your profile")) {
                        All_comments_On_Profile all_comments_on_profile = new All_comments_On_Profile();
                        Bundle bundle = new Bundle();
                        bundle.putString("comments_ids", data.get(position).getIds());
                        all_comments_on_profile.setArguments(bundle);
                        ((MainActivity) context).loadFragment(all_comments_on_profile, "Comment_on_profile", view);
                    }
                    else if (data.get(position).getFor_what().equalsIgnoreCase("There is new request send to you")){
                        open_dialog(position);
                    }
                }
            }
        });
//        }
        return vi;
    }

    private void open_dialog(final int position){
        final Dialog dialog =new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog_accept_reject);
        ImageView dialog_image=dialog.findViewById(R.id.dialog_img);
        TextView dialog_text=dialog.findViewById(R.id.dialog_text);
        dialog.show();
        Glide.with(context)
                .load(data.get(position).getImage())
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.profile)
                .into(dialog_image);
        dialog_text.setText(data.get(position).getName()+" send you a new friend request");
        dialog.findViewById(R.id.dialog_reject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hitApiAcceptReject("3", position);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.dialog_accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hitApiAcceptReject("1", position);
                dialog.dismiss();
            }
        });
    }

    private void hitApiAcceptReject(final String status, final int position) {
        ((BaseActivity) context).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) context).dismissLoader();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                if (status.equalsIgnoreCase("1"))
                                    Toast.makeText(context, "Successfully Added to Buddy List", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(context, "Successfully Rejected Your Request", Toast.LENGTH_SHORT).show();
                                data.remove(position);
                                ids_req.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Something Went Wrong... Please Try Again", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ((BaseActivity) context).dismissLoader();
                error.printStackTrace();
                Toast.makeText(context, "Something Went Wrong... Please Try Again", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "accept_reject_fave");
                map.put("user_type", SharedPreference_Main.getInstance(context).getUserType());
                map.put("request_id", data.get(position).getIds()); //data id
                map.put("req_per_id", ids_req.get(position));  //mentor or student data id
                map.put("status", status);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}
