package com.stembuddy.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.stembuddy.Database.DB_Chat;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.ModelScan;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Config;
import com.stembuddy.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChatActivity extends BaseActivity implements View.OnClickListener {

    private ImageView imgBack, imgSend, imgProImg;
    private TextView textName;
    private ListView lstChat;
    private EditText etChat;
    private Bundle bundle;
    private String name = "", destination = "", reciepient_id = "", start_loc = "", imgUrl = "", drive_date = "", drive_time = "", mobile_no = "";
    private SharedPreference_Main sharedPreference_main;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ChatAdapter chatAdapter;
    private ArrayList<ModelScan> chatData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new__chat);

        initView();

//        findViewById(R.id.imgProImg).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Dialog dialog = new Dialog(ChatActivity.this);
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.dialog_profile_pic);
//
//                ((TextView) dialog.findViewById(R.id.textName)).setText(textName.getText().toString());
//
//                try {
//                    Picasso.with(ChatActivity.this).load(imgUrl)
//                            .transform(new CircleTransform())
//                            .into((ImageView) dialog.findViewById(R.id.goProDialogImage));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                dialog.show();
//            }
//        });

    }

    private void initView() {
        sharedPreference_main = SharedPreference_Main.getInstance(ChatActivity.this);

        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgProImg = (ImageView) findViewById(R.id.imgProImg);
        imgSend = (ImageView) findViewById(R.id.imgSend);
        textName = (TextView) findViewById(R.id.textName);
        etChat = (EditText) findViewById(R.id.etChat);
        lstChat = (ListView) findViewById(R.id.lstChat);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            try {
                if (bundle.getString("senders_id") == null) {// from notification to ChatActivity.java

                    name = bundle.getString("KEY_FULLNAME");
                    reciepient_id = bundle.getString("KEY_USER_ID");

                } else {          //from Chat.java to ChatActivity.java
                    name = bundle.getString("senders_name");
                    reciepient_id = bundle.getString("senders_id");
                }

//                String response = (String) bundle.get("message");
//                JSONObject jsonObject = new JSONObject(response);
//                JSONObject details_jsonOnj = jsonObject.getJSONObject("details");
//                name = details_jsonOnj.getString("KEY_FULLNAME");
//                reciepient_id = details_jsonOnj.getString("id");
//                mobile_no = details_jsonOnj.getString("mobile");
//                imgUrl = details_jsonOnj.getString("KEY_PROFILE_IMAGE");

//                name = bundle.getString("KEY_FULLNAME");
//                reciepient_id = bundle.getString("KEY_USER_ID");
//                start_loc = bundle.getString("start_loc");
//                destination = bundle.getString("destination");
//                imgUrl = bundle.getString("KEY_PROFILE_IMAGE");
//                drive_date = bundle.getString("drive_date");
//                drive_time = bundle.getString("drive_time");

            } catch (Exception e) {
                e.printStackTrace();
//                name = bundle.getString("KEY_FULLNAME");
//                reciepient_id = bundle.getString("KEY_USER_ID");
//                start_loc = bundle.getString("start_loc");
//                destination = bundle.getString("destination");
//                imgUrl = bundle.getString("KEY_PROFILE_IMAGE");
//                drive_date = bundle.getString("drive_date");
//                drive_time = bundle.getString("drive_time");
            }
        }
        textName.setText(name);

        DB_Chat db_chat = DB_Chat.getInstance(ChatActivity.this);
        chatData = db_chat.get_Chats_By_SenderAndReceiverID(reciepient_id);

        chatAdapter = new ChatAdapter(ChatActivity.this, chatData);
        lstChat.setAdapter(chatAdapter);
        lstChat.setSelection(chatData.size() - 1);

        imgBack.setOnClickListener(this);
        imgSend.setOnClickListener(this);

        try {
            Glide.with(ChatActivity.this).load(imgUrl)
                    .transform(new CircleTransform(ChatActivity.this))
                    .into(imgProImg);
        } catch (Exception e) {
            e.printStackTrace();
        }

        CheckFor_CHAT_message(null);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {       //if app is in foreground then ,it will show dialog by broadcast
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    CheckFor_CHAT_message(intent);
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
    }

    private void CheckFor_CHAT_message(Intent intent) {
        Bundle bundle = null;
        if (intent != null) {
            bundle = intent.getExtras();                //intent come from broadcast forground intent
        } else {
            bundle = getIntent().getExtras();             //intent come background notification click
        }

        if (bundle.get("Chat_msg") != null) {
            String message = bundle.getString("Chat_msg");
            reciepient_id = bundle.getString("senders_id");//the other persons id with which we will search his chats

//            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
            if (intent != null) {//means msg from broadcast when app in foreground else msg from notification
                DB_Chat db_chat = DB_Chat.getInstance(ChatActivity.this);
                ModelScan newmodelScan_hopon = db_chat.getLastrowChat();     //getting last row values of db and notifying adapter
                chatData.add(newmodelScan_hopon);
            }
            chatAdapter.notifyDataSetChanged();
            lstChat.setSelection(lstChat.getAdapter().getCount() - 1);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.imgBack:
                finish();
                break;

            case R.id.imgSend:
                if (isNetworkEnabled(ChatActivity.this)) {
                    if (etChat.getText().toString().trim().length() > 0)
                        send_msg_api();
                } else {
//                    showMessage(ChatActivity.this, "No Internet Available");
                    Toast.makeText(this, "No Internet Available", Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                break;
        }
    }

    private void send_msg_api() {
//        final ProgressDialog pDialog = new ProgressDialog(ChatActivity.this);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Sending Message...");
//        pDialog.show();
        imgSend.setOnClickListener(null);
        StringRequest sr = new StringRequest(com.android.volley.Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    imgSend.setOnClickListener(ChatActivity.this);

//                    pDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
//                        showMessage(ChatActivity.this, "Message sent");

//                        Toast.makeText(ChatActivity.this, "Message sent", Toast.LENGTH_SHORT).show();
                        DB_Chat db_chat = DB_Chat.getInstance(ChatActivity.this);
                        ModelScan modelScan = new ModelScan();
                        modelScan.setRecievers_Id(reciepient_id);
                        modelScan.setSenders_Id(sharedPreference_main.getUserId());
                        modelScan.setMsg_senders_name(name);
                        modelScan.setMsg_senders_image(imgUrl);
                        modelScan.setMessage(etChat.getText().toString());
//                        modelScan.setMobile_chat(sharedPreference_main.getMobile_no());
                        db_chat.insertDB_ChatDetails(modelScan);
                        chatData.add(modelScan);
                        chatAdapter.notifyDataSetChanged();
                        lstChat.setSelection(lstChat.getAdapter().getCount() - 1);
                        etChat.setText("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.wtf("TAG", "Error: " + error.getMessage());
                imgSend.setOnClickListener(ChatActivity.this);
                Toast.makeText(ChatActivity.this, "Network Error..., Try Again", Toast.LENGTH_SHORT).show();
//                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("rule", "get_stem_chatting_api");
                params.put("to", "" + reciepient_id);
                params.put("from", "" + sharedPreference_main.getUserId());
                params.put("msg", "" + etChat.getText().toString());
                params.put("regId", sharedPreference_main.getFireBaseTokenid());

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(sr);
    }

    private class ChatAdapter extends BaseAdapter {
        private Context context;
        private ArrayList<ModelScan> chatData;

        public ChatAdapter(Context context, ArrayList<ModelScan> chatData) {
            this.context = context;
            this.chatData = chatData;
        }

        @Override
        public int getCount() {
            return chatData.size();
        }

        @Override
        public Object getItem(int i) {
            return chatData.get(i).getMessage();
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View viewForOtherMsg = LayoutInflater.from(context).inflate(R.layout.message, viewGroup, false);
            View viewForMyMsg = LayoutInflater.from(context).inflate(R.layout.message2, viewGroup, false);

            if (chatData.get(i).getSenders_Id().equalsIgnoreCase(sharedPreference_main.getUserId())) {
                long timestampString;
                try {
                    timestampString = Long.parseLong(chatData.get(i).getDateTime_ofMsgRecieveed()) * 1000;
                } catch (Exception ee) {
                    timestampString = (System.currentTimeMillis() / 1000) * 1000;
                }
                String value = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").
                        format(new java.util.Date(timestampString));

                ((TextView) viewForMyMsg.findViewById(R.id.message2)).setText(chatData.get(i).getMessage() + "\n" + value);
//                chatData .get(i).getDateTime_ofMsgRecieveed()
                return viewForMyMsg;
            } else {
                long timestampString = Long.parseLong(chatData.get(i).getDateTime_ofMsgRecieveed()) * 1000;
                String value = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").
                        format(new java.util.Date(timestampString));
//
                ((TextView) viewForOtherMsg.findViewById(R.id.message1)).setText(chatData.get(i).getMessage() + "\n" + value);
//                chatData.get(i).getDateTime_ofMsgRecieveed()

                return viewForOtherMsg;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        (AppController.getInstance()).activityChatResumed(reciepient_id);
    }

    @Override
    protected void onPause() {
        super.onPause();
        (AppController.getInstance()).activityChatPaused();
    }
}
