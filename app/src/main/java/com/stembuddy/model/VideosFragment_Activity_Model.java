package com.stembuddy.model;

/**
 * Created by Himanshu on 1/19/2018.
 */

public class VideosFragment_Activity_Model {
    private String video_Id, video_Url, video_thumb_Image, video_Name, video_To;

    public String getVideo_Id() {
        return video_Id;
    }

    public void setVideo_Id(String video_Id) {
        this.video_Id = video_Id;
    }

    public String getVideo_Url() {
        return video_Url;
    }

    public void setVideo_Url(String video_Url) {
        this.video_Url = video_Url;
    }

    public String getVideo_thumb_Image() {
        return video_thumb_Image;
    }

    public void setVideo_thumb_Image(String video_thumb_Image) {
        this.video_thumb_Image = video_thumb_Image;
    }

    public String getVideo_Name() {
        return video_Name;
    }

    public void setVideo_Name(String video_Name) {
        this.video_Name = video_Name;
    }

    public String getVideo_To() {
        return video_To;
    }

    public void setVideo_To(String video_To) {
        this.video_To = video_To;
    }
}
