package com.stembuddy.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.fragment.Play_video;
import com.stembuddy.model.VideosFragment_Activity_Model;

import java.util.ArrayList;

public class SavedVideosCommonAdapter extends RecyclerView.Adapter {


    private Context context;
    ArrayList<VideosFragment_Activity_Model> list2;

    public SavedVideosCommonAdapter(Context context, ArrayList<VideosFragment_Activity_Model> list2) {

        this.context = context;
        this.list2 = list2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_view_video_other, null, false);

        CommonViewHolder commonViewHolder = new CommonViewHolder(view);
        return commonViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {


        CommonViewHolder commonViewHolder = (CommonViewHolder) holder;
        commonViewHolder.textView.setText(list2.get(position).getVideo_Name());
        try {
            Glide.with(context)
                    .load(list2.get(position).getVideo_thumb_Image())
                    .into(commonViewHolder.video_upload);
        } catch (Exception e) {
            e.printStackTrace();
        }

        commonViewHolder.play_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Play_video play_video = new Play_video();
                Bundle b = new Bundle();
                b.putString("video", list2.get(position).getVideo_Url());
                b.putString("from", "Save_videos");
                play_video.setArguments(b);
                ((MainActivity) context).loadFragment(play_video, "Play_Video", view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list2.size();
    }

    private class CommonViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView video_upload;
        ImageView play_icon;


        public CommonViewHolder(View itemView) {
            super(itemView);

             textView = itemView.findViewById(R.id.text_view);
             video_upload = itemView.findViewById(R.id.video_upload);
             play_icon = itemView.findViewById(R.id.play_icon);

        }
    }

}
