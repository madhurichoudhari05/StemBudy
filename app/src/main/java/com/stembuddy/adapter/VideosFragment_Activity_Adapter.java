package com.stembuddy.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.fragment.Play_video;
import com.stembuddy.model.VideosFragment_Activity_Model;

import java.util.ArrayList;

/**
 * Created by Himanshu on 1/19/2018.
 */

public class VideosFragment_Activity_Adapter extends BaseAdapter {
    private ArrayList<VideosFragment_Activity_Model> list = new ArrayList<>();
    private Context context;

    public VideosFragment_Activity_Adapter(Context context, ArrayList<VideosFragment_Activity_Model> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(context).inflate(R.layout.layout_view_video_other, viewGroup, false);
        TextView textView = v.findViewById(R.id.text_view);
        ImageView video_upload = v.findViewById(R.id.video_upload);
        ImageView play_icon = v.findViewById(R.id.play_icon);

        textView.setText(list.get(i).getVideo_Name());
        try {
            Glide.with(context)
                    .load(list.get(i).getVideo_thumb_Image())
                    .into(video_upload);
        } catch (Exception e) {
            e.printStackTrace();
        }

        play_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Play_video play_video = new Play_video();
                Bundle b = new Bundle();
                b.putString("video", list.get(i).getVideo_Url());
                b.putString("from", "Save_videos");
                play_video.setArguments(b);
                ((MainActivity) context).loadFragment(play_video, "Play_Video", view);
            }
        });
        return v;
    }
}