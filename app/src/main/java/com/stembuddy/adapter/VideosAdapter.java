package com.stembuddy.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.VideosStore;
import com.stembuddy.activities.VidioesActivity;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.BlockUserList;
import com.stembuddy.fragment.Play_video;
import com.stembuddy.model.Videos_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/29/2017.
 */

public class VideosAdapter extends BaseAdapter {
    private ArrayList<Videos_Model> data;
    private LayoutInflater layoutInflater;
    Context context;
    private String from;

    public VideosAdapter(Context context, ArrayList<Videos_Model> data, String from) {
        this.data = data;
        this.context = context;
        this.from = from;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
//
//    public static class ViewHolder {
//
////        public VideoView videoView;
//        public TextView name;
//        public ImageView video_upload , play_icon;
//
//
//    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View v = null;
        if (!from.equalsIgnoreCase("Videos_activity")) {
            v = layoutInflater.inflate(R.layout.layout_view_video_other, viewGroup, false);
        } else {
            v = layoutInflater.inflate(R.layout.video_list_row, viewGroup, false);
            FrameLayout item_delete_layout = v.findViewById(R.id.items_delete_layout);
            if (!from.equalsIgnoreCase("Videos_activity")) {
                item_delete_layout.setVisibility(View.GONE);
            }
            item_delete_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (from.equalsIgnoreCase("Videos_activity"))
                        hitApiToDeleteVideo(position, view);
                }
            });
        }


        if (data.size() <= 0) {
            ((TextView) v.findViewById(R.id.text_view)).setText("No data Avilable");
        } else

        {
            ((TextView) v.findViewById(R.id.text_view)).setText(data.get(position).getVideo_name());

            try {
                ImageView video_upload = ((ImageView) v.findViewById(R.id.video_upload));
                Glide.with(context)
                        .load(data.get(position).getVideo_image())
                        .into(video_upload);


                Log.e("video","videopath"+data.get(position).getVideo_image());
                Log.e("video","videopath"+data.get(position).getVideo());
//                VideoView videoView =(VideoView) v.findViewById(R.id.video_upload);
//                videoView.setVideoURI(Uri.parse(data.get(position).getVideo()));
//                videoView.seekTo(0);
            } catch (Exception e) {
                e.printStackTrace();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }


            v.findViewById(R.id.play_icon).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (from.equalsIgnoreCase("Videos_activity") || from.equalsIgnoreCase("Videos_activity_user") || from.equalsIgnoreCase("Videos_activity_sb_community")||from.equalsIgnoreCase("Specific_user_from")) {
                        Play_video play_video = new Play_video();
                        Bundle b = new Bundle();
                        b.putString("from", from);
                        b.putString("video", data.get(position).getVideo());
                        play_video.setArguments(b);
                        ((MainActivity) context).loadFragment(play_video, "Play_Video", view);
                    } else if (from.equalsIgnoreCase("mock_interview")) {
                        Play_video play_video = new Play_video();
                        Bundle b = new Bundle();
                        b.putString("from", from);
                        b.putString("video", data.get(position).getVideo());
                        b.putString("video_id", data.get(position).getVideo_id());
                        play_video.setArguments(b);
                        ((MainActivity) context).loadFragment(play_video, "Play_Video", view);
                    }else if (from.equalsIgnoreCase("Videos_activity_sb_community_other"))
                    {
                        Play_video play_video = new Play_video();
                        Bundle b = new Bundle();
                        b.putString("from", from);
                        b.putString("video", data.get(position).getVideo());
                        b.putString("video_id", data.get(position).getVideo_id());
                        play_video.setArguments(b);
                        ((MainActivity) context).loadFragment(play_video, "Video_Share_Community", view);
                    }
                }
            });

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (from.equalsIgnoreCase("Videos_Share")) {
                        BlockUserList blockUserList = new BlockUserList();
                        Bundle bundle = new Bundle();
                        bundle.putString("from_activity", "share_video");
                        bundle.putString("video_id", data.get(position).getVideo_id());
                        blockUserList.setArguments(bundle);
                        ((MainActivity) context).loadFragment(blockUserList, "Block_user_list", view);
                    } else if (from.equalsIgnoreCase("Videos_Share_Community")) {
//                        VideoShareCommunity shareCommunity = new VideoShareCommunity();
//                        Bundle bundle = new Bundle();
//                        bundle.putString("video_title", data.get(position).getVideo_name());
//                        bundle.putString("video_id", data.get(position).getVideo_name());
//                        bundle.putString("video_url", data.get(position).getVideo());
//                        shareCommunity.setArguments(bundle);
//                        ((MainActivity) context).loadFragment(shareCommunity, "Video_Share_Community", view);
                        hitApiForShare_Video(data.get(position).getVideo_id(), view);

                    }
                }
            });
        }

        return v;

    }

    public class LoadVideoThumbnail extends AsyncTask<String, Object, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... objectURL) {
            return ThumbnailUtils.extractThumbnail(ThumbnailUtils.createVideoThumbnail(objectURL[0], MediaStore.Video.Thumbnails.MINI_KIND), 100, 100);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            //img.setImageBitmap(result);
        }

    }

    private void hitApiToDeleteVideo(final int position, final View view) {
        ((BaseActivity) context).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) context).dismissLoader();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                VidioesActivity vidioesActivity = new VidioesActivity();
                                Bundle bundle = new Bundle();
                                bundle.putString("from", "Videos_activity");
                                vidioesActivity.setArguments(bundle);
                                ((MainActivity) context).loadFragment(vidioesActivity, "Videos_activity", view);
                                Toast.makeText(context, "Video Successfully Deleted", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Video Not Deleted Successfully", Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Opps! Something Went Wrong! Please Try Again Later", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                ((BaseActivity) context).dismissLoader();
                Toast.makeText(context, "Opps! Something Went Wrong! Please Check Your Internet Connections", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "delete_video_user");
                map.put("video_id", data.get(position).getVideo_id());
                map.put("user_id", SharedPreference_Main.getInstance(context).getUserId());
                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void hitApiForShare_Video(final String video_id, final View view) {
        ((BaseActivity) context).showLoader();
        RequestQueue requestQueue= Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) context).dismissLoader();
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("flag").equalsIgnoreCase("1")) {
//                                VidioesActivity vidioesActivity = new VidioesActivity();
//                                Bundle bundle = new Bundle();
//                                bundle.putString("from", "Videos_activity");
//                                vidioesActivity.setArguments(bundle);
                                ((MainActivity) context).loadFragment(new VideosStore(), "video_store", view);
                                Toast.makeText(context, "Video Successfully Share", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Video Not Share Successfully", Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Opps! Something Went Wrong! Please Try Again Later", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                ((BaseActivity) context).dismissLoader();
                Toast.makeText(context, "Opps! Something Went Wrong! Please Check Your Internet Connections", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "send_video_to_all");
                map.put("video_id", video_id);
                map.put("user_id", SharedPreference_Main.getInstance(context).getUserId());
                return map;
            }
        };
        int socketTimeout = 30000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);
        requestQueue.add(stringRequest);
    }
}

