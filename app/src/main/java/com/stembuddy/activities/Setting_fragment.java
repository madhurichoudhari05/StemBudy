package com.stembuddy.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stembuddy.R;
import com.stembuddy.fragment.Change_password;


public class Setting_fragment extends Fragment implements View.OnClickListener{

    private  View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    v = inflater.inflate(R.layout.setting_layout, container, false);



        v.findViewById(R.id.change_password).setOnClickListener(this);
        v.findViewById(R.id.edit_profile).setOnClickListener(this);



        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case  R.id.change_password:
                ((MainActivity)getActivity()).loadFragment(new Change_password(), "change_password", view);

                break;
            case  R.id.edit_profile:
                ((MainActivity)getActivity()).loadFragment(new Edit_profile(), "edit_profile", view);
                break;
        }
    }
    }
