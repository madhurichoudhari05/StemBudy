package com.stembuddy.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.apicall.FileUploadModel;
import com.stembuddy.apicall.UpdateMultipartListener;
import com.stembuddy.apicall.VolleyMultipartRequest;
import com.stembuddy.apicall.onUpdateViewListener;
import com.stembuddy.fragment.FeatureHome_Fragment;
import com.stembuddy.model.ImageViewModel;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by THAKUR on 12/7/2017.
 */

public class UploadInformation extends Fragment implements View.OnClickListener {
    private ImageView img_upload,iVedit;
    private EditText mobile, profession, speciality, discription, affiliation,upload_name;
    private Button btn_submit;
    private Bitmap filephoto;
    private static final int request_camera = 1001;
    private static final int PICK_IMAGE_REQUEST = 1;
    private File photo;
    private String user_email, u_User_Type;
    private EditText uName, uPass, uMobile, uGender, uAddress;
    private TextView uEmail;
    private Uri selected_image;
    private Uri uriFilePath;
    private int columnindex;
    private String file_path = "";

    private SharedPreference_Main sharedPreference_main;
    private View v;

    private Context context;
    private String image="";


    private String[] permissions = new String[]{
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA,
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.upload_info_layout, container, false);
        context=getActivity();

        sharedPreference_main = SharedPreference_Main.getInstance(getActivity());

        img_upload = v.findViewById(R.id.upload_img);
        iVedit = v.findViewById(R.id.iVedit);
        mobile = v.findViewById(R.id.upload_mobile);
        upload_name = v.findViewById(R.id.upload_name);
        profession = v.findViewById(R.id.upload_professtion);
        speciality = v.findViewById(R.id.upload_speciality);
        affiliation = v.findViewById(R.id.upload_affiliation);
        discription = v.findViewById(R.id.upload_desc);
        img_upload.setOnClickListener(this);
        btn_submit = v.findViewById(R.id.upload_submit);
        btn_submit.setOnClickListener(this);

        iVedit.setOnClickListener(this);


        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new FeatureHome_Fragment(), "feature_home", view);
            }
        });



        getMyInfo();
        return v;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.upload_img:
                if (checkPermissions()) {
                    openDialog();
                } else{
                    Toast.makeText(getActivity(), "Please Allow Permissions", Toast.LENGTH_SHORT).show();
                    }
            break;

            case R.id.iVedit:
                if (checkPermissions()) {
                    openDialog();
                } else{
                    Toast.makeText(getActivity(), "Please Allow Permissions", Toast.LENGTH_SHORT).show();
                }
                break;

            case  R.id.upload_submit:
            {
                String mobile_no = mobile.getText().toString().trim();
                String name = upload_name.getText().toString().trim();
                String profess = profession.getText().toString();
                String specility = speciality.getText().toString();
                String desc = discription.getText().toString();
                String affiliation_st = affiliation.getText().toString();
                if (checkValidity(mobile_no, profess, specility, affiliation_st, desc)) {
                    if (((BaseActivity)getActivity()).isNetworkEnabled(getActivity()))
                        uploadImageTextApi(name,mobile_no, profess, specility, affiliation_st, desc);

                    else
                        Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

                }

            }
            break;

        }





       /* if (view.getId() == R.id.upload_img) {
            if (checkPermissions()) {
                openDialog();
            } else
                Toast.makeText(getActivity(), "Please Allow Permissions", Toast.LENGTH_SHORT).show();

        } else if (view.getId() == R.id.upload_submit) {

            String mobile_no = mobile.getText().toString().trim();
            String profess = profession.getText().toString();
            String specility = speciality.getText().toString();
            String desc = discription.getText().toString();
            String affiliation_st = affiliation.getText().toString();
            if (checkValidity(mobile_no, profess, specility, affiliation_st, desc)) {
                if (((BaseActivity)getActivity()).isNetworkEnabled(getActivity()))
                uploadImageTextApi(mobile_no, profess, specility, affiliation_st, desc);

                else Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();

            }
//            uploadTextApi();
        }*/

    }


    private void uploadImageTextApi(String name, String mobile_no, String profess, String speciltiy, String affiliation_st, String desc) {
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.show();
        pd.setMessage("Uploading....");
// Volley MultipartRequest==========================================
        VolleyMultipartRequest request = new VolleyMultipartRequest(Constants.BASE_URL, new UpdateMultipartListener(getActivity(), new onUpdateViewListener() {
            @Override
            public void updateView(Object responseObject, boolean isSuccess, int reqType) {
                try {
                    pd.dismiss();

                    Log.e("uploadImage",String.valueOf(responseObject));
                    JSONObject jsonObject = new JSONObject(String.valueOf(responseObject));
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Toast.makeText(getActivity(), "Profile Successfully Updated!", Toast.LENGTH_LONG).show();

                        JSONObject jsonObject1 = jsonObject.getJSONObject("response");
                        sharedPreference_main.setuserMobile(jsonObject1.getString("mobile"));
                        sharedPreference_main.setUserProfile(jsonObject1.getString("image"));
                        sharedPreference_main.setUserName(jsonObject1.getString("name"));

                        String image=jsonObject1.getString("image");
                        Log.v("UplaodProfileImage:::",image);


                        ImageViewModel model=new ImageViewModel();
                     //   model.getmCurrentImage().postValue(sharedPreference_main.getUserProfile());
                        model.getmCurrentImage().postValue(jsonObject1.getString("image"));



                        Intent intent = new Intent("GetImage");
                        intent.putExtra("UploadImage",sharedPreference_main.getUserProfile() );
                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        ((MainActivity) getActivity()).setProfile_Name();
                        }


                        else {
                        Toast.makeText(getActivity(), "Opps! Something went wrong. Please try again...", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, 1), FileUploadModel.class, "file", photo, getParams(name,mobile_no, profess, speciltiy, affiliation_st, desc));




        int socketTime = 20000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTime,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
//        requestQueue.add(request);
        AppController.getInstance().addToRequestQueue(request);

    }

    private HashMap<String, String> getParams( String name,String mobile_no, String profess, String speciltiy, String affiliation_st, String desc) {
        HashMap<String, String> params = new HashMap<>();
        params.put("rule", "profile");
        params.put("mobile", mobile_no);
        params.put("name", name);
        params.put("profession", profess);
        params.put("specility", speciltiy);
        params.put("affiliation", affiliation_st);
        params.put("description", desc);
        params.put("user_id", sharedPreference_main.getUserId());
        return params;
    }

    private boolean checkValidity(final String mobile_no, String profess, String specility, String affiliation_st, String desc) {
        if (TextUtils.isEmpty(mobile_no)) {
            mobile.setError("Enter Mobile");
            return false;
        } else if (TextUtils.isEmpty(profess)) {
            profession.setError("Enter Profession");
            return false;
        } else if (TextUtils.isEmpty(specility)) {
            speciality.setError("Enter Speciality");
            return false;
        } else if (TextUtils.isEmpty(affiliation_st.trim())) {
            affiliation.setError("Enter Affiliation");
            return false;
        } else {
            return true;
        }

    }


    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.uplaod_image);
        final TextView gallary = dialog.findViewById(R.id.gallary);
        TextView camera = dialog.findViewById(R.id.camera);
        dialog.show();

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
                dialog.dismiss();
            }
        });

        gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChosser();
                dialog.dismiss();
            }
        });

    }

    private void openCamera() {
        PackageManager packageManager = getActivity().getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            File mainDirectory = new File(Environment.getExternalStorageDirectory(), "MyFolder/tmp");
            if (!mainDirectory.exists())
                mainDirectory.mkdirs();
            Calendar calendar = Calendar.getInstance();
            uriFilePath = Uri.fromFile(new File(mainDirectory, "IMG" + calendar.getTimeInMillis() + ".jpg"));
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriFilePath);
            startActivityForResult(intent, request_camera);
        }
    }

    private void showFileChosser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a File Here !"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            try {
                selected_image = data.getData();
                file_path = getRealPathFromURI(getActivity(), selected_image);
                photo = new File(new URI("file://" + file_path.replace(" ", "%20")));
                Glide.with(getActivity())
                        .load(photo)
                        .transform(new CircleTransform(getActivity()))
                        .into(img_upload);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == request_camera) {
            try {

                ExifInterface exif = new ExifInterface(uriFilePath.getPath());
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL);
                Matrix matrix = new Matrix();

                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.postRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.postRotate(270);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                Uri uri = getImageUri(getActivity(), bitmap);
                String path = getRealPathFromURI(getActivity(), uri);
                /*Glide.with(this).load(uri).asBitmap().centerCrop().into(new BitmapImageViewTarget(img_upload) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(UploadInformation.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        img_upload.setImageDrawable(circularBitmapDrawable);
                    }
                });
                */

//            Glide.with(this).load(uriFilePath).placeholder(R.mipmap.profile_pic).into(img_profile);
//                Glide.with(this).load(uriFilePath).placeholder(R.mipmap.prfile_bg).into(img_back);

                photo = new File(new URI("file://" + path.replace(" ", "%20")));
//                img_upload.setImageBitmap(bitmap);
                Glide.with(getActivity())
                        .load(photo)
                        .transform(new CircleTransform(getActivity()))
                        .placeholder(R.drawable.profile)
                        .into(img_upload);
//                Glide.with(getActivity()).load(photo).into(img_upload);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);

            return path;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void getMyInfo() {
        ((BaseActivity) getActivity()).showLoader();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            ((BaseActivity) getActivity()).dismissLoader();

                            Log.e("EditProfile",response);

                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                JSONArray jsonArray1 = jsonObject.getJSONArray("response");
                                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                                Glide.with(getActivity())
                                        .load(jsonObject1.getString("image"))
                                        .transform(new CircleTransform(getActivity()))
                                        .placeholder(R.drawable.profile)
                                        .into(img_upload);
                                mobile.setText(jsonObject1.getString("mobile"));
                                upload_name.setText(jsonObject1.getString("name"));
                                profession.setText(jsonObject1.getString("profession"));
                                speciality.setText(jsonObject1.getString("specility"));
                                affiliation.setText(jsonObject1.getString("affiliation"));
                                discription.setText(jsonObject1.getString("description"));
                                }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                ((BaseActivity) getActivity()).dismissLoader();
                Toast.makeText(getActivity(), "Something went wrong. Please cheeck yor internet connection", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("rule", "user_details");
                map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getActivity(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }
}
