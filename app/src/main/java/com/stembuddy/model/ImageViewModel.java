package com.stembuddy.model;


import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

public class ImageViewModel extends ViewModel {

    private MutableLiveData<String> mCurrentImage;

    public MutableLiveData<String> getmCurrentImage() {
        if (mCurrentImage == null) {
            mCurrentImage = new MutableLiveData<String>();
        }
        return mCurrentImage;
    }
}
