package com.stembuddy.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.BaseActivity;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.apicall.AppController;
import com.stembuddy.fragment.BlockUserList;
import com.stembuddy.model.Notification_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by THAKUR on 11/27/2017.
 */

public class BlockUserAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<Notification_Model> data;
    Context context;
    private String from_activity;
    private View vi;
    private ArrayList<String> block_ids;

    public BlockUserAdapter(Activity activity, ArrayList<Notification_Model> data, Context context, String from_activity, ArrayList<String> block_ids) {
        this.activity = activity;
        this.data = data;
        this.context = context;
        this.from_activity=from_activity;
        this.block_ids=block_ids;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {

        ImageView img;
        TextView name;
        TextView profession;
        Button block_user;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        vi = view;
        if (vi == null) {
            /********** Inflate tabitem.xml file for each row ( Defined below ) ************/
            vi = LayoutInflater.from(context).inflate(R.layout.block_user_list_row, viewGroup, false);

            /******** View Holder Object to contain tabitem.xml file elements ************/
            holder = new ViewHolder();
            holder.name = (TextView) vi.findViewById(R.id.noti_list_name);
            holder.profession = (TextView) vi.findViewById(R.id.noti_list_profession);
            holder.img = (ImageView) vi.findViewById(R.id.noti_list_profile);
            holder.block_user=vi.findViewById(R.id.block_user);
            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Glide.with(context)
                .load(data.get(position).getPhoto())
                .transform(new CircleTransform(context))
                .placeholder(R.mipmap.profile_dmmy)
                .into(holder.img);

        holder.name.setText(data.get(position).getName());

        holder.profession.setText(data.get(position).getProfession());
        if (from_activity.equalsIgnoreCase("Block_user_list")||from_activity.equalsIgnoreCase("Block_user_list_after_block")){
            holder.block_user.setVisibility(View.VISIBLE);
            if(data.get(position).getBlock_user().equals("1")){
                holder.block_user.setText("Block");
            }else{
                holder.block_user.setText("UnBlock");
            }
        }
        else {
            holder.block_user.setVisibility(View.GONE);
        }

        vi.findViewById(R.id.block_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.block_user.getText().toString().equals("Block")) {
                    alertDialogToBlockAndUnBlock("Are you sure want to block this profile .", position , "block" , holder.block_user);
                }else if(holder.block_user.getText().toString().equals("UnBlock")){
                    alertDialogToBlockAndUnBlock("Are you sure want to unblock this profile .", position , "unblock" , holder.block_user);
                }
            }
        });
        return vi;
    }

    private void alertDialogToBlockAndUnBlock(String msg, final int position, final String s, final Button block_user){
        AlertDialog.Builder ab = new AlertDialog.Builder(context);
        ProgressDialog progressDialog;

        ab.setMessage(msg);
        ab.setCancelable(false);
        ab.setPositiveButton("YES ", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(s.equals("block")) {
                    hitApiBlockUser(position , "0" , block_user);
                }else if(s.equals("unblock")){
                    hitApiBlockUser(position , "1" , block_user);
                }

            }
        });
        ab.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        ab.show();
    }

    private void hitApiBlockUser(final int position, final String s, final Button block_user) {
        ((BaseActivity)context).showLoader();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            progressDialog.dismiss();
//                            ((BaseActivity)context).dismissLoader();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")){
                                if(s.equals("0")) {
                                    Toast.makeText(context, "Successfully block", Toast.LENGTH_SHORT).show();
                                }else if(s.equals("1")){
                                    Toast.makeText(context, "Successfully unblock", Toast.LENGTH_SHORT).show();
                                }
                                BlockUserList blockUserList=new BlockUserList();
                                Bundle bundle = new Bundle();
                                bundle.putString("from_activity","Block_user_list_after_block");
                                blockUserList.setArguments(bundle);
                                ((MainActivity) context).loadFragment(blockUserList, "Block_user_list", vi);
//                                ((BAse))
                            }else {
//                                Toast.makeText(context, ""+jsonObject.getString("Result"), Toast.LENGTH_SHORT).show();
                                hitApiBlockUser147(position, s, block_user);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(activity, "Opps! Something Went Wrong. Please Try Again Later.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                ((BaseActivity)context).dismissLoader();
                Toast.makeText(activity, "Opps! Something Went Wrong. Please Connect Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                SharedPreference_Main sharedPreference_main = SharedPreference_Main.getInstance(context);
                if (sharedPreference_main.getUserType().equalsIgnoreCase("student")) {
                    map.put("rule", "added_blok_by_student");
                    map.put("student_id", sharedPreference_main.getUserId());
                    map.put("mentor_id", data.get(position).getId());
                    map.put("status" , s);
                } else {
                    map.put("rule", "added_blok_by_mentor");
                    map.put("student_id", data.get(position).getId());
                    map.put("mentor_id", sharedPreference_main.getUserId());
                    map.put("status" , s);
                }
//                map.put("rule", "accept_reject_fave");
//                map.put("user_type", SharedPreference_Main.getInstance(context).getUserType());
//                map.put("request_id", block_ids.get(position));
//                map.put("req_per_id", data.get(position).getId());
                    //                map.put("status", s);
                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    private void hitApiBlockUser147(final int position, final String s, final Button block_user) {
//        ((BaseActivity)context).showLoader();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            progressDialog.dismiss();
//                            ((BaseActivity)context).dismissLoader();
                            JSONArray jsonArray = new JSONArray(response);
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")){
                                if(s.equals("0")) {
                                    Toast.makeText(context, "Successfully block", Toast.LENGTH_SHORT).show();
                                }else if(s.equals("1")){
                                    Toast.makeText(context, "Successfully unblock", Toast.LENGTH_SHORT).show();
                                }
                                BlockUserList blockUserList=new BlockUserList();
                                Bundle bundle = new Bundle();
                                bundle.putString("from_activity","Block_user_list_after_block");
                                blockUserList.setArguments(bundle);
                                ((MainActivity) context).loadFragment(blockUserList, "Block_user_list", vi);
                            }else {
                                Toast.makeText(context, ""+jsonObject.getString("Result"), Toast.LENGTH_SHORT).show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(activity, "Opps! Something Went Wrong. Please Try Again Later.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                ((BaseActivity)context).dismissLoader();
                Toast.makeText(activity, "Opps! Something Went Wrong. Please Connect Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                SharedPreference_Main sharedPreference_main = SharedPreference_Main.getInstance(context);
                if (sharedPreference_main.getUserType().equalsIgnoreCase("student")) {
                    map.put("rule", "added_blok_by_student147");
                    map.put("student_id", sharedPreference_main.getUserId());
                    map.put("mentor_id", data.get(position).getId());
                    map.put("status" , s);
                } else {
                    map.put("rule", "added_blok_by_mentor147");
                    map.put("student_id", data.get(position).getId());
                    map.put("mentor_id", sharedPreference_main.getUserId());
                    map.put("status" , s);
                }
//                map.put("rule", "accept_reject_fave");
//                map.put("user_type", SharedPreference_Main.getInstance(context).getUserType());
//                map.put("request_id", block_ids.get(position));
//                map.put("req_per_id", data.get(position).getId());
                //                map.put("status", s);
                return map;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest);
    }
}

