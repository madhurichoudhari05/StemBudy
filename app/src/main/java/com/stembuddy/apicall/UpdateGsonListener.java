package com.stembuddy.apicall;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;

public class UpdateGsonListener<T> implements Listener<T>, ErrorListener {

	private int						reqType;
	private onUpdateViewListener	onUpdateViewListener;

	public UpdateGsonListener(onUpdateViewListener onUpdateView, int reqType) {
		this.reqType = reqType;
		this.onUpdateViewListener = onUpdateView;
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		onUpdateViewListener.updateView(VolleyExceptionUtil.getErrorMessage(error), false, reqType);
	}

	@Override
	public void onResponse(final Object responseModel) {
		try {
			onUpdateViewListener.updateView(responseModel, true, reqType);
		} catch (Exception ex) {
			ex.printStackTrace();
			onUpdateViewListener.updateView(responseModel, false, reqType);
		}

	}

}
