package com.stembuddy.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.model.Comments_topic_Model;
import com.stembuddy.utils.CircleTransform;

import java.util.ArrayList;

/**
 * Created by Himanshu on 12/29/2017.
 */

public class Comment_List_Adapter extends BaseAdapter {

    private ArrayList<Comments_topic_Model> comments_topic_modelArrayList;
    private android.support.v4.app.Fragment fragment;
    private Context context;
    private String recent_all;
    private int i5;

    public Comment_List_Adapter(Context context, ArrayList<Comments_topic_Model> comments_topic_modelArrayList, android.support.v4.app.Fragment fragment, String recent_all) {
        this.context = context;
        this.comments_topic_modelArrayList = comments_topic_modelArrayList;
        this.fragment = fragment;
        this.recent_all = recent_all;
        this.i5 = 1;
    }

    @Override
    public int getCount() {
        return comments_topic_modelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return comments_topic_modelArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.layout_topic_comment_cell, viewGroup, false);
        ImageView imageView = view1.findViewById(R.id.topic_comment_image);
        TextView textView = view1.findViewById(R.id.topic_comment_name);
        TextView textView1 = view1.findViewById(R.id.topic_comment);
        TextView textView2 = view1.findViewById(R.id.topic_comment_date);
        Glide.with(context)
                .load(comments_topic_modelArrayList.get(i).getImage())
                .transform(new CircleTransform(context))
                .placeholder(R.drawable.profile)
                .into(imageView);

        textView.setText(comments_topic_modelArrayList.get(i).getName()+" ("+comments_topic_modelArrayList.get(i).getUser_type()+")");
        textView1.setText(Html.fromHtml(comments_topic_modelArrayList.get(i).getComment()));
        textView2.setText(comments_topic_modelArrayList.get(i).getDate());
        return view1;
    }
}
