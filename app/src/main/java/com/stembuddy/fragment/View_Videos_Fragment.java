package com.stembuddy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.VideosStore;
import com.stembuddy.activities.VidioesActivity;

/**
 * Created by Himanshu on 1/4/2018.
 */

public class View_Videos_Fragment extends Fragment implements View.OnClickListener{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.layout_view_videos, container, false);
        view.findViewById(R.id.saved_videos_button).setOnClickListener(this);
        view.findViewById(R.id.share_videos_button).setOnClickListener(this);
        view.findViewById(R.id.share_videos_sbc_button).setOnClickListener(this);
        view.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new VideosStore(), "video_store", view);
            }
        });
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.saved_videos_button:
                //VideosFragment_Activity vidioesActivity = new VideosFragment_Activity();
                SavedVediosFragment savedVediosFragment=new SavedVediosFragment();
                Bundle bundle = new Bundle();
                bundle.putString("from", "Videos_activity");
                savedVediosFragment.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(savedVediosFragment, "Videos_activity", view);
                break;
            case R.id.share_videos_button:
                VidioesActivity vidioesActivity2 = new VidioesActivity();
                Bundle bundle2 = new Bundle();
                bundle2.putString("from", "Videos_activity_user_other");
                vidioesActivity2.setArguments(bundle2);
                ((MainActivity) getActivity()).loadFragment(vidioesActivity2, "Videos_activity_user", view);
                break;
            case R.id.share_videos_sbc_button:
                VidioesActivity vidioesActivity3 = new VidioesActivity();
                Bundle bundle3 = new Bundle();
                bundle3.putString("from", "Videos_activity_sb_community_other");
                vidioesActivity3.setArguments(bundle3);
                ((MainActivity) getActivity()).loadFragment(vidioesActivity3, "Videos_activity_sb_community", view);
                break;
        }
    }
}
