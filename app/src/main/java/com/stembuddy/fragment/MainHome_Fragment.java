package com.stembuddy.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.StemFindBuddy;
import com.stembuddy.activities.Stem_Buds;
import com.stembuddy.activities.TopDiscussionList;
import com.stembuddy.activities.VideosStore;


/**
 * Created by THAKUR on 11/27/2017.
 */

public class MainHome_Fragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_home_fragment, container, false);

        view.findViewById(R.id.main_buddy_search).setOnClickListener(this);
        view.findViewById(R.id.main_chat).setOnClickListener(this);
        view.findViewById(R.id.main_stem_bud).setOnClickListener(this);
        view.findViewById(R.id.main_stem_feature).setOnClickListener(this);
        view.findViewById(R.id.main_stem_videos).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_buddy_search:
                ((MainActivity) getActivity()).loadFragment(new StemFindBuddy(), "Stem_find_buddy", view);

                // startActivity(new Intent(getContext(), StemFindBuddy.class));
                break;
            case R.id.main_chat:
                ((MainActivity) getActivity()).loadFragment(new TopDiscussionList(), "Top_discussion", view);
                //   startActivity(new Intent(getContext(), TopDiscussionList.class));
                break;
            case R.id.main_stem_bud:
                ((MainActivity) getActivity()).loadFragment(new Stem_Buds(), "Stem_Bud", view);
                //startActivity(new Intent(getContext(), Stem_Buds.class));
                break;
            case R.id.main_stem_feature:
                ((MainActivity) getActivity()).loadFragment(new FeatureHome_Fragment(), "feature_home", view);
                break;
            case R.id.main_stem_videos:
                ((MainActivity) getActivity()).loadFragment(new VideosStore(), "video_store", view);

                break;
        }

    }
//
}
