package com.stembuddy.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.stembuddy.R;
import com.stembuddy.adapter.StemResultGridAdapter;
import com.stembuddy.apicall.AppController;
import com.stembuddy.model.Notification_Model;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class StemBuddyFindResult extends Fragment implements View.OnClickListener {


    private GridView gridView;
    private View v;
    private ArrayList<Notification_Model> arrayList;
    private ArrayList<String> block_list;
    private StemResultGridAdapter adapter;
    private boolean showLoder = true;
    private ScheduledExecutorService scheduledExecutorService;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.stem_buddy_find_result, container, false);

        gridView = v.findViewById(R.id.stem_result_gridview);

        block_list = new ArrayList<>();

        try {
            block_list = getArguments().getStringArrayList("block_user_list");
        } catch (Exception e) {
            e.printStackTrace();
        }

        hitySearchAPi();

        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
               // hitySearchAPi();
                showLoder = false;
            }
        }, 0, 5, TimeUnit.SECONDS);

        v.findViewById(R.id.result_btn_back).setOnClickListener(this);
        return v;

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.result_btn_back) {
            ((MainActivity) getActivity()).loadFragment(new StemFindBuddy(), "Stem_find_buddy", view);
        }
    }


    private void hitySearchAPi() {
        String url = Constants.BASE_URL;
        if (showLoder) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((BaseActivity) getActivity()).showLoader();
                }
            });
        }
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((BaseActivity) getActivity()).dismissLoader();
                        }
                    });
                    arrayList = new ArrayList<>();
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        JSONArray array = jsonO.getJSONArray("response");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonObject = array.getJSONObject(i);
                            JSONObject object = jsonObject.getJSONObject("response");

                            Notification_Model model = new Notification_Model();
                            model.setAffiliation(object.getString("affiliation"));
                            model.setId(object.getString("id"));
                            model.setBlock_user(object.getString("block"));
                            model.setDescription(object.getString("description"));
                            model.setName(object.getString("name"));
                            model.setComment(object.getString("comment"));
                            model.setEmail(object.getString("email"));
                            model.setUser_type(object.getString("user_type"));
                            model.setSpeciality(object.getString("specility"));
                            model.setRate(object.getString("rating_bar"));
                            model.setMobile(object.getString("mobile"));
                            model.setOnline_offline(object.getString("status"));
                            model.setProfession(object.getString("profession"));
                            model.setPhoto(object.getString("image"));
                            model.setUser_name(object.getString("user_name"));

//                            boolean st = false;
//                            for(int j=0 ; j<block_list.size() ; j++) {
//                                if(object.getString("id").equals(block_list.get(j))) {
//                                    st = true;
//                                }
//                            }
//                            if(!st) {
                            arrayList.add(model);
//                            }
                        }
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int a = 0;
                                if (adapter == null) {
                                    adapter = new StemResultGridAdapter(getActivity(), arrayList, getArguments().getString("name_value"));
                                } else {
                                    a = adapter.getPosition();
                                    adapter = new StemResultGridAdapter(getActivity(), arrayList, getArguments().getString("name_value"));
                                }
                                gridView.setAdapter(adapter);
                                gridView.smoothScrollToPosition(a);
                            }
                        });
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                gridView.setVisibility(View.GONE);
                                v.findViewById(R.id.text_no_result_found).setVisibility(View.VISIBLE);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((BaseActivity) getActivity()).dismissLoader();
                                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
                                ((TextView) v.findViewById(R.id.text_no_result_found)).setText("Network error... Please try again");

                                gridView.setVisibility(View.GONE);
                                v.findViewById(R.id.text_no_result_found).setVisibility(View.VISIBLE);
                            }
                        });

                    }
                });

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("rule", "user_search_name");
                params.put("name", getArguments().getString("name_value"));
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("type", "mentor");
                } else {
                    params.put("type", "student");
                }

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "serach");
    }

    @Override
    public void onPause() {
        super.onPause();
        scheduledExecutorService.shutdown();
    }
}
