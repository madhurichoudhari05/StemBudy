package com.stembuddy.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.activities.MainActivity;
import com.stembuddy.activities.VidioesActivity;
import com.stembuddy.fragment.Play_video;
import com.stembuddy.model.SpecificUserVideo_Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Himanshu on 1/22/2018.
 */

public class SpecificUserVideoAdapter extends BaseAdapter {
    private Context context;
    private List<SpecificUserVideo_Model> specificUserVideoArrayList;
    private int count = 0;
    private ArrayList<String> count1=new ArrayList<>();

    public SpecificUserVideoAdapter(Context context, List<SpecificUserVideo_Model> specificUserVideoArrayList) {
        this.context = context;
        this.specificUserVideoArrayList = specificUserVideoArrayList;
    }

    @Override
    public int getCount() {
        return specificUserVideoArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return specificUserVideoArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.layout_specific_user, viewGroup, false);
        TextView client_name = view1.findViewById(R.id.client_name);
        TextView video_title = view1.findViewById(R.id.video_title);
        TextView saved_videos_more = view1.findViewById(R.id.saved_videos_more);
        ImageView video_upload = view1.findViewById(R.id.video_upload);
        ImageView play_icon = view1.findViewById(R.id.play_icon);
        LinearLayout video_upload_layout = view1.findViewById(R.id.video_upload_layout);
        saved_videos_more.setVisibility(View.GONE);
        if (i == 0) {
            client_name.setVisibility(View.VISIBLE);
        } else {
                String mNegative = specificUserVideoArrayList.get(i - 1).getSender_name();
                String mPositive = specificUserVideoArrayList.get(i).getSender_name();
                if (mNegative.equalsIgnoreCase(mPositive)) {
                    client_name.setVisibility(View.GONE);
//                    count=count+1;
//                    count1.add(specificUserVideoArrayList.get(i).getSender_name());
                } else {
                    client_name.setVisibility(View.VISIBLE);
//                    count=0;
//                    count1=new ArrayList<>();
                }
        }
//        if (count<2){
//            video_upload_layout.setVisibility(View.VISIBLE);
//            video_title.setVisibility(View.VISIBLE);
//            saved_videos_more.setVisibility(View.GONE);
//        }
//        else {
//            if (count>2){
//                video_upload_layout.setVisibility(View.GONE);
//                video_title.setVisibility(View.GONE);
//                saved_videos_more.setVisibility(View.GONE);
//            }
//            else {
//                video_upload_layout.setVisibility(View.GONE);
//                video_title.setVisibility(View.GONE);
//                saved_videos_more.setVisibility(View.VISIBLE);
//            }
//            video_upload_layout.setVisibility(View.GONE);
//            video_title.setVisibility(View.GONE);
//            saved_videos_more.setVisibility(View.VISIBLE);
//        }
        client_name.setText("Video from " + specificUserVideoArrayList.get(i).getSender_name());
        video_title.setText(specificUserVideoArrayList.get(i).getVideo_name());
        try {
            Glide.with(context)
                    .load(specificUserVideoArrayList.get(i).getVideo_url())
                    .into(video_upload);
        } catch (Exception e) {
            e.printStackTrace();
        }

        play_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Play_video play_video = new Play_video();
                Bundle b = new Bundle();
                b.putString("from", "Videos_activity_user_other");
                b.putString("video", specificUserVideoArrayList.get(i).getVideo_url());
                play_video.setArguments(b);
                ((MainActivity) context).loadFragment(play_video, "Play_Video", view);
            }
        });
        saved_videos_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VidioesActivity vidioesActivity = new VidioesActivity();
                Bundle bundle = new Bundle();
                bundle.putString("from", "Specific_user_from");
                bundle.putString("from_id", specificUserVideoArrayList.get(i).getSerder_id());
                vidioesActivity.setArguments(bundle);
                ((MainActivity)context).loadFragment(vidioesActivity, "Videos_activity", view);
            }
        });
        return view1;
    }
}
