package com.stembuddy.activities;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.util.Util;
import com.stembuddy.BuildConfig;
import com.stembuddy.R;
import com.stembuddy.RealPathUtil;
import com.stembuddy.apicall.FileUploadModel;
import com.stembuddy.apicall.UpdateMultipartListener;
import com.stembuddy.apicall.VolleyMultipartRequest;
import com.stembuddy.apicall.onUpdateViewListener;
import com.stembuddy.fragment.MainHome_Fragment;
import com.stembuddy.fragment.View_Videos_Fragment;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.Constants;
import com.vincent.videocompressor.VideoCompress;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by THAKUR on 11/27/2017.
 */

public class VideosStore extends Fragment implements View.OnClickListener {

    private View v;
    private int REQUEST_VIDEO_CAPTURE = 1;
    private File file, file1;
    private String for_what = "";

    private long startTime, endTime;

    Context context;


    private String[] permissions = new String[]{
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA,
    };
    private File file3, file5;
    private String mCurrentPhotoPath;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.videos_store, container, false);
        v.findViewById(R.id.vid_str_pvideo).setOnClickListener(this);
        v.findViewById(R.id.vid_str_sb_com).setOnClickListener(this);
        v.findViewById(R.id.vid_str_spe_user).setOnClickListener(this);
        v.findViewById(R.id.vid_str_view_vid).setOnClickListener(this);
        v.findViewById(R.id.vid_str_mock_interiew).setOnClickListener(this);
        v.findViewById(R.id.btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).loadFragment(new MainHome_Fragment(), "Home", view);

            }
        });
        getActivity().startService(new Intent(getActivity(), UploadInformation.class));
        context = getActivity();
        return v;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.vid_str_pvideo:
                if (checkPermissions()) {
//                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//                    if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
//                        startActivityForResult(takeVideoIntent,
//                                REQUEST_VIDEO_CAPTURE);
//                    }
                    for_what = "own";
                    openVideoRecorder();
//                    Intent intent = new Intent();
//                    intent.setType("video/*");
//                    intent.setAction(Intent.ACTION_PICK);
//                    startActivityForResult(Intent.createChooser(intent, "Take Video"), 3);
                }
                break;
            case R.id.vid_str_sb_com:
                open_chooser_dialog();
//                VidioesActivity vidioesActivity2 = new VidioesActivity();
//                Bundle bundle2 = new Bundle();
//                bundle2.putString("from", "Videos_Share_Community");
//                vidioesActivity2.setArguments(bundle2);
//                ((MainActivity) getActivity()).loadFragment(vidioesActivity2, "Videos_activity", view);
                break;
            case R.id.vid_str_spe_user:
                VidioesActivity vidioesActivity1 = new VidioesActivity();
                Bundle bundle1 = new Bundle();
                bundle1.putString("from", "Videos_Share");
                vidioesActivity1.setArguments(bundle1);
                ((MainActivity) getActivity()).loadFragment(vidioesActivity1, "Videos_activity", view);
                break;
            case R.id.vid_str_view_vid:
                ((MainActivity) getActivity()).loadFragment(new View_Videos_Fragment(), "Videos_activity", view);
                break;
            case R.id.vid_str_mock_interiew:
                VidioesActivity vidioesActivity2 = new VidioesActivity();
                Bundle bundle2 = new Bundle();
                bundle2.putString("from", "mock_interview");
                vidioesActivity2.setArguments(bundle2);
                ((MainActivity) getActivity()).loadFragment(vidioesActivity2, "Videos_activity", view);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3) {

            try {
                Uri uri = data.getData();
                String file_path = data.getData().getPath();
                String nwePth = file_path.substring(15, file_path.length() - 1);

                //file = new File(new URI("file://" + file_path.replace(" ", "%20")));

                // String videoPath = RealPathUtil.getRealPath(context, uri);
                file5 = new File(mCurrentPhotoPath);
                long lll = file5.length();
                file = new File(new URI("file://" + file_path.replace(" ", "%20")));
                String abAPth = file.getAbsolutePath();

                long fileSizeInBytes = file.length();
                long fileSizeInKB = fileSizeInBytes / 1024;



// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                long fileSizeInMB = fileSizeInKB / 1024;
                String thumbnailpath = "/storage/emulated/0/" + "myvideo.mp4";
                Bitmap tmb = ThumbnailUtils.createVideoThumbnail(mCurrentPhotoPath,
                        MediaStore.Images.Thumbnails.MINI_KIND);
                String image_path = getRealPathFromURI(getImageUri(getActivity(), tmb));
                file1 = new File(new URI("file://" + image_path.replace(" ", "%20")));
//                createThumbnailFromPath(file_path , MediaStore.Images.Thumbnails.MINI_KIND);
//                Uri selectedVideo = data.getData();
//                String[] filePathColumn = {MediaStore.Video.Media.DATA};
//
//                Cursor cursor = getActivity().getContentResolver().query(selectedVideo, filePathColumn, null, null, null);
//                cursor.moveToFirst();
//
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                String filePath = cursor.getString(columnIndex);
//                file = new File(new URI("file://" + filePath.replace(" ", "%20")));
//                cursor.close();


                String destinationPath = "/storage/emulated/0/" +
                        new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4";
                file3 = new File(new URI("file://" + file_path.replace(" ", "%20")));
                long kk = file3.length();
                InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
                // openDialog();
                compressVideio(file5.getAbsolutePath(), destinationPath);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Locale getLocale() {
        Configuration config = getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }

        return sysLocale;
    }

    @SuppressWarnings("deprecation")
    public static Locale getSystemLocaleLegacy(Configuration config) {
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getSystemLocale(Configuration config) {
        return config.getLocales().get(0);
    }

    private void compressVideio(String file_path, final String desPath) {
        ((MainActivity) getActivity()).showLoader();

        VideoCompress.compressVideoLow(file_path, desPath, new VideoCompress.CompressListener() {
            @Override
            public void onStart() {

                startTime = System.currentTimeMillis();

                //com.stembuddy.utils.Util.writeFile(getActivity(), "Start at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()) + "\n");

            }

            @Override
            public void onSuccess() {


                String file_path = desPath;
                try {
                    file3 = new File(new URI("file://" + desPath.replace(" ", "%20")));
                    long fileSizeInBytes = file3.length();

                    long fileSizeInKB = fileSizeInBytes / 1024;
// Convert the KB to MegaBytes (1 MB = 1024 KBytes)
                    long fileSizeInMB = fileSizeInKB / 1024;

                    Log.v("CompressedFileSize", String.valueOf(fileSizeInMB));

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                ((MainActivity) getActivity()).dismissLoader();

                openDialog();

                endTime = System.currentTimeMillis();

                com.stembuddy.utils.Util.writeFile(getActivity(), "End at: " + new SimpleDateFormat("HH:mm:ss", getLocale()).format(new Date()) + "\n");
                com.stembuddy.utils.Util.writeFile(getActivity(), "Total: " + ((endTime - startTime) / 1000) + "s" + "\n");
                com.stembuddy.utils.Util.writeFile(getActivity());

            }

            @Override
            public void onFail() {

            }

            @Override
            public void onProgress(float percent) {

            }
        });
    }

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(getActivity(), p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 100);
            return false;
        }
        return true;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void openDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_set_title_video);
        dialog.show();
        final EditText dialog_set_title_video = (EditText) dialog.findViewById(R.id.title_video);
        dialog.findViewById(R.id.cancel_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.submit_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                hitApiForUpload_video(dialog_set_title_video.getText().toString(), dialog);
                if (((BaseActivity) getActivity()).isNetworkEnabled(getActivity()))
                    hitApiForUpload_Thumbnail(dialog_set_title_video.getText().toString(), dialog);
                else
                    Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public File createImageFile() throws IOException {

        mCurrentPhotoPath = "";
        String imageFileName = "Video_";
        String state = Environment.getExternalStorageState();
        File storageDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            storageDir = Environment.getExternalStorageDirectory();
        } else {
            storageDir = getActivity().getFilesDir();
        }
        storageDir.mkdirs();
        File appFile = new File(storageDir, getString(R.string.app_name));
        appFile.mkdir();
        File image = File.createTempFile(
                imageFileName,  // prefix /
                ".mp4",         // suffix /
                appFile      // directory /
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void openVideoRecorder() {
        File mediaFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
                new SimpleDateFormat("yyyyMMdd_HHmmss", getLocale()).format(new Date()) + ".mp4");
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        Uri videoUri = null;
        try {
            videoUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, createImageFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Uri videoUri = Uri.fromFile(mediaFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,videoUri);
        // intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        //code here//////////////////////////////////

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        } else {
            List<ResolveInfo> resInfoList = getActivity().getPackageManager()
                    .queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                getActivity().grantUriPermission(packageName, videoUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }
        startActivityForResult(intent, 3);
    }

    public void openPath(Uri uri, File file) {
        InputStream is = null;
        try {
            is = getActivity().getContentResolver().openInputStream(uri);
            //Convert your stream to data here
            File file2 = copyInputStreamToFile(is, file);
            long vedio = file2.length();
            is.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private File copyInputStreamToFile(InputStream in, File file) {
        OutputStream out = null;

        try {
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Ensure that the InputStreams are closed even if there's an exception.
            try {
                if (out != null) {
                    out.close();
                }

                // If you want to close the "in" InputStream yourself then remove this
                // from here but ensure that you close it yourself eventually.
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }

    private void hitApiForUpload_video(String video_title, final Dialog dialog, String thumb_img) {
//        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
//        progressDialog.setMessage("Please Wait");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        VolleyMultipartRequest request = new VolleyMultipartRequest(Constants.BASE_URL,
                new UpdateMultipartListener(getActivity(), new onUpdateViewListener() {
                    @Override
                    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
                        try {
                            ((MainActivity) getActivity()).dismissLoader();
                            dialog.dismiss();
                            JSONArray jsonArray = new JSONArray(responseObject.toString());
                            // JSONObject jsonObject = new JSONObject(responseObject.toString());
                            JSONObject jsonObject = jsonArray.getJSONObject(0);
                            String status = jsonObject.getString("status");
                            if (status.equals("0")) {
                                Toast.makeText(getActivity(), "Video Not uploaded, Please Try Again", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "Video Successfully uploaded", Toast.LENGTH_SHORT).show();
                            }

                           /* if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                Toast.makeText(getActivity(), "Video Successfully uploaded", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), "Video Not uploaded, Please Try Again", Toast.LENGTH_SHORT).show();
                            }*/
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getActivity(), responseObject.toString(), Toast.LENGTH_SHORT).show();
                            ((MainActivity) getActivity()).dismissLoader();

                        }
                    }
                }, 1), FileUploadModel.class, "file", file3, getParams(video_title, thumb_img));
        int socketTimeout = 150000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        requestQueue.add(request);
    }

    private HashMap<String, String> getParams(String video_title, String thumb_img) {
        HashMap<String, String> map = new HashMap<>();
        map.put("rule", "add_video_by_user");
        map.put("user_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
        map.put("title", video_title);
        map.put("video_type", for_what);
        map.put("thumb_img", thumb_img);
        return map;
    }

    private void hitApiForUpload_Thumbnail(final String video_title, final Dialog dialog) {
        ((MainActivity) getActivity()).showLoader();
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        VolleyMultipartRequest request = new VolleyMultipartRequest(Constants.BASE_URL,
                new UpdateMultipartListener(getActivity(), new onUpdateViewListener() {
                    @Override
                    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
                        try {
//                            progressDialog.dismiss();
                            JSONObject jsonObject = new JSONObject(responseObject.toString());
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String thumb_image = jsonObject.getString("message");
                                hitApiForUpload_video(video_title, dialog, thumb_image);
                            } else {
                                ((MainActivity) getActivity()).dismissLoader();

                                Toast.makeText(getActivity(), responseObject.toString(), Toast.LENGTH_SHORT).show();
                            }
                           /* if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                String thumb_image = jsonObject.getString("message");
                                hitApiForUpload_video(video_title, dialog, thumb_image);
                            } else {
                                //progressDialog.dismiss();
                            }*/


                        } catch (Exception e) {
                            e.printStackTrace();
                            ((MainActivity) getActivity()).dismissLoader();
                            Toast.makeText(getActivity(), responseObject.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, 1), FileUploadModel.class, "file", file1, getParams1());
        int socketTimeout = 50000;
        RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        requestQueue.add(request);
    }

    private HashMap<String, String> getParams1() {
        HashMap<String, String> map = new HashMap<>();
        map.put("rule", "image_upload_by");

        return map;
    }

    private void open_chooser_dialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_choose_community_upload);
        dialog.show();
        dialog.findViewById(R.id.upload_saved_videos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VidioesActivity vidioesActivity = new VidioesActivity();
                Bundle bundle = new Bundle();
                bundle.putString("from", "Videos_Share_Community");
                vidioesActivity.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(vidioesActivity, "Videos_activity", view);
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.upload_new_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermissions()) {
                    for_what = "community";
                    openVideoRecorder();
                    dialog.dismiss();
                }
            }
        });
        dialog.findViewById(R.id.cancel_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
}
