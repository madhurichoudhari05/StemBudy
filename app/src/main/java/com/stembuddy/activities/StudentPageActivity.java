package com.stembuddy.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.stembuddy.R;
import com.stembuddy.apicall.AppController;
import com.stembuddy.shard_preference.SharedPreference_Main;
import com.stembuddy.utils.CircleTransform;
import com.stembuddy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;
import java.util.Map;

import static com.stembuddy.activities.MainActivity.block_Arraylist;

/**
 * Created by THAKUR on 11/28/2017.
 */

public class StudentPageActivity extends Fragment implements View.OnClickListener {

    private View v;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.student_page_layout, container, false);

        try {
            Glide.with(getActivity()).load(getArguments().getString("photo"))
                    .placeholder(R.drawable.profile).transform(new CircleTransform(getActivity()))
                    .into((ImageView) v.findViewById(R.id.show_image));
        } catch (Exception e) {
            e.printStackTrace();
        }

        ((TextView) v.findViewById(R.id.name)).setText(getArguments().getString("name"));

        if (!getArguments().getString("profession").equals("")) {
            ((TextView) v.findViewById(R.id.profession)).setText(getArguments().getString("profession"));
        } else {
            ((TextView) v.findViewById(R.id.profession)).setText("Not Mentioned");
        }

        if (!getArguments().getString("affilation").equals("")) {
            ((TextView) v.findViewById(R.id.affilation)).setText(getArguments().getString("affilation"));
        } else {
            ((TextView) v.findViewById(R.id.affilation)).setText("Not Mentioned");
        }

        if (!getArguments().getString("speciality").equals("")) {
            ((TextView) v.findViewById(R.id.speciality)).setText(getArguments().getString("speciality"));
        } else {
            ((TextView) v.findViewById(R.id.speciality)).setText("Not Mentioned");
        }

        if (!getArguments().getString("description").equals("")) {
            ((TextView) v.findViewById(R.id.desc)).setText(getArguments().getString("description"));
        } else {
            ((TextView) v.findViewById(R.id.desc)).setText("Description: Not Mentioned");
        }

        try {
            if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                v.findViewById(R.id.rate).setVisibility(View.VISIBLE);
                v.findViewById(R.id.view_info).setVisibility(View.GONE);

                if (getArguments().getString("rating").equals("")) {
                    ((RatingBar) v.findViewById(R.id.rate)).setRating(Float.parseFloat("0"));
                } else {
                    ((RatingBar) v.findViewById(R.id.rate)).setRating(Float.parseFloat(getArguments().getString("rating")));
                }
            } else {
                v.findViewById(R.id.rate).setVisibility(View.GONE);
                v.findViewById(R.id.view_info).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        v.findViewById(R.id.stu_add_btn).setOnClickListener(this);
        v.findViewById(R.id.stu_back_btn).setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.stu_add_btn:
                boolean b = true;
                for (int i=0;i<block_Arraylist.size();i++){
                    if (getArguments().getString("id").equalsIgnoreCase(block_Arraylist.get(i))){

                        Toast.makeText(getActivity(), "id::"+getArguments().getString("id"), Toast.LENGTH_SHORT).show();
                        b=false;
                    }
                }
                if (b)
                    if (((BaseActivity)getActivity()).isNetworkEnabled(getActivity())) {
                        hitAPi();
                      //  RequesthitAPi();
                    }
                    else {
                        Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                else Toast.makeText(getActivity(), "You have already Sent Your Request", Toast.LENGTH_SHORT).show();
                break;
            case R.id.stu_back_btn:
                StemBuddyFindResult stemBuddyFindResult = new StemBuddyFindResult();
                Bundle bundle = new Bundle();
                bundle.putString("name_value", getArguments().getString("name_value"));
                stemBuddyFindResult.setArguments(bundle);
                ((MainActivity) getActivity()).loadFragment(stemBuddyFindResult, "StemBuddyFindResult()", view);
                break;
        }

    }

    private void hitAPi() {
        String url = Constants.BASE_URL;
        ((BaseActivity) getActivity()).showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("addByStudent", response);

                try {
                    //  Toast.makeText((MainActivity) getActivity(), "object", Toast.LENGTH_SHORT).show();
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        ((BaseActivity) getActivity()).showToast("Successfully Sent Your Request");
                        hitSend_api();
                        getActivity().finishAffinity();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }
                    else {
                        ((BaseActivity) getActivity()).showToast("You have already Sent Your Request");




                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }



              /*  try {
                    Object jsonobj = new JSONTokener(response).nextValue();
                    if (jsonobj instanceof JSONObject) {
                      //  Toast.makeText((MainActivity) getActivity(), "object", Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).dismissLoader();
                        JSONObject jsonO =new  JSONObject(response);
                        if (jsonO.getString("status").equals("1")) {
                            ((BaseActivity) getActivity()).showToast("Successfully Sent Your Request");
                            getActivity().finishAffinity();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                        } else {
                            ((BaseActivity) getActivity()).showToast("You have already Sent Your Request");
                        }}
                    //you have an object
                    else if (jsonobj instanceof JSONArray){
                       // Toast.makeText((MainActivity) getActivity(), "objectArrayy", Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).dismissLoader();
                        JSONArray array = new JSONArray(response);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonO = array.getJSONObject(i);
                            if (jsonO.getString("status").equals("1")) {
                                ((BaseActivity) getActivity()).showToast("Successfully Sent Your Request");
                                getActivity().finishAffinity();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            } else {
                                ((BaseActivity) getActivity()).showToast("You have already Sent Your Request");
                            }
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "add_fav_by_student");
                    params.put("student_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                    params.put("mentor_id", getArguments().getString("id"));
                } else {
                    params.put("rule", "add_fav_by_mentore");
                    params.put("mentor_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                    params.put("student_id", getArguments().getString("id"));
                }
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "add_fav");
    }





    private void RequesthitAPi() {
        String url = Constants.BASE_URL;
        ((BaseActivity) getActivity()).showLoader();
        final StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("addByStudent", response);

                try {
                    //  Toast.makeText((MainActivity) getActivity(), "object", Toast.LENGTH_SHORT).show();
                    JSONObject jsonO = new JSONObject(response);
                    if (jsonO.getString("status").equals("1")) {
                        ((BaseActivity) getActivity()).showToast("Successfully Sent Your Request");
                        hitSend_api();
                        getActivity().finishAffinity();
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }
                    else {
                        ((BaseActivity) getActivity()).showToast("You have already Sent Your Request");




                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }



              /*  try {
                    Object jsonobj = new JSONTokener(response).nextValue();
                    if (jsonobj instanceof JSONObject) {
                      //  Toast.makeText((MainActivity) getActivity(), "object", Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).dismissLoader();
                        JSONObject jsonO =new  JSONObject(response);
                        if (jsonO.getString("status").equals("1")) {
                            ((BaseActivity) getActivity()).showToast("Successfully Sent Your Request");
                            getActivity().finishAffinity();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                        } else {
                            ((BaseActivity) getActivity()).showToast("You have already Sent Your Request");
                        }}
                    //you have an object
                    else if (jsonobj instanceof JSONArray){
                       // Toast.makeText((MainActivity) getActivity(), "objectArrayy", Toast.LENGTH_SHORT).show();
                        ((BaseActivity) getActivity()).dismissLoader();
                        JSONArray array = new JSONArray(response);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject jsonO = array.getJSONObject(i);
                            if (jsonO.getString("status").equals("1")) {
                                ((BaseActivity) getActivity()).showToast("Successfully Sent Your Request");
                                getActivity().finishAffinity();
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            } else {
                                ((BaseActivity) getActivity()).showToast("You have already Sent Your Request");
                            }
                        }


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
                ((BaseActivity) getActivity()).dismissLoader();
                ((BaseActivity) getActivity()).showToast("Network error... Please try again");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                    params.put("rule", "send_request");
                    params.put("student_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                    params.put("mentor_id", getArguments().getString("id"));
                    params.put("sender_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                    params.put("receiver_id", getArguments().getString("id"));


                }
                else {
                    params.put("rule", "send_request");
                    params.put("mentor_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                    params.put("student_id", getArguments().getString("id"));
                    params.put("sender_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                    params.put("receiver_id", getArguments().getString("id"));

                }
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(sr, "add_fav");
    }
















    private void hitSend_api() {
            ((BaseActivity) getActivity()).showLoader();
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            StringRequest sr = new StringRequest(Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
//                ((BaseActivity) getActivity()).dismissLoader();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("status").equalsIgnoreCase("1")) {


                            /*Madhuri*/


                            Toast.makeText(getActivity(), "Successfully Sent Your Request", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "You have already Sent Your Request", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
//                ((BaseActivity) getActivity()).dismissLoader();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("rule", "notification_ios");

                    if (SharedPreference_Main.getInstance(getActivity()).getUserType().equals("student")) {
                        params.put("to_id",  getArguments().getString("id"));
                    } else {
                        params.put("to_id",  getArguments().getString("id"));
                    }
                    params.put("from_id", SharedPreference_Main.getInstance(getActivity()).getUserId());
                    params.put("body", SharedPreference_Main.getInstance(getActivity()).getUserName()+" send a friend request.");
                    return params;
                }
            };
            int socketTimeout = 30000;
            RetryPolicy retryPolicy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            sr.setRetryPolicy(retryPolicy);
            requestQueue.add(sr);
        }




}
